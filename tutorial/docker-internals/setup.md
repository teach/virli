\newpage

Mise en place
=============

* `docker-compose`
* `venv` (Python3)
* `jq`
* `runc`
* `containerd`
* `ctr`
* `linuxkit`
