### Clair

Un outil complet pour indexer et chercher des vulnérabilités est
[`Clair`](https://github.com/coreos/clair/), du projet CoreOS. À partir des
informations mises à disposition par les équipes de sécurités des principales
distributions, cela alimente en continu une base de données qui sera accédé au
moment de l'analyse.

L'outil se présente sous la forme d'un serveur autonome dans la récupération
de ses données sources, auquel nous pourrons interagir au moyen d'une API :
pour lui envoyer des images et lui demander une analyse. Les clients de cette
API seront soit les registres directement, soit un programme dédié.

---

Commençons par lancer notre propre instance de `Clair`, à l'aide d'un
`docker-compose.yml` :

<div lang="en-US">
```yml
version: '3'
services:
  postgres:
    container_name: clair_postgres
    image: postgres:latest
    restart: unless-stopped
    environment:
      - POSTGRES_PASSWORD

  clair:
    container_name: clair_clair
    image: quay.io/coreos/clair:v2.0.9
    restart: unless-stopped
    depends_on:
      - postgres
    ports:
      - "6060-6061:6060-6061"
    links:
      - postgres
    volumes:
      - /tmp:/tmp
      - ./clair_config:/config
    command: [-config, /config/config.yaml]
```
</div>

Prenez quelques minutes pour comprendre ce `docker-compose.yml` : notez la
présence de la variable d'environnement `POSTGRES_PASSWORD`, non définie : ce
sera la variable présente dans votre environnement, au moment du
`docker-compose up` qui sera utilisée. N'oubliez pas de la définir :

<div lang="en-US">
```bash
export POSTGRES_PASSWORD=$(openssl rand -base64 16)
```
</div>

Parmi les volumes partagés avec `clair`, il y a un dossier
`./clair_config`. Notez le `./` au début, qui indique que le dossier sera
recherché relativement par rapport à l'emplacement du `docker-compsose.yml`.

Dans ce dossier, vous devez placer un exemplaire du fichier de configuration
dont un [exemple se trouve dans le dépôt du
projet](https://raw.githubusercontent.com/coreos/clair/master/config.yaml.sample). **N'oubliez
pas de changer le nom d'hôte et le mot de passe pour se connecter au conteneur
de base de données.**

Une fois lancé, la base nécessite d'être initialisée. L'opération peut prendre
plusieurs minutes. Vous pouvez suivre l'avancement de l'ajout via :

<div lang="en-US">
```bash
curl http://localhost:6060/v1/namespaces
curl http://localhost:6060/v1/namespaces/debian:9/vulnerabilities?limit=10
```
</div>


### PAClair

Afin de pouvoir réaliser à la demande et sans registre privé, l'analyse de
conteneur, nous allons utiliser le programme
[`paclair`](https://github.com/yebinama/paclair) :

<div lang="en-US">
```bash
pip3 install paclair
```
</div>

Il nécessite un fichier de configuration pour être utilisé, essayez :

<div lang="en-US">
```yml
General:
  clair_url: 'http://localhost:6060'
Plugins:
  Docker:
    class: paclair.plugins.docker_plugin.DockerPlugin
```
</div>

Pour obtenir un rapport d'analyse, on commence par envoyer les couches de
l'image à `Clair` :

<div lang="en-US">
```bash
paclair --conf conf.yml Docker nemunaire/fic-admin push
```
</div>

Puis on lui demande la génération d'un rapport `html` :

<div lang="en-US">
```bash
paclair --conf conf.yml Docker nemunaire/fic-admin analyse \
   --output-format html --output-report file
```
</div>

![Rapport d'analyse statique des vulnérabilités par Clair](paclair.png)

Si l'on souhaite uniquement avoir des statistiques dans la console :

<div lang="en-US">
```bash
42sh$ paclair --conf conf.yml Docker node:latest analyse --output-format stats
Unknown: 2
Negligible: 1
Medium: 5
High: 4
```
</div>
