\newpage

`linuxkit`
==========

[`linuxkit`](https://github.com/linuxkit/linuxkit) est encore un autre projet
tirant parti des conteneurs. Il se positionne comme un système de construction
de machine. En effet, grâce à lui, nous allons pouvoir générer des systèmes
complets, *bootable* dans QEMU, VirtualBox, VMware ou même sur des machines
physiques, qu'il s'agisse de PC ou bien même de Raspberry Pi, ou même encore
des images pour les différents fournisseurs de cloud !

Bien entendu, au sein de ce système, tout est fait de conteneur ! Alors quand
il s'agit de donner une IP publique utilisable par l'ensemble des conteneurs,
il faut savoir jouer avec les *namespaces* pour arriver à ses fins !
