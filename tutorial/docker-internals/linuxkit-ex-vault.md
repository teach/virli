::::: {.exercice}

Réalisez une recette `vault.yml` démarrant une instance du gestionnaire de
secrets [Hashicorp Vault](https://www.vaultproject.io/), utilisant une [base de
données au
choix](https://www.vaultproject.io/docs/configuration/storage/index.html)
(Consul, Etcd, MySQL, Cassandra, ...).

Au démarrage, Vault devra déjà être configuré pour parler à sa base de données,
qui devra se trouver dans un conteneur isolé et non accessible d'internet. Il
faudra donc établir un lien `virtual ethernet` entre les deux conteneurs ; et
ne pas oublier de le configurer (automatiquement au *runtime*, grâce à un
[`poststart`
*hook*](https://github.com/opencontainers/runtime-spec/blob/master/config.md#posix-platform-hooks)
ou bien à un conteneur issu du *package*
[`ip`](https://github.com/linuxkit/linuxkit/tree/master/pkg/ip)).

Les permissions étant généralement très strictes, vous aurez sans doute besoin
de les assouplir un peu en ajoutant des *capabilities* autorisées à vos
conteneurs, sans quoi vos conteneurs risquent d'être tués prématurément.

En bonus, vous pouvez gérer la [persistance des
données](https://github.com/linuxkit/linuxkit/blob/master/examples/swap.yml)
stockées dans Vault.

:::::
