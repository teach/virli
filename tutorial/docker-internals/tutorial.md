---
title: Virtualisation légère -- TP n^o^ 5
subtitle: Linux et Docker Internals -- Systèmes de fichiers et projets de l'Open Container Initiative
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 17 novembre 2022
abstract: |
  Après avoir beaucoup parlé de Docker, nous allons voir dans ce TP la
  manière dont les différents projets qu'il utilise dans sa plomberie
  interne interagissent.

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le mercredi 30
  novembre 2022 à 23 h 42. Consultez les sections matérialisées par un
  bandeau jaunes et un engrenage pour plus d'informations sur les
  éléments à rendre.
...
