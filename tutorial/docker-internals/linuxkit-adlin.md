Si vous vous rappelez du cours d'AdLin[^adlin] en début d'années, toutes les
VMs que vous avez utilisées reposaient entièrement sur `linuxkit`. En fait,
chaque conteneur représentait alors une machine différente : un conteneur
postgres d'un côté, un conteneur `miniflux` directement issu du DockerHub, un
conteneur `unbound` pour faire office de serveur DNS, et les machines clientes
étaient de simples conteneurs exécutant un client DHCP.

[^adlin]: toutes les sources des machines sont dans ce dépôt :
    <https://git.nemunai.re/srs/adlin>. Les fichiers de construction
    LinuxKit sont `challenge.yml`, `server.yml`, `tuto2.yml`,
    `tuto3.yml`
