[`Clair`](https://github.com/coreos/clair/) est un projet de CoreOS, similaire
à Trivy : il va rechercher parmi sa base de vulnérabilités lesquels concernent
les images qu'on lui donne.

Contrairement à Trivy, il ne va pas tenter d'analyser les dépendances
supplémentaires des applications métiers (Trivy est capable d'analyser les
dépendances PHP, Python, Ruby, Go, ...).

La mise en œuvre de Clair est un peu plus complexe car il s'agit d'un *daemon*
qui s'exécute en permanence, et auquel on peut transmettre, via une API, nos
images. Dans un contexte d'intégration continue, ou d'un registre d'images qui
teste régulièrement ses images hébergées, c'est un outil idéal. Néanmoins il
faut le configurer un minimum pour qu'il soit opérationnel. Consultez [l'annexe
dédiée](https://virli.nemunai.re/tutorial-2-clair.pdf) si vous souhaitez opter
pour cette solution.

On notera tout de même que les outils donnés générent des rapports HTML avec
des graphiques explicites :

![Rapport d'analyse statique des vulnérabilités par Clair](paclair.png)
