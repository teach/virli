---
title: Virtualisation légère -- TP n^o^ 2
subtitle: Construire des images Docker et leur sécurité
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 21 septembre 2022
abstract: |
  Durant ce deuxième TP, nous allons voir comment créer nos propres
  images, et comment s'assurer qu'elles n'ont pas de vulnérabilités
  connues !
...
