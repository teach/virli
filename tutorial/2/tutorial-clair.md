---
title: Virtualisation légère -- TP n^o^ 2
subtitle: Construire des images Docker et leur sécurité -- Annexe
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 21 septembre 2022
abstract: |
  Clair est un outil très intéressant à déployer dans un contexte d'analyse
  continue, au sein d'une plateforme de déploiement continue par exemple. Vous
  retrouverez dans cette annexe un guide pour le mettre en place et faire vos
  premiers pas avec.
...

![Rapport d'analyse statique des vulnérabilités d'un conteneur](paclair.png)

Une vision plus Clair de la sécurité des images
-----------------------------------------------
