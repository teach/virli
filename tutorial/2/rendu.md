\newpage

Rendu
=====

Est attendu d'ici le cours suivant :

- votre `Dockerfile` et son *entrypoint* pour dépôt `youp0m` ;
- le [mini-projet numéro 2](https://virli.nemunai.re/2-project.pdf) ;
- vos réponses à [l'évaluation du cours](https://virli.nemunai.re/quiz/34).


Arborescence attendue
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires) :

<div lang="en-US">
```
./
./youp0m/
./youp0m/Dockerfile
./youp0m/entrypoint.sh
./youp0m/.dockerignore
./youp0m/...
./ctr-updater/
./ctr-updater/Dockerfile
./ctr-updater/...
```
</div>

::::: {.question}

#### Rendre ou ne pas rendre le dépôt `youp0m` {-}

Les fichiers du dépôt seront placé dans le dossier `youp0m`. Vous
n'avez qu'à rendre les fichiers supplémentaires que vous ajoutez au dépôt.

:::::

Votre rendu sera pris en compte en faisant un [tag **signé par votre clef
PGP**](https://lessons.nemunai.re/keys). Consultez les détails du rendu (nom du
tag, ...) sur la page dédiée au projet sur la plateforme de rendu.

::::: {.question}

Si vous utilisez un seul dépôt pour tous vos rendus, vous **DEVRIEZ**
créer une branche distincte pour chaque rendu :

<div lang="en-US">
```
42sh$ git checkout --orphan renduX
42sh$ git reset
42sh$ rm -r *
42sh$   # Créer l'arborescence de rendu ici
```
</div>

Pour retrouver ensuite vos rendus des travaux précédents :

<div lang="en-US">
```
42sh$ git checkout renduY
-- ou --
42sh$ git checkout master
...
```
</div>

Chaque branche est complètement indépendante l'une de l'autre. Vous
pouvez avoir les exercices du TP1 sur `master`, les exercices du TP2
sur `rendu2`, ... ce qui vous permet d'avoir une arborescence
correspondant à ce qui est demandé, sans pour autant perdre votre
travail (ou le rendre plus difficile d'accès).

::::
