
### Les conteneurs Docker

Alors que les images constituent la partie immuable de Docker, les conteneurs
sont sa partie vivante. Chaque conteneur est créé à partir d'une image : à
chaque fois que nous lançons un conteneur, une couche lecture/écriture est
ajoutée au-dessus de l'image. Cette couche est propre au conteneur et
temporaire : l'image n'est pas modifiée par l'exécution d'un conteneur.

![Couches d'un conteneur](layers-multi-container.png "Couches d'un conteneur"){ width=70% }

Chaque conteneur s'exécute dans un environnement restreint et distinct de
l'environnement principal (où vous avez votre bureau). Par exemple, dans cet
environnement, vous ne pouvez pas voir les processus qui sont situés en dehors,
ni accéder aux fichiers extérieurs.
