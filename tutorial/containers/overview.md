L'écosystème des conteneurs
===========================

Pour entrer dans le monde des conteneurs, il faut bien saisir un certain nombre
de concepts qui ont vu le jour progressivement, au fil des tentatives faites
pour standardiser tout cet écosystème. Ce sont des notions importantes, car la
plupart des outils utilisent d'une manière ou d'une autre ces concepts.


Les images
----------

Une image est une méthode pour distribuer un logiciel, qui s'apparente à un
package. L'image contient les exécutables, mais aussi toutes les dépendances,
les bibliothèques systèmes, les fichiers de configuration, ... Tout ce dont
notre logiciel a besoin pour fonctionner.

Il existe différentes typologies d'images :

- certaines contiennent des logiciels ou des services prêts à l'emploi : le
  serveur web `nginx`, ou `apache`, ou encore la base de données MySQL, ou
  MongoDB, ...
- certaines contiennent un programme spécifique : on peut vouloir utiliser
  `git`, le client `MySQL`, `emacs` ou bien encore le navigateur Chrome ;
- d'autes enfin servent pour développer et étendre les possibilités. Ce sont
  souvent des images utilisées comme bases pour les deux types que l'on a vus :
  Debian, Alpine, Windows, ...

Il est aisé de s'échanger des images, ainsi tout le monde peut récupérer des
images de ses logiciels favoris.


Les registres
-------------

Les registres sont des plateformes qui centralisent les images. Ils permettent
de récupérer les images, ils disposent généralement d'une interface permettant
de consulter leur catalogue, et ils permettent aussi d'en envoyer.

Les registres les plus connus sont le Docker Hub ou Quay.io. Ils contiennent à
la fois :
- des images officielles (`nginx`, `postgres`, ...) issues directement de leurs
  éditeurs,
- des images communautaires, maintenues par les utilisateurs d'un logiciel,
  mais aussi,
- des images de tout un chacun pour ses propres besoins.

D'autres registres existent en dehors de ces grands noms publics. Certaines
forges telles que GitHub, GitLab ou Gitea proposent un service de registre que
leurs utilisateurs peuvent utiliser pour publier les images liées à un
projet. Cela permet aux utilisateurs de retrouver tous les produits de
compilation au même endroit.

Enfin, chacun peut déployer son propre registre, qui peut alors avoir des
droits d'accès restreint. Une entreprise peut vouloir déployer un registre
privé lorsqu'elle utilise des conteneurs dans son infrastructure, mais qu'il
n'est pas souhaitable que ses images soient diffusées.


Les conteneurs
--------------

On parle d'un conteneur pour désigner une instance en cours d'exécution.

Lorsqu'on lance un conteneur, une copie de l'image est créée sur le disque,
puis le système crée une isolation, pour contenir et restreindre l'exécution
aux seules données de l'image.

Les conteneurs sont par nature **immuables** : on exécute le contenu d'une
image déjà construite, on n'y change pas le code qui est exécuté et on n'y fait
pas de changements qui pourrait altérer son fonctionnement. L'idée directrice dans
le design des conteneurs est de toujours être en mesure de pouvoir relancer un
conteneur ailleurs et s'attendre exactement au même comportement.

Cela signifie que l'on ne fait par exemple pas de mise à jour dans un
conteneur : lorsque notre application ou le système sous-jacent a besoin d'être
mis à jour, on construit ou on télécharge une nouvelle image.

Ce qui distingue deux exécutions de la même image, ce sont les données que l'on
donne au conteneur.

::::: {.question}

##### Tous les conteneurs utilisent ces principes ? {-}

Nous avons décrit ici les concepts propres aux conteneurs applicatifs. Dans le
cas des conteneurs systèmes, leurs cycles de vie sont plus proches de l'usage que
l'on peut avoir des machines virtuelles classiques : chaque conteneur démarre
avec son historique (la manière dont il a été installé, les mises à jour qui
ont été appliquées, les configurations modifiées et les données qui ont été
apportées au fil du temps ...). On ne peut pas dans ces circonstances
remplacer un conteneur système par un autre, tout comme on ne peut pas
remplacer une machine virtuelle que l'on a installée et configuré à la main.

:::::
