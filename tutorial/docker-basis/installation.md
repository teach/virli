Installation
------------

Docker repose sur plusieurs techniques implémentées dans les noyaux Linux
récents (et plus marginalement, Windows). Ces techniques, contrairement aux
instructions de virtualisation qui rendent les hyperviseurs attractifs, ne sont
pas limitées à une architecture de microprocesseur spécifique ; cependant la
communauté autour de Docker utilise principalement l'architecture `amd64`,
c'est sur cette dernière que Docker pourra être exploité à son plein potentiel,
suivi de près par l'architecture `arm64`.

Avant de continuer, assurez-vous que votre machine a bien démarré sur un noyau
64 bits. Le retour de la commande `uname -m` doit vous indiquer :

<div lang="en-US">
```
x86_64
```
</div>

Ou si vous êtes intrépide :

<div lang="en-US">
```
aarch64
```
</div>

Assurez-vous également d'avoir un noyau récent, avec la commande `uname -r` :

<div lang="en-US">
```
5.14.4-gentoo
```
</div>

Rassurez-vous, même si vous n'avez pas compilé le dernier noyau disponible sur
[`kernel.org`](https://www.kernel.org/), Docker s'utilise à partir de Linux 3.10.


### Sous Linux, par le gestionnaire de paquets

En général, votre distribution mettra à votre disposition une version de Docker
plus ou moins récente. Sous Debian et ses dérivés (Ubuntu, Mint, ...) le paquet
a été nommé [`docker.io`](https://packages.debian.org/sid/docker.io).

Si dans un environnement de production, on préférera sans doute utiliser une
version déjà bien éprouvée (comme celle des dépôts de sa distribution), pour
bien suivre les exemples, nous allons avoir besoin de la **dernière version
disponible**. Référez-vous à la documentation officielle correspondant à votre
distribution :

<https://docs.docker.com/engine/install/debian/>

### Sous Windows et macOS

Bien que les fonctionnalités de contenerisation de Docker que nous utiliserons
ne soient disponibles que sous Linux, il est possible d'utiliser Docker de
manière déportée : le daemon Docker tournera dans une machine virtuelle Linux,
mais vous pourrez interagir avec lui via votre ligne de commande habituelle.

Téléchargez la version correspondant à votre système d'exploitation :

* Docker Desktop for Mac :\
  <https://hub.docker.com/editions/community/docker-ce-desktop-mac>

* Docker Desktop for Windows :\
  <https://hub.docker.com/editions/community/docker-ce-desktop-windows>

Une fois l'installation terminée, lancez l'application : elle ajoutera une
icône dans la zone de notification, vous permettant de contrôler l'exécution de
la machine virtuelle sous-jacente.

::::: {.warning}

Depuis septembre 2021, ces applications sont passées sous une licence payante
pour les grosses entreprises[^DockerSubscription]. Cela ne vous concerne pas,
car la licence est gratuite pour un usage éducatif ou personnel.

Notez que cela ne concerne pas le projet ou le binaire Docker : ceux-ci restent
libres. Seules les applications Docker Desktop sont concernées.

:::::

[^DockerSubscription]: <https://www.docker.com/blog/updating-product-subscriptions/>

### Évaluation en ligne

Si vous rencontrez des difficultés pour vous lancer, le projet Play With
Docker[^PlayWithDocker] vous donne accès à un bac à sable
dans lequel vous pourrez commencer à faire les exercices à suivre.

[^PlayWithDocker]: Play With Docker est accessible à cette adresse : <https://labs.play-with-docker.com/>

Il vous faudra disposer [d'un compte
Docker](https://hub.docker.com/signup). Une fois identifié, vous pourrez créer
une nouvelle instance, et vous connecter dessus via SSH.


### Vérifier la bonne marche de l'installation

Vous devriez maintenant être capable de lancer la commande suivante :

<div lang="en-US">
```bash
docker version
```
</div>

Une sortie similaire au bloc suivant devrait apparaître sur votre écran :

<div lang="en-US">
```
Client:
 Version:           20.10.8
 API version:       1.41
 Go version:        go1.16.6
 Git commit:        3967b7d28e
 Built:             Wed Aug  4 12:55:42 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      false

Server:
 Engine:
  Version:          20.10.8
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.16.6
  Git commit:       75249d88bc
  Built:            Wed Aug  4 12:55:42 2021
  OS/Arch:          linux/amd64
  Experimental:     true
 containerd:
  Version:          1.5.5
  GitCommit:        72cec4be58a9eb6b2910f5d10f1c01ca47d231c0.m
 runc:
  Version:          1.0.2
  GitCommit:        v1.0.2-0-g52b36a2d
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
```
</div>


### Problèmes courants

#### `no such file or directory`?

Si vous avez cette erreur : `dial unix /var/run/docker.sock: no such file or
directory.`, le daemon n'est sans doute pas lancé. Lancez-le :

<div lang="en-US">
```bash
sudo service docker restart
```
</div>


#### `permission denied`?

Si vous avez cette erreur : `dial unix /var/run/docker.sock: permission
denied.`, ajoutez votre utilisateur au groupe `docker` et **relancez votre
session** :

<div lang="en-US">
```bash
sudo gpasswd -a $USER docker
```
</div>

::::: {.warning}

Cette action n'est pas anodine d'un point de vue de la sécurité :

<https://docs.docker.com/engine/security/#docker-daemon-attack-surface>

:::::
