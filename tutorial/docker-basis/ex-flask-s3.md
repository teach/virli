::::: {.exercice}

Faire persister les données à l'heure du cloud
----------------------------------------------

Aïe, c'est un sujet épineux... On peut entendre ici et là que les conteneurs
permettent de monter en puissance facilement, mais lorsque l'on est lié à un
espace de stockage classique, on se heurte bien vite à des réalités physiques.

Même si l'on utilise un système de fichiers partagé pour stocker nos images et
ainsi répartir la puissance de calcul entre plusieurs machines, il va vite
arriver un moment où la bande passante du disque réseau ne suivra plus. Il faut
donc passer sur une classe de stockage d'un autre type : l'Object Storage.

L'Object Storage est une méthode de stockage des objets adaptée au monde du
Cloud Computing. Il s'agit d'une API HTTP avec laquelle on peut interagir pour
accéder, ajouter, partager ou gérer un flot d'octets, que l'on nomme *objet* dans
le jargon, mais il s'agit ni plus ni moins que d'un fichier.

Il est de la responsabilité de l'administrateur du service (Amazon, Microsoft,
Google ...) de faire en sorte que les données soient suffisamment réparties
pour éviter les goulots d'étranglement et les SPOF. Ce n'est plus notre
problème de gérer la distribution de la bande passante, ni l'usure des disques,
ni même la capacité de notre espace de stockage.

Toutes les applications ne sont malheureusement pas nativement compatibles avec
ce système de stockage. Mais il se trouve que youp0m le supporte !

Nous allons déployer le service `minio`, qui est une implémentation libre de
l'API d'Amazon S3. `minio` dispose d'une interface graphique pour gérer nos
*buckets*, c'est un volume auquel on attribue des droits particuliers,
notamment quel utilisateur y a accès. C'est au sein d'un *bucket* que l'on va
pouvoir envoyer nos fichiers. Ceux-ci seront référencés avec une *clef*
(l'équivalent du chemin) : il n'y a pas de notion d'arborescence ou de dossier,
mais par convention, lorsque l'on place un `/` dans le nom de la clef, on
considère qu'il s'agit d'un séparateur de dossier.

::::: {.question}

Comme il n'y a pas de notion de dossier, il n'y a pas besoin de créer
l'arborescence avant d'écrire un objet. Chaque objet est référencé par sa clef,
sans qu'il ne soit question d'arborescence ou de dossier.

À partir d'un *bucket* vide, vous pouvez donc directement ajouter le fichier :
`images/next/racoon.jpg`, sans vous préoccuper de l'existence ou non des
dossiers `images` et `next`, la séparation n'est qu'une convention, l'objet
n'est pas enregistré comme étant le fichier `racoon.jpg` du dossier `next`,
mais bien comme `images/next/racoon.jpg`.

:::::

Au sein de l'interface minio, vous devrez donc créer un bucket (par exemple
`youp0m` puisque ce sera un *bucket* dédié à cette application). Ainsi qu'un
compte de service (sous *Identity* > *Service Accounts*). Cela vous permettra
de récupérer une *Access Key* ainsi que sa *Secret Key* associée. Ce sont des
informations qui permettront au conteneur `youp0m` de s'identifier auprès de
l'API.


Il faudra ensuite lancer un conteneur youp0m avec les
options suivantes :

<div lang="en-US">
```
S3_ENDPOINT=http://NOM_DU_CONTENEUR_MINIO:9000
S3_BUCKET=NOM_DU_BUCKET
S3_ACCESS_KEY=ACCESS_KEY_GENEREE
S3_SECRET_KEY=SECRET_KEY_CORRESPONDANTE
S3_PATH_STYLE=true
```
</div>

::::: {.more}

La dernière option `S3_PATH_STYLE` est importante lorsque l'on utilise minio,
car il y a une subtile différence entre l'API d'Amazon et celle de minio
lorsque l'on ne le configure pas davantage.

Un *bucket* pour Amazon S3 correspond à un sous-domaine (par exemple
`youp0m.nemunai.re.s3.amazonaws.com.` pour le bucket
`youp0m.nemunai.re`). Cependant, dans notre installation de minio, nous n'avons
pas de nom de domaine à proprement parler. L'option est donc là pour indiquer
que le nom du *bucket* est attendu comme premier paramètre de l'URL et non pas
comme sous-domaine.

:::::

Votre instance de `youp0m` est désormais réellement prête pour passer en
production. Bravo !

:::::
