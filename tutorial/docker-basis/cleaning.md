Faire le ménage
---------------

Au fur et à mesure de nos tests, la taille utilisée par les données de Docker
peut devenir conséquente et son interface peut commencer à déborder
d'informations dépréciées.

Dans la mesure du possible, Docker essaie de ne pas encombrer inutilement votre
disque dur avec les vieilles images qui ne sont plus utilisées. Il ne va
cependant jamais supprimer une image encore liée à un conteneur ; il ne
supprimera pas non plus les conteneurs qui n'auront pas été démarrés avec
l'option `--rm`.


### Conteneurs

Nous pouvons afficher l'ensemble des conteneurs, quel que soit leur état (en
cours d'exécution, arrêtés, ...) avec la commande suivante :

<div lang="en-US">
```
42sh$ docker container ls -a
CONTAINER ID  IMAGE        CREATED     STATUS                  NAMES
552d71619723  hello-world  4 days ago  Exited (0) 4 days ago   dreamy_g
0e8bbff6d500  debian       2 weeks ago Exited (0) 2 weeks ago  cranky_j
```
</div>

Il y a de fortes chances pour que vous n'ayez plus besoin de ces vieux
conteneurs. Pour les supprimer, utilisez la commande :

<div lang="en-US">
```bash
docker container rm 0e8bbff6d500 552d71619723
```
</div>

ou encore :

<div lang="en-US">
```bash
docker container rm cranky_jones dreamy_gates
```
</div>


### Images et autres objets

De la même manière que pour les conteneurs, nous pouvons lister tous les autres
objets avec la sous-commande `ls` :

<div lang="en-US">
```bash
docker image ls
docker volume ls
docker [object] ls
```
</div>

Et les supprimer avec la sous-commande `rm` suivie du nom ou de l'identifiant
de l'objet (pour les images, il faut aussi préciser le tag en plus du nom).

### `prune`

Dans la plupart des menus permettant de gérer les objets Docker, vous trouverez
une commande `prune` qui supprimera les objets inutilisés :

<div lang="en-US">
```bash
docker container prune
```
</div>

On aura tendance à vouloir supprimer tous les objets inutiles d'un seul coup, via :

<div lang="en-US">
```bash
docker system prune
```
</div>
