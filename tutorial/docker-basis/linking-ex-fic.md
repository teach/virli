::::: {.exercice}

À vous maintenant de connecter une instance de `nemunaire/fic-admin` à sa base
de données.

En lançant le conteneur avec les mêmes options que `youp0m`, les journaux
indiquent que le service cherche à se connecter à une base de données. Il va
donc falloir lier notre interface d'administration à [un conteneur
MariaDB](https://hub.docker.com/_/mariadb).

Ne vous embêtez pas avec les mots de passes des services, initialisez la base
de données avec le nom d'utilisateur et le mot de passe par défaut. Vous les
obtiendrez en lisant la documentation de l'image fic-admin :
<https://hub.docker.com/r/nemunaire/fic-admin/>

<div lang="en-US">
```bash
docker run --rm -e MYSQL_HOST=mysql_cntr_name nemunaire/fic-admin -help
```
</div>

Notez la définition de la variable d'environnement `MYSQL_HOST`, celle-ci
indique le nom du serveur vers lequel le service doit se connecter.

Vous aurez besoin de créer :
- un volume pour stocker la base de données,
- un réseau, dans lequel vous connecterez la base de données et le conteneur applicatif.

Une fois le service `fic-admin` lancé, vous pouvez exposer le port 8081, sur
lequel vous devriez voir l'interface d'admin : <http://localhost:8081/>.

Placez les différentes commandes (volumes, réseau, `run`, ...) dans un
script `ficadmin-run.sh`. Vous devriez pouvoir appeler ce script
plusieurs fois, sans que les données ne soient perdues, entre deux
arrêts.


### Exemple d'exécution  {-}

<div lang="en-US">
```bash
42sh$ ./ficadmin-run.sh
http://localhost:12345/
42sh$ #docker kill db ficadmin
42sh$ ./ficadmin-run.sh  # le script relancera une base de données,
                         # sans avoir perdu les données
http://localhost:12345/
```
</div>

:::::
