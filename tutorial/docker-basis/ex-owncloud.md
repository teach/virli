::::: {.exercice}

Pour mettre en pratiques toutes les notions que l'on a vues jusque-là, écrivez un
script `mycloud-run.sh` pour automatiser le lancement de votre instance
personnelle de [`nextcloud`](https://hub.docker.com/_/nextcloud/) ou
d'[`owncloud`](https://hub.docker.com/r/owncloud/server/). Une attention
particulière devra être apportée à la manière dont vous gérerez le rappel du
script pour éventuellement relancer un conteneur qui se serait arrêté
(évidemment sans perdre les données).

À la fin de son exécution, le script affichera un lien utilisable sur l'hôte
pour se rendre sur la page de connexion. Une autre machine de votre réseau
local devrait également pouvoir accéder à la plate-forme, simplement en
renseignant l'IP de votre machine et en ajoutant éventuellement des règles de
pare-feu (mais cette dernière partie n'est pas demandée, gardez simplement en
tête que cela doit pouvoir être fait manuellement au cas par cas : sur une
machine sans pare-feu configurée, cela ne demande pas d'étape supplémentaire).

Votre script devra se limiter aux notions vues durant cette partie (ie. sans
utiliser `docker-compose` ou `docker stack` que l'on verra par la suite). Il
pourra cependant faire usage des commandes `docker OBJECT inspect` pour ne pas
avoir à faire d'analyse syntaxique sur les retours des commandes lisibles par
les humains.

Cette instance devra utiliser une base de données MySQL (lancée par votre
script dans un autre conteneur) et contenir ses données dans un ou plusieurs
volumes (afin qu'elles persistent à une mise à jour des conteneurs par
exemple).

L'exécution doit être la plus sécurisée possible (pas de port MySQL exposé sur
l'hôte par exemple, etc.) et la plus respectueuse des bonnes pratiques que l'on
a pu voir jusque-là.


### Exemple d'exécution {-}

<div lang="en-US">
```bash
42sh$ ./mycloud-run.sh
http://localhost:12345/
42sh$ #docker kill db
42sh$ ./mycloud-run.sh  # le script relancera une base de données,
                        # sans avoir perdu les données
http://localhost:12345/
```
</div>

:::::

### Au secours, ça veut pas se connecter !

Lorsque nous lançons pour la première fois notre conteneur MySQL ou MariaDB, un
script est chargé d'initialiser le volume attaché à `/var/lib/mysql`. Les
démarrages suivant, ou si vous réutilisez un volume déjà initialisé avec une
base de données, le script ne refait pas d'initialisation. Même si les
variables d'environnement ont changé.

Si vous rencontrez des difficultés pour connecter votre conteneur à
`my-db`, prenez le temps de recréer un volume.
