::::: {.exercice}

Faire persister les données : niveau 1
--------------------------------------

Le service `youp0m` que nous avons déployé fonctionne comme on pourrait s'y
attendre, mais vous imaginez bien que ce n'est pas très pratique de devoir
réimporter les données à chaque fois que l'on met à jour le conteneur ou sa
configuration.

Maintenant que nous savons utiliser les volumes nous allons les utiliser pour
rendre notre service plus pérenne.

Le service stocke par défaut les images dans le dossier `/images` du
conteneur. Pour les sauvegarder hors du conteneur, nous devons donc créer un
volume vers ce dossier :

<div lang="en-US">
```
42sh$ docker volume create youp0m_images
42sh$ docker run -v youp0m-image:/images -p 8080:8080 registry.nemunai.re/youp0m
```
</div>

Ajoutons quelques images puis arrêtons et supprimons le conteneur. Relançons
ensuite un nouveau conteneur avec les mêmes options :

<div lang="en-US">
```
42sh$ docker run -v youp0m-image:/images -p 8080:8080 registry.nemunai.re/youp0m
```
</div>

::::: {.question}

Nous ne recréons pas le volume, il est important de ne pas l'avoir supprimé
ici, puisque c'est ce volume qui assure la persistance des images.

:::::

Nos images sont bien persistantes d'une instance à l'autre de notre contenu.

Nous voici prêt à déployer en production notre service, sans crainte de perdre
les jolies contributions. Mais... est-ce que ce sera suffisant pour répondre aux
milliers de visiteurs attendus ?

:::::
