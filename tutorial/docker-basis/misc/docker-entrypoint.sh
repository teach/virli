#!/bin/sh

[ -n "${NETSOUL_LOGIN}" ] && sed -i "s/user = '%LOGIN%'/user = '${NETSOUL_LOGIN}'/" /tumsoul_0.3.3.py
[ -n "${PASSSOCKS}" ] && sed -i "s/password = '%PASSSOCKS%'/password = '${PASSSOCKS}'/" /tumsoul_0.3.3.py

exec $@
