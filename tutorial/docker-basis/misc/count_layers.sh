#!/bin/sh

IMG="$1"
NLAYER=0

while [ "$IMG" != "" ] && [ "$IMG" != "null" ]
do
    IMG=$(docker inspect "$IMG" | jq .[0].Parent | tr -d '"')
    NLAYER=$(($NLAYER + 1))
done

echo $1 image has $NLAYER layers.
