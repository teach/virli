\newpage

Intervenir durant l'exécution
=============================

Lorsque nous lançons un conteneur, il arrive que son comportement nous
échappe : une imprécision dans la documentation, un cas particulier, une erreur
de configuration, ... Lorsque le conteneur est en cours d'exécution, il y a
plusieurs manières de contrôler ce qu'il se passe.


Consulter les journaux
----------------------

La première étape consiste bien souvent à regarder ce que le conteneur affiche
sur ses sorties standard et d'erreur. Lorsqu'il est lancé en monde *daemon*, il
convient d'utiliser la commande :

<div lang="en-US">
```bash
docker container logs 0a1b2c3d4e
```
</div>

Cela affichera l'intégralité des journaux depuis la création du conteneur.

En ajoutant l'option `--follow` lorsque le conteneur est actif, cela laissera
la commande active `logs` pour afficher en direct les nouvelles lignes
inscrites sur les sorties du conteneur.


Entrer dans un conteneur en cours d'exécution
---------------------------------------------

Dans certaines circonstances, les journaux ne sont pas suffisants pour déboguer
correctement l'exécution d'un conteneur.

En réalisant les exercices, vous serez sans doute confronté à des comportements
étranges, que vous ne pourriez comprendre qu'en ayant la main sur le conteneur,
à travers un shell.

Lorsqu'un conteneur est actif, vous pouvez y lancer un nouveau processus,
notamment un shell par exemple :

<div lang="en-US">
```bash
docker container exec -it mycloud /bin/bash
(inctnr)# hostname
0a1b2c3d4e5f
(inctnr)# ping mysql_cntr_name
...
```
</div>

On peut aussi imaginer lancer régulièrement une commande par ce biais : pour
déclencher une sauvegarde d'un conteneur serveur de base de données ou pour
exécuter une tâche périodique.

::::: {.question}

Il n'est pas possible d'`exec` dans un conteneur éteint. Aussi, si la commande
initiale du conteneur se termine, tous les `exec` seront instantanément tués.

:::::


Voir les processus
------------------

Si plusieurs processus doivent s'exécuter dans un conteneur, un bon moyen de
savoir s'ils sont tous actifs est d'utiliser :

<div lang="en-US">
```bash
docker container top cntr_name
```
</div>

Cela liste tous les processus rattaché au conteneur nommé : à la fois les
processus démarrés par le `run`, mais également les éventuels processus
rattachés par `exec`.


Inspecter les propriétés
------------------------

Pour aller plus loin, découvrons la commande permettant d'afficher les
propriétés internes des objets Docker.

Peut-être avez-vous encore un conteneur `youp0m` actif ?

<div lang="en-US">
```bash
docker container run -d -P registry.nemunai.re/youp0m
0a1b2c3d4e5f...

docker container inspect 0a1b2c3d4e5f
[
  {
    "Id": "0a1b2c3d4e5f...",
    "Created": "2032-06-31T18:42:23.0123456789Z",
    ...
```
</div>

Le retour de cette commande va afficher un long enregistrement JSON, rempli
d'informations importantes pour Docker. Nous y retrouvons par exemple
l'emplacement réel des points de montage du système de fichiers de notre
conteneur, les réseaux rejoints, les ports exposés, la commande par
défaut, ... et tant d'autres choses.

Du fait que la commande retourne un objet JSON, nous pouvons facilement
réutiliser cette sortie en la passant à l'utilitaire `jq` :

<div lang="en-US">
```bash
42sh$ docker container inspect 0a1b2c3d4e5f | \
    jq -r '.NetworkSettings.Ports["8080/tcp"][0].HostPort'
49153
```
</div>

Cela peut aussi être fait avec l'option `--format`, qui utilise la syntaxe des
templates du langage Go :

<div lang="en-US">
```bash
42sh$ docker container inspect 0a1b2c3d4e5f \
    -f '{{ (index (index .NetworkSettings.Ports "8080/tcp") 0).HostPort }}'
49153
```
</div>
