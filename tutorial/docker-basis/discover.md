\newpage

Découvrons Docker
=================

Tous les programmes d'exécution de conteneurs (*container runtimes*) ne
fonctionnent pas de la même manière et n'apportent pas les mêmes
fonctionnalités. Dans leurs couches les plus basses, chacun de ces programmes
va certes utiliser les mêmes fonctionnalités du système d'exploitation afin de
créer les couches d'isolation, mais chacun apporte un enrobage et une manière
d'utiliser les conteneurs différents.

Nous allons commencer sans plus attendre par découvrir Docker. Ce projet, dont
les sources ont été rendues libres en 2013, a tout de suite remporté un
engouement indéniable. Le projet a énormément grossi depuis, et il s'est aussi
bien stabilisé.

Dans ce chapitre, nous allons partir à la découverte de cet outil :
après l'avoir installé, nous apprendrons d'abord les concepts clefs puis
nous lancerons notre premier conteneur et nous irons jusqu'à déployer
un premier service web avec lequel nous pourrons interagir.
