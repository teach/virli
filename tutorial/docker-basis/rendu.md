\newpage

Projet et rendu
===============

## Projet

Écrivez un script `mycloud-run.sh` pour automatiser le lancement de votre
instance personnelle [`nextcloud`](https://hub.docker.com/_/nextcloud/) ou
d'[`owncloud`](https://hub.docker.com/r/owncloud/server/). Une attention
particulière devra être apportée à la manière dont vous gérerez le rappel du
script pour éventuellement relancer un conteneur qui se serait arrêté
(évidemment sans perdre les données).

À la fin de son exécution, le script affichera un lien utilisable sur l'hôte
pour se rendre sur la page de connexion. Une autre machine de votre réseau
local devrait également pouvoir accéder à la plate-forme, simplement en
renseignant l'IP de votre machine et en ajoutant éventuellement des règles de
pare-feu (mais cette dernière partie n'est pas demandée, gardez simplement en
tête que cela doit pouvoir être fait manuellement au cas par cas : sur une
machine sans pare-feu configurée, cela ne demande pas d'étape supplémentaire).

Votre script devra se limiter aux notions vues durant ce TP (ie. sans utiliser
`docker-compose` ou `docker stack` que l'on verra au prochain TP). Il pourra
cependant faire usage des commandes `docker OBJECT inspect` pour ne pas avoir à
faire d'analyse syntaxique sur les retours des commandes lisibles par les
humains.

Cette instance devra utiliser une base de données MySQL (lancée par vos soins
dans un autre conteneur) et contenir ses données dans un ou plusieurs volumes
(afin qu'elles persistent à une mise à jour des conteneurs par exemple).

L'exécution doit être la plus sécurisée possible (pas de port MySQL exposé sur
l'hôte par exemple, etc.) et la plus respectueuse des bonnes pratiques que l'on
a pu voir durant ce premier cours.


### Exemple d'exécution

<div lang="en-US">
```bash
42sh$ ./mycloud-run.sh
http://localhost:12345/
42sh$ #docker kill db
42sh$ ./mycloud-run.sh  # le script relancera une base de données,
                        # sans avoir perdu les données
http://localhost:12345/
```
</div>


## Modalité de rendu

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Par ailleurs, n'oubliez pas de répondre à
[l'évaluation du cours](https://www.epitaf.fr/moodle/mod/quiz/view.php?id=213).


## Tarball

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type:

<div lang="en-US">
```
login_x-TP1/
login_x-TP1/mycloud-run.sh
```
</div>

## Signature du rendu

Deux méthodes sont utilisables pour signer votre rendu :

* signature du courriel ;
* signature de la tarball.

Dans les deux cas, si vous n'en avez pas déjà une, vous devrez créer une clef
PGP à **votre nom et prénom**.

Pour valider la signature, il est nécessaire d'avoir reçu la clef publique
séparément. Vous avez le choix de l'uploader sur un serveur de clefs, soit de
me fournir votre clef en main propre, soit l'envoyer dans un courriel distinct.

### Signature du courriel

[Enigmail](https://enigmail.net) est une extension très bien réputée pour
signer ses mails depuis Thunderbird.

Utilisez le service automatique <signcheck@nemunai.re> pour savoir si votre
courriel est correctement signé et que je suis en mesure de vérifier la
signature.


### Astuces

#### No public key

Si vous recevez un rapport avec l'erreur suivante :

<div lang="en-US">
```
[FAIL] Bad signature. Here is the gnupg output:

gpg: Signature made Tue Jan 01 16:42:23 2014 CET
gpg:                using RSA key 842807A84573CC96
gpg: requesting key E2CCD99DD37BD32E from hkp server keys.openpgp.org
gpg: Can't check signature: No public key
```
</div>

C'est que votre clef publique n'est pas dans mon trousseau et que les
méthodes de récupération automatique n'ont pas permis de la
trouver. Uploadez votre clef sur [un serveur de
clefs](https://keys.openpgp.org/) ou envoyez un courriel au service
avec votre clef publique en pièce-jointe, avant de retenter votre
rendu.


#### Not explicit username

Si vous recevez un rapport avec l'erreur suivante :

<div lang="en-US">
```
[FAIL] The username of your key is not explicit, I can't find you.
```
</div>

Votre clef ne contient sans doute pas vos noms et prénoms ou l'adresse
électronique associée à la clef n'est pas celle que j'ai dans ma base de
données.


#### I've decided to skip your e-mail

Si vous recevez un rapport concluant ainsi :

<div lang="en-US">
```
After analyzing your e-mail, I've decided to SKIP it.
```
</div>

Cela signifie que la lecture de votre courriel qui a été préférée n'est pas
celle d'un rendu. Vérifiez que vous n'envoyez pas votre clef publique avec
votre rendu.
