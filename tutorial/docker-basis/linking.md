\newpage

Lier les conteneurs
===================

Lorsque l'on gère des services un peu plus complexes qu'un simple gestionnaire
d'images, on a souvent besoin de faire communiquer plusieurs services
entre eux.

Notre premier service, `youp0m`, utilisait le système de fichiers pour stocker
ses données, mais la plupart des applications réclament un serveur de base de
données.

Les bonnes pratiques nous dictent de ne pas placer plus d'un service par
conteneur : en effet, on peut vouloir mettre à jour l'applicatif sans pour
autant redémarrer sa base de données, etc. Nous allons donc voir dans cette
partie comment lier deux conteneurs.

Mais avant, regardons d'un peu plus près la gestion des réseaux par Docker.


## Les pilotes réseau

Docker propose de base trois pilotes (*drivers*) pour « gérer » cela :

- `none` : pour limiter les interfaces réseau du conteneur à l'interface de
  loopback `lo` ;
- `host` : pour partager la pile réseau avec l'hôte ;
- `bridge` : pour créer une nouvelle pile réseau par conteneur et rejoindre un
  pont réseau dédié.


Ces trois *drivers* sont instanciés de base dans Docker avec le même nom que
leur pilote. Pour consulter la liste de réseaux utilisables, lancez :

<div lang="en-US">
```
42sh$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
74cedd3ff385        bridge              bridge              local
d5d907add6e2        host                host                local
16b702ed01a0        none                null                local
```
</div>

Par défaut, c'est le réseau `bridge` (de type `bridge`) qui est employé : ce
réseau utilise le pont `docker0` que vous pouvez voir dans vos interfaces
réseau via `ip link`. C'est via ce pont que les conteneurs peuvent avoir accès
à Internet, au travers d'une couche de NAT.

Pour changer le réseau principal joint par le conteneur, utiliser l'option
`--network` du `docker container run`.


### Le réseau `host`

En utilisant ce réseau, vous abandonnez toute isolation sur cette partie du
conteneur et partagez donc avec l'hôte toute sa pile IP. Cela signifie, entre
autres, que les ports que vous chercherez à allouer dans le conteneur devront
être disponibles dans la pile de l'hôte et également que tout port alloué sera
directement accessible, sans avoir à utiliser l'option `-p` du `run`.


### Créer un nouveau réseau `bridge`

Afin de contrôler quels échanges peuvent être réalisés entre les conteneurs, il
est recommandé de créer des réseaux utilisateurs.

La création d'un réseau se fait tout simplement au travers des sous-commandes
relatives aux objets Docker `network` :

<div lang="en-US">
```bash
docker network create --driver bridge my_fic
```
</div>

C'est ensuite ce nom de réseau que vous passerez à l'option `--network` de vos
`run`, ou vous pouvez également faire rejoindre un conteneur déjà lancé à un
réseau :

<div lang="en-US">
```bash
docker network connect NETWORK CONTAINER
```
</div>

Lorsque plusieurs conteneurs ont rejoint un réseau utilisateur, ils peuvent
mutuellement se découvrir grâce à un système de résolution de nom basé sur leur
nom de conteneur.
