### Entrer dans un conteneur en cours d'exécution

Dans certaines circonstances, les journaux ne sont pas suffisants pour déboguer
correctement l'exécution d'un conteneur.

En réalisant l'exercice, vous serez sans doute confronté à des comportements
étranges, que vous ne pourriez comprendre qu'en ayant la main sur le conteneur,
via un shell.

Lorsqu'un conteneur est actif, vous pouvez y lancer un nouveau processus,
notamment un shell par exemple.

<div lang="en-US">
```bash
docker container exec -it mycloud /bin/bash
(inctnr)$ ping mysql_cntr_name
```
</div>

Notez qu'il n'est pas possible d'`exec` dans un conteneur éteint, et que si la
commande initiale du conteneur se termine, tous les `exec` seront instantanément
tués.
