\newpage

Stockage de données applicatives
================================

Il est généralement toujours possible d'écrire dans le système de fichier de
notre conteneur. (Cela n'affecte pas l'image, chaque conteneur est démarré à
partir de l'image originale.) Cependant, il n'est pas recommandé de chercher à
stocker des données ainsi. En effet, il n'est pas aisé de récupérer ces données
une fois l'exécution du conteneur terminée ; les données peuvent même être
détruite si on a lancé le conteneur avec l'option `--rm`.

Docker met donc à notre disposition plusieurs mécanismes pour que les données
de nos applications persistent et soient prêtes à être migrées d'un conteneur à
l'autre.


## Partage avec la machine hôte

Il est possible de partager un répertoire de la machine hôte avec un
conteneur. L'intérêt reste plutôt limité à des fins de facilité ou de test, par
exemple si vous voulez partager des fichiers en passant par le protocole HTTP,
mais sans se casser la tête à installer et configurer un serveur web, vous
pourriez utiliser :

<div lang="en-US">
```bash
docker run --rm -p 80:80 -v ~/Downloads:/usr/share/nginx/html:ro -d nginx
```
</div>

Une fois le conteneur lancé, vous pourrez accéder à votre dossier *Downloads*
en renseignant l'IP de votre machine dans un navigateur !

::::: {.warning}

Par défaut, `nginx` ne va pas permettre de lister le contenu du répertoire (et
va afficher une page 404, car il cherche un fichier `index.html` dans votre
répertoire). Vous pouvez par contre accéder à un fichier directement, par
exemple : <http://10.42.12.23/dQw4w9WgXcQ.mp4>

:::::

## Les volumes

Les volumes sont des espaces créés via Docker (il s'agit d'objets Docker). Ils
permettent de partager facilement des données entre conteneurs, sans avoir à se
soucier de leur réel emplacement.


Comme il s'agit d'un objet, la première chose à faire va être de créer notre
volume :

<div lang="en-US">
```bash
docker volume create prod_youp0m
docker volume create prod_foodp0m
```
</div>

Ensuite, nous pouvons démarrer un conteneur utilisant, par exemple :

<div lang="en-US">
```bash
docker container run --mount source=prod_youp0m,target=/images \
   nemunaire/youp0m
```
</div>

On pourra également faire de même avec un conteneur MySQL :

<div lang="en-US">
```bash
docker container run --name mydb -e MYSQL_ROOT_PASSWORD=my-secret-pw \
   --mount source=prod_db,target=/var/lib/mysql mysql
```
</div>

::::: {.question}

Lorsque le volume est vide, si des données sont présentes dans l'image à
l'endroit du point de montage, celles-ci sont recopiées dans le volume.

:::::

## Volumes temporaires

Lorsque vous n'avez pas besoin de stocker les données et que vous ne désirez
pas qu'elles persistent (des données sensibles par exemple) ou si cela peut
améliorer les performances de votre conteneur, il est possible de créer des
points de montages utilisant le système de fichiers `tmpfs` et donc résidant
exclusivement en RAM :

<div lang="en-US">
```bash
docker container run --mount type=tmpfs,target=/images nemunaire/youp0m
```
</div>

En cas de crash de la machine, le contenu sera perdu, mais il reste
possible d'utiliser `--volume-from` afin de partager le volume avec un
autre conteneur.


## Partage de volumes entre conteneurs

Les volumes sont des espaces détachés des conteneurs, particulièrement utiles
pour mettre à jour ou relancer un conteneur, sans perdre les données. Un autre
intérêt est de pouvoir partager des fichiers entre plusieurs conteneurs.

Il est ainsi parfaitement possible de lancer deux conteneurs qui partagent le
même volume :

<div lang="en-US">
```bash
docker container run -d --mount source=prod_youp0m,target=/images \
    -p 8080:8080 nemunaire/youp0m

docker container run -d --mount source=prod_youp0m,target=/images \
    -p 8081:8080 nemunaire/youp0m
```
</div>

Dans cet exemple, l'ajout d'une image dans un conteneur l'ajoutera également
dans le second.

Un exemple plus intéressant serait sur une architecture de micro-services
traitant des fichiers de grande taille : plutôt que de faire passer les
fichiers par un système de message/socket, on peut partager un volume pour
épargner les coûts de transferts inutiles, lorsqu'ils ne changent pas de
machine.
