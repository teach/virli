---
title: Virtualisation légère -- TP n^o^ 1
subtitle: Les bases de Docker
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 7 septembre 2022
abstract: |
  Durant ce premier TP, nous allons apprendre à utiliser Docker, puis
  nous apprendrons à déployer un groupe de conteneurs !
...
