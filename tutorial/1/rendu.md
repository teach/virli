\newpage

Projet et rendu
===============

Projet
------

Réalisez le `docker-compose.yml` permettant de lancer toute notre stack de
monitoring, d'un simple :

<div lang="en-US">
```
42sh$ docker compose up
```
</div>

Vous intégrerez les trois images (`influxdb`, `chronograf`[^onlyv1] et `telegraf`),
mettrez en place les *volumes* et *networks* nécessaires au bon fonctionnement
de la stack.

[^onlyv1]: N'ajoutez pas chronograf dans votre `docker-compose.yml` si vous
    avez opté pour la version 2 d'InfluxDB.

Le résultat final attendu doit permettre d'afficher dans `chronograf` l'hôte
auto-monitoré par la stack, sans plus de configuration. Vous aurez pour cela
besoin de placer des fichiers de configuration à côté de votre
`docker-compose.yml`, afin de pouvoir inclure ces configurations dans les
conteneurs, sans avoir besoin de reconstruire ces conteneurs.


Arborescence attendue
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires) :

<div lang="en-US">
```
./
./docker-compose.yml
./...                  # Pour les fichiers de configuration
```
</div>

Votre rendu sera pris en compte en faisant un [tag **signé par votre clef
PGP**](https://lessons.nemunai.re/keys). Consultez les détails du rendu (nom du
tag, ...) sur la page dédiée au projet sur la plateforme de rendu.
