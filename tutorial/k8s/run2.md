Pour chronograf, la commande suivante fonctionnerait, mais prenons exemple sur
le fichier YAML d'InfluxDB pour Chronograf :

```bash
kubectl create deployment chronograf --image=chronograf -- chronograf \
    --influxdb-url=http://influxdb:8086 \
    --influxdb-username=chronograf \
    --influxdb-password=eBoo8geingie8ziejeeg8bein6Yai1a
```

#### Notre application

```bash
TAG=0.1
for SERVICE in hasher rng worker; do
  kubectl create deployment $SERVICE --image=nemunaire/$SERVICE:$TAG
done
```

#### Exposer les ports

Pour trois des applications, des `ClusterIP` font l'affaire, car ils n'ont pas
besoin d'être exposés en dehors du cluster.

```bash
kubectl expose deployment influxdb --port 8086
kubectl expose deployment rng --port 80
kubectl expose deployment hasher --port 80
```

Si vous avez utilisé le *chart* Helm d'InfluxDB, Un `ClusterIP` a été
automatiquement créé.

Par contre, notre Chronograf doit être exposé, on lui alloue donc un NodePort :

```bash
kubectl create service nodeport chronograf --tcp=8888 --node-port=30001
```

À ce stade, nous devrions pouvoir accéder à l'interface de Chronograf !

Le port 30001 est exposé par `kind` (cela faisait partie des ports redirigés par
Docker entre le nœud *master* et votre machine !), nous devrions donc pouvoir
nous rendre sur : <http://localhost:30001/> pour y voir Chronograf.

Pour afficher un graphique intéressant, on se rend dans la partie
*Explore*, puis on choisit la base `chocominer.autogen`, puis la table `hashes`
et enfin on sélectionne l'élément `value`. Pour être tout à fait juste, il faut
choisir la fonction `summ`, car nous voulons afficher le nombre total de
condensat générés. Un second graphique intéressant est celui du nombre de
pépites trouvées : il faut compter (`count`) le nombre d'éléments dans la table
`chunks`.

![Montée en charge progressive dans Chronograph](nuggets-graph.png)

Vous n'avez pas la même courbe de progression ? Alors continuons pour augmenter
la puissance de notre *rig* !
