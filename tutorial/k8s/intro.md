\

![ChocoMiner](dockercoins-diagram.svg)

Fier de respecter le paradigme des micro-services, notre ChocoMiner fonctionne
ainsi :

* le `worker` demande à `rng` de générer un grand nombre aléatoire,
* le `worker` envoie ce grand nombre au `hasher`, qui lui retourne un hash,
* si le condensat respecte les contraintes pour obtenir une pépite, on est content,
* et on recommence, ainsi de suite, pour avoir le maximum de pépites.

Chaque seconde, le `worker` envoie à `influxdb` le nombre de hashs et de
pépites qu'il a ainsi pu obtenir.

Une interface graphique (`chronograf`) permet d'interroger la base de données
pour afficher des statistiques.


### Obtenir l'application

Les micro-services sont regroupés sur le dépôt suivant :

<div lang="en-US">
```shell
git clone https://git.nemunai.re/srs/chocominer.git
```
</div>


### Rappels sur la découverte de services

Dans Docker, nous avions vu que nous n'avions pas besoin de connaître les IP
des conteneurs : un serveur DNS nous permettait de se connecter aux différents
services à partir de leurs noms.

Dans Kubernetes, le même principe s'applique : dans aucun cas, nous ne devrions
inscrire en dur des adresses IP. Il convient d'utiliser au maximum le système
DNS, car les IP sont susceptibles de changer !


### Tester avec `docker-compose`

<div lang="en-US">
```bash
docker-compose up
```
</div>

Une fois le `docker-compose` lancé, nous devrions pouvoir accéder à l'interface
de `chronograf` pour voir l'avancement de recherche de pépites :

<http://localhost:8888/sources/1/dashboards/1>


### Montée en puissance

Avec `docker-compose`, on peut facilement monter en puissance. Commençons en
augmentant doucement le nombre de `worker`, pour voir si cela a un impact :

<div lang="en-US">
```bash
docker-compose up -d --scale worker=2
```
</div>

On remarque que le nombre de hashs calculés augmente ! Génial ! Continuons
d'augmenter alors :

<div lang="en-US">
```bash
docker-compose up -d --scale worker=10
```
</div>

Mais l'augmentation n'est plus aussi nette, on semble atteindre un palier au
bout d'un moment...


### Identification du goulot d'étranglement

De nombreux outils existent pour réaliser des tests de performance, essayons
`httping` sur nos différents services pour voir si un service ne serait pas
la cause des ralentissements :

- Testons `rng` : `httping -c 3 localhost:8001`,
- puis testons `hasher` : `httping -c 3 localhost:8002`.

Il semblerait que notre application `rng` nécessite d'être exécutée en
parallèle ! Mais on ne peut pas faire la répartition de charge facilement avec
`docker-compose` !
