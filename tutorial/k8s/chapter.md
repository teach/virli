Tour d'horizon de Kubernetes
============================

Nous avons abordé plusieurs aspects des conteneurs qui les rendent
particulièrement attrayants dans un environnement de production :
principalement le fait de cloisonner les environnements de telle sorte
qu’un développeur peut concevoir lui-même l’environnement d’exécution
de son application, laissant à l’équipe d’administrateurs système le
soin de gérer les machines et le système de base.

Docker est désormais considéré comme un projet mature et est digne
d’être utilisé en production.  Bien qu’il soit acceptable de déployer
des conteneurs via des outils de provisionnement tels qu’Ansible ou
Salt, on peut faire beaucoup mieux.

Kubernetes se place comme un orchestrateur de conteneur : il peut
piloter, sur une grappe de machines, le lancement, le monitoring et la
mise à jour de conteneurs. On peut ajouter ou retirer des machines au
cluster, il va toujours faire en sorte de garder l’équilibre qu’on lui
a demandé de conserver.

C’est une solution qui est particulièrement adaptée au monde des
entreprises géantes, mais elle vient avec une courbe d’apprentissage
particulièrement abrupte. Il faut bien veiller à jauger les avantages
et les inconvénients d’une telle solution avant de la choisir, car si
l’outil promet de résoudre des problèmes que vont rencontrer des très
gros sites web à fort trafic, cela peut demander aussi beaucoup
d’investissements tant humains que financiers pour des entreprises qui
voudraient juste profiter de l’outil sans réel besoin.
