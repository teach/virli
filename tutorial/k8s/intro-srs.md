\newpage

Introduction
------------

Aujourd'hui, nous allons travailler avec un mineur de pépites ... de
chocolat !

<!--Alors, on se sert un bon thé, on prend sa boîte de gâteaux pour tenir le coup,
et c'est parti !-->
Comme c'est notre dernier cours ensemble, de véritables cookies sont à
gagner pour celles et ceux qui auront amassé le plus de pépites d'ici
la fin du TP[^cookies] ! Vous pouvez suivre votre progression sur
[cette page](https://virli.nemunai.re/scores.html).

[^cookies]: dans la limite des stocks disponibles.
