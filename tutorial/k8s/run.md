Cookies dans Kube
-----------------

Maintenant que nous en savons un peu plus sur Kubernetes, nous allons commencer
à déployer notre application ChocoMiner dans notre cluster. Pour cela, nous
allons devoir :

- lancer des déploiements de ces images ;
- exposer avec un ClusterIP les services qui ont besoin de communiquer
  entre-eux ;
- exposer avec un NodePort l'interface graphique de contrôle.


### Lancement des *pod*s

#### Via Helm

[Helm](https://helm.sh/) est l'équivalent d'un gestionnaire de paquets, mais
pour Kubernetes. Nous avons pu voir dans la section précédente qu'il faut
parfois écrire des fichiers de description YAML assez volumineux (et encore,
celui du tableau de bord est tout petit !) afin de se faire comprendre de
Kubernetes.

Helm se veut donc, notamment, être un moyen de packager une application, pour
que ce soit plus simple de l'ajouter à son cluster k8s. L'[artifact
hub](https://artifacthub.io/) est une agrégation de différents dépôts,
permettant de trouver facilement son bonheur. On va y trouver
[`influxdb`](https://artifacthub.io/packages/helm/influxdata/influxdb) dont on
va avoir besoin pour la suite.

Mais d'abord, il va nous falloir [installer
Helm](https://helm.sh/docs/intro/install/). Il utilisera la même configuration
que `kubectl`, il n'y a rien de plus à configurer.

Une fois `helm` installé, et le dépôt `influxdata` ajouté, comme précisé dans
la documentation du *chart* d'InfluxDB, nous pouvons le déployer dans notre
cluster :

```bash
helm install influxdb influxdata/influxdb
```

Les valeurs de configuration indiquées dans le `README` du *chart* se modifient
ainsi :

```bash
helm upgrade -f values.yml your-influx-name influxdata/influxdb
```

Il vous sera entre autres nécessaire d'ajouter un administrateur afin de pouvoir
utiliser la base de données.

Nous pouvons ensuite faire de même avec
[Chronograf](https://artifacthub.io/packages/helm/influxdata/chronograf) ou
mixer avec la méthode ci-dessous (en adaptant certaines valeurs).


#### Via `kubectl`

Si vous ne souhaitez pas utiliser `helm`, vous pouvez vous rabattre sur les
YAML que l'on a utilisés jusqu'à maintenant, et utiliser `kubectl`. Commençons
par lancer `influxdb` :
