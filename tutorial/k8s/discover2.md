::::: {.warning}

Notez que le dashboard, avec cette configuration, va s'exécuter sans les
prérequis minimums de sécurité : pas de certificat TLS, ni
d'authentification. Ceci est juste pour jouer avec l'interface, en production,
on n'utilisera pas cette recette.

:::::

Regardons où nous pouvons contacter notre dashboard :

```bash
$ kubectl -n kubernetes-dashboard get svc
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
dashboard    NodePort    10.96.78.69   <none>        80:31505/TCP   3m10s
kubernetes   ClusterIP   10.96.0.1     <none>        443/TCP        6m51s
```

Regardons si cela répond :

```bash
$ docker exec -it kind-control-plane curl 10.96.78.69:80
   <p class="browsehappy">You are using an <strong>outdated</strong> browser.
```

Pas très sympa... il faudrait que l'on puisse le voir dans un navigateur plus
... moderne alors.

Étant donné que notre cluster ne se trouve pas directement sur notre machine,
mais dans différents conteneurs Docker, nous ne pouvons pas accéder à
`127.0.0.1`. Heureusement, au moment de la création de notre cluster avec
`kind`, nous avons renseigné plusieurs ports redirigés au sein de notre
configuration. Il va donc falloir indiquer à Kubernetes que l'on désire
utiliser un port spécifique pour exposer le tableau de bord.

Pour ce faire, éditons le fichier `dashboard-insecure.yaml`, pour ajouter, dans
la partie `Service` un *node port* plus spécifique :

```yaml
  - name: shared
    port: 80
    protocol: TCP
    targetPort: 80
    nodePort: 30002
```

Maintenant, nous n'allons pas recréer un nouveau dashboard : nous allons
simplement « appliquer » la nouvelle configuration :

```bash
kubectl apply -f my-dashboard-insecure.yaml
```

En voyant la divergence entre la réalité et la configuration demandée,
Kubernetes va tout mettre en œuvre pour se conformer à nos directives. En
l'occurrence, il s'agit de changer le port qui expose le service au sein du
cluster.

En fait, on pourra faire exactement la même chose lors d'un changement de
version. Kubernetes verra la différence et appliquera une politique de
migration déterminée.

Une fois que c'est fait, nous pouvons fièrement utiliser notre navigateur pour
aller sur l'adresse <http://localhost:30002/> (vous pouvez *Skip* l'authentification,
dans cette configuration d'exemple, elle n'est pas nécessaire).
