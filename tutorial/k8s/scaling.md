### Montée en charge

Commençons facilement, en augmentant le nombre de `workers` :

```bash
kubectl scale deploy/worker --replicas=10
```

Tout comme cela fonctionnait en partie avec `docker-compose`, on obtient ici le
même résultat. Ouf ... c'était pas trop tôt !

Nous pouvons profiter de regarder l'augmentation en direct, via la commande :

```bash
kubectl get pods -w
```

Par contre, ce ne sera pas aussi simple d'augmenter le nombre de `rng`. En
effet, il nous faut répartir les services entre plusieurs machines.


#### Daemon sets

Une ressource *daemon sets* va s'assurer que tous les nœuds (ou une partie)
vont exécuter une instance d'un *pod*. Ainsi, si un nouveau nœud rejoint le
cluster, le *pod* sera automatiquement lancé dessus. Inversement, lorsqu'un
nœud quitte le cluster, le *pod* est nettoyé.

On s'en sert principalement pour exécuter une instance de daemon de stockage
(tel que Ceph, `glusterd`, ...) ou pour la collecte de logs (`fluentd`,
`logstash`, ...), voire du monitoring (Prometheus, `collectd`, ...)

Pour créer un *daemon sets*, il est nécessaire d'écrire un fichier YAML :

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-elasticsearch
  namespace: kube-system
  labels:
	k8s-app: fluentd-logging
spec:
  selector:
	matchLabels:
	  name: fluentd-elasticsearch
  template:
	metadata:
	  labels:
	    name: fluentd-elasticsearch
	spec:
	  tolerations:
	  - key: node-role.kubernetes.io/master
	    effect: NoSchedule
	  containers:
	  - name: fluentd-elasticsearch
		image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
		resources:
		  limits:
		    memory: 200Mi
		  requests:
		    cpu: 100m
			memory: 200Mi
		volumeMounts:
		- name: varlog
		  mountPath: /var/log
	    - name: varlibdockercontainers
		  mountPath: /var/lib/docker/containers
		  readOnly: true
	  terminationGracePeriodSeconds: 30
	  volumes:
	  - name: varlog
	    hostPath:
		  path: /var/log
	  - name: varlibdockercontainers
	    hostPath:
          path: /var/lib/docker/containers
```

Ensuite, on crée le *DaemonSet* en appliquant la nouvelle spécification :

```bash
kubectl apply -f https://k8s.io/examples/controllers/daemonset.yaml
```

Pour plus d'informations, consultez [la
documentation](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/).


##### *DaemonSet* `rng`

Pour réaliser le *DaemonSet* de notre *pod* `rng`, le plus simple est de partir
d'un export de la ressource existante :

```bash
kubectl get deploy/rng -o yaml > rng.yml
```

La première chose que l'on peut faire, c'est changer le type décrit dans le
champ `kind` :

```yaml
kind: DaemonSet
```

Vous pouvez essayer d'appliquer cette spec, pour voir et essayer de tâtonner
grâce aux erreurs renvoyées par l'API.

Il vous faudra également retirer le champ `replicas` (qui n'a pas de sens ici,
vu que la réplication est basée sur les nœuds), les champs `strategy`,
`progressDeadlineSeconds`, ainsi que la ligne `status: {}`.

###### Force ! {-}

En fait, plutôt que de corriger ces erreurs, on aurait aussi très bien pu
désactiver la validation comme ceci :

```bash
kubectl apply -f rng.yml --validate=false
```


##### Trop de *pods* `rng` {-}

Après avoir appliqué la nouvelle spec, on constate qu'il y a beaucoup de *pod*s
`rng`. En effet, l'ancien *pod* déployé avec la ressource *deployment* est
toujours là.


##### Botleneck résolu ? {-}

Admirez maintenant dans Chronograf si vous avez réussi à augmenter votre nombre
de pépites !
