\newpage

Rendu
=====

Arborescence attendue
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires) :

<div lang="en-US">
```
./my-kind-cluster.yml
./my-dashboard.yaml
./influxdb.yml       # values.yml avec Helm ou le fichier passé à kubectl -f
./chronograph.yml
./daemonset-rng.yml
```
</div>
