---
title: Virtualisation légère -- TP n^o^ 6
subtitle: Kubernetes
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Vendredi 19 novembre 2021
abstract: |
  Le but de ce dernier TP est d'appréhender Kubernetes et l'orchestration de
  conteneurs.

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le mercredi 7
  décembre 2022 à 23 h 42. Consultez les sections matérialisées par un
  bandeau jaunes et un engrenage pour plus d'informations sur les
  éléments à rendre.
...
