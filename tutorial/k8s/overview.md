Vue d'ensemble de Kubernetes
----------------------------

*Kubernetes* (prononcé Ku-ber-né-tice[^prononciation-k8s] en grec) est un
système *open source* d'orchestration et de gestion de conteneurs. C'est-à-dire
qu'il se charge de faire coller constamment la liste des conteneurs qu'il voit
vivant aux spécifications qu'on lui aura demandées.

[^prononciation-k8s]: <https://github.com/kubernetes/kubernetes/issues/44308>

Ce projet est l'aboutissement de plus d'une quinzaine d'années
d'expérience de gestion de conteneurs applicatifs chez Google
(rappelons que c'est eux qui ont poussé de nombreuses technologies
dans le noyau Linux, notamment les *cgroups*, ...).

Dans Kubernetes, il n'est pas question d'indiquer comment lancer ses
conteneurs, ni même quels *cgroups* utiliser. On va fournir à l'orchestrateur
des informations, des *spécifications*, qui vont altérer l'état du cluster. Et
c'est en cherchant à être constamment dans l'état qu'on lui a décrit, qu'il va
s'adapter pour répondre aux besoins.

Par exemple, on ne va pas lui expliquer comment lancer des conteneurs
ou récupérer des images ; mais on va lui demander d'avoir 5 conteneurs
`youp0m` lancés, de placer ces conteneurs derrière un load-balancer ;
on pourra également lui demander d'adapter la charge pour absorber les
pics de trafic (par exemple lors du Black Friday sur une boutique),
mais également, on pourra gérer les mises à jour des conteneurs selon
différentes méthodes ...


### Architecture de Kubernetes

![Architecture de Kubernetes](k8s-archi.png)

Un cluster Kubernetes est composé d’un (ou plusieurs) nœuds *control-plane*, et
d’une série de nœuds *workers*.

Le(s) *control-plane(s)* sont en charge de prendre des décisions globales sur
le déroulement des opérations du cluster : cela va de la détection d'anomalies
sur le cluster ou encore le traiter d'événements et l'organisation de leur
réponse, ...

On retrouve sur ces nœuds centraux les composants suivants :

API HTTP
: On distingue plusieurs API, elles sont toutes utilisées pour communiquer avec
  le cluster, pour son administration.

L'ordonnanceur
: Il a la responsabilité de monitorer les ressources utilisées sur chaque nœud
  et de répartir les nouveaux conteneurs en fonction des ressources
  disponibles.

Les contrôleurs
: Ils vont contrôler l'état des différents composants déployées au sein du
  cluster, pour s'assurer d'être dans l'état désiré. Il y a en fait plusieurs
  contrôleurs ayant chacun la responsabilité de veiller sur une partie des
  objets, ainsi que le `cloud-controller-manager` lorsque le cluster se trouve
  chez un hébergeur cloud compatibles.

**`etcd`**
: Il s'agit d'une base de données clef/valeur, supportant la
  haute-disponibilité, que Kubernetes emploie comme système de stockage
  persistant pour les objets et ses états.
\


Chaque nœud (généralement, le nœud *master* est également *worker*) est utilisé
via deux composants :

`kubelet`
: C'est l'agent qui va se charger de créer les conteneurs et les manager, afin
  de répondre aux spécifications demandées par les *control-planes*.

`kube-proxy`
: Ce programme va servir de load-balancer pour se connecter aux conteneurs. Il
  se base généralement sur le système de filtrage de paquet du système et est
  donc amené à l'altérer.

Sans oublier le moteur de conteneurs (généralement [CRI-O](https://cri-o.io/)),
qui va effectivement se charger de lancer les conteneurs demandés par
`kubelet`.
\


Évidemment, chaque élément de l'architecture est malléable à souhait, c'est la
raison pour laquelle il peut être très difficile de mettre en place une
architecture Kubernetes : avec ou sans haute-disponibilité, avec le bon nombre
de nœuds décideurs, avec un gestionnaire de réseau qui correspond aux besoins,
avec un moteur de conteneur exotique (`rkt`, `ctr`, ...), etc.


### Modèle réseau

Pour Kubernetes, il n'y a qu'un seul gros réseau au sein duquel se retrouve
tous les conteneurs. Il ne doit pas y avoir de NAT, que ce soit entre les
*pods* ou les *nodes*, chacun doit pouvoir contacter n'importe quel autre
élément, sans qu'il y ait de routage.

C'est un modèle assez simpliste au premier abord, mais en raison de la
diversité des infrastructures et des besoins différents de chacun, de
nombreuses extensions viennent compléter ce schéma.

La [spécification
CNI](https://github.com/containernetworking/cni/blob/master/SPEC.md#network-configuration)
(pour Container Network Interface) définit l'interface commune que les plugins
doivent gérer : il s'agit de pouvoir ajouter et configurer une interface, la
supprimer ou de vérifier qu'une interface va bien.

Ainsi, à la création d'un conteneur, Kubernetes va laisser aux plugins CNI le
loisir d'ajouter les interfaces réseaux adéquates, d'allouer l'adresse IP, de
configurer les routes, les règles de pare-feu, ... quelque soit
l'infrastructure et la complexité du réseau utilisé derrière.
\

Terminons en ajoutant qu'un serveur DNS faisant autorité est nécessaire pour
que, de la même manière que Docker, il soit possible d'accéder aux autres
conteneurs via leur nom (sans qu'il ne soit nécessaire de le déclarer sur un
serveur de noms public). Il n'y a pas de projet de porté par Kubernetes pour
cela, mais cette tâche est généralement assurée par
[CoreDNS](https://coredns.io/).


### *Resources*

Avec Docker, nous avons eu l'habitude de travailler avec des objets (images,
containers, networks, volumes, secrets, ...). Au sein de Kubernetes, cela
s'appelle des *resources* et elles sont très nombreuses.

Parmi les plus courantes, citons les types (désignés *Kind* dans l'API, rien à
voir avec le projet `kind` dont on va parler par ailleurs) suivants :

node
: il s'agit d'une machine de notre cluster (elle peut être physique ou
  virtuelle).

pod
: un groupe de conteneurs travaillant ensemble. C'est la ressource que l'on
  déploie sur un *node*. Les conteneurs au sein d'un *pod* ne peuvent pas être
  séparés pour travailler sur deux *nodes* différents.

service
: c'est un point de terminaison (*endpoint*), stable dans le temps, sur lequel
  on peut se connecter pour accéder à un ou plusieurs
  conteneurs. Historiquement, appelés portails/*portals*, on les retrouve
  encore quelques fois désignés ainsi dans de vieux articles.

namespace
: à ne pas confondre avec les *namespaces* Linux. Ici il s'agit d'espaces
  nommés que Kubernetes va utiliser pour regrouper des objets ensembles.

secret
: comme `docker secret`, il s'agit d'un moyen de passer des données sensibles à
  un conteneur.

Pour voir la liste complète des *resources*, on utilise : `kubectl
api-resources`.


### Pour aller plus loin

* La documentation de Kubernetes : <https://kubernetes.io/docs/>
* Les spécifications CNI : <https://github.com/containernetworking/cni/blob/main/SPEC.md>
