\newpage

Création du cluster
-------------------

Pour travailler sur un cluster, il faut avoir plusieurs machines ... au moins
plus d'une !

Mais qui dit plusieurs machines, dit généralement de nombreuses installations à
faire, de nombreuses configurations à partager, etc. Tout ce qu'un
administrateur système redoute : du travail !

Évidemment, de nombreuses solutions existent pour travailler moins, tout en
déployant plus et de manière plus fiable ! On peut parler
d'[`ansible`](https://www.ansible.com/), de
[`chef`](https://docs.chef.io/provisioning.html), ou encore de
[`puppet`](https://puppet.com/products/capabilities/automated-provisioning), ...
Toutes ces solutions permettent d'obtenir des configurations finement
personnalisées, mais nécessitent une phase d'apprentissage plutôt lourde.

Comme nous voulons lancer des conteneurs, les machines à provisionner n'ont pas
besoin de configuration bien particulière et les seuls paquets importants sont
une version récente de `docker` et ses dépendances.


### Provisionner des machines ...

La solution magique consiste à passer par `docker-machine` pour créer des
machines virtuelles automatiquement provisionnées avec Docker.

Rien de plus simple, mais cela nécessite d'être dans un environnement dans
lequel vous maîtrisez le shell et dans lequel vous pouvez créer des machines
virtuelles. Si votre machine Linux est en fait une machine virtuelle hébergée
par VirtualBox sous Windows, vous allez sans doute préférer la deuxième
solution, que d'utiliser PowerShell et Docker4Windows[^amazon].

[^amazon]: Vous pouvez aussi vous inscrire sur [Amazon Web
    Services](https://aws.amazon.com/) <https://aws.amazon.com/>,
    `docker-machine` est capable, à partir de vos clefs d'API, de créer et
    lancer des machines dans le cloud ! Et ce n'est pas le seul service de
    cloud supporté !


##### Créer une machine {-}

Pour créer une nouvelle machine, nous allons utiliser la commande
`docker-machine create`, en prenant soin de préciser le pilote à utiliser, les
éventuelles options que prend ce pilote, ainsi que le nom que vous souhaitez
donner à cette machine (les machines ne sont pas considérées comme jetables,
leur nom vous permettra par exemple de relancer une machine plus tard) :

<div lang="en-US">
```bash
docker-machine create --driver virtualbox echinoidea
```
</div>

Consultez la section suivante, réservée aux gens qui ne peuvent pas passer par
`docker-machine`, si vous souhaitez avoir plus d'information sur ce que fait
cette commande.

##### Commandes usuelles de `docker-machine` {-}

De la même manière que `docker`, vous pouvez lister les machines connues
(`ls`), changer leur état d'exécution (`start`/`stop`/`kill`/`restart`) ou
encore supprimer une machine (`rm`).

Si vous avez besoin d'obtenir un shell : `docker-machine ssh $NAME` et
évidemment la commande `scp` s'utilise de la même manière, pour transférer des
fichiers.

##### Utilisation avec Docker {-}

Nous avons déjà évoqué le fait que le daemon pouvait ne pas se trouver sur la
même machine que le client `docker`. Eh bien avec `docker-machine` cela prend
tout son sens, car vous pouvez très facilement changer de daemon/machine avec
une simple commande :

<div lang="en-US">
```
42sh$ docker container ls -a
CONTAINER ID IMAGE            COMMAND    CREATED        STATUS

42sh$ eval $(docker-machine env echinoidea)

42sh$ docker container ls -a
CONTAINER ID IMAGE            COMMAND    CREATED        STATUS
a814293b9f45 armbuild/busybox "/bin/sh" 18 seconds ago  Up 10 minutes
0caddeed5037 armbuild/alpine  "/bin/sh" 2 weeks ago     Created
```
</div>

On remarque que le client Docker est influencé par l'exécution de notre
commande `docker-machine env`. En effet, cette commande va modifier
l'environnement (d'où le besoin d'`eval` le retour de la commande) pour faire
correspondre `DOCKER_HOST` avec le chemin où le daemon peut être contacté.

Pour reprendre le contrôle de votre environnement local, vous pouvez, soit
supprimer manuellement les variables qui ont été ajoutées par l'évaluation,
soit lancer `eval $(docker machine env -u)`.


#### Derrière Docker Machine...

Si votre environnement ne vous permet pas d'utiliser `docker-machine`, vous
pouvez vous contenter de démarrer une ou deux autres machines virtuelles sur le
même réseau que votre machine virtuelle.

Rassurez-vous, vous n'allez pas avoir besoin de refaire toute la phase
d'installation. Des contributeurs ont conçu une petite image de CD ne contenant
que le strict nécessaire pour utiliser Docker. C'est d'ailleurs cette ISO qui
est utilisée par `docker-machine` pour démarrer et configurer en un rien de
temps ses machines virtuelles.

1. Dans un premier temps, commençons par télécharger la dernière ISO de
   `boot2docker.iso` :\
   <https://github.com/boot2docker/boot2docker/releases/latest>
1. Ensuite, dans notre hyperviseur, créons deux nouvelles machines, avec
   suffisamment de ressources pour pouvoir lancer des conteneurs. Ce n'est pas
   parce que l'image que l'on va démarrer est petite que l'on ne va pas
   pouvoir allouer beaucoup d'espace disque ou de RAM.
1. Lançons ensuite nos deux images, configurées pour démarrer sur l'image ISO
   que l'on a téléchargée précédemment.
1. Faisons un `ip a` pour connaître l'IP de la machine, nous devrions pouvoir
   nous y connecter en SSH avec l'utilisateur `docker` dont le mot de passe est
   `tcuser`.

Vous constaterez que le daemon `dockerd` est déjà lancé. `docker` est déjà
pleinement utilisable dans cette machine !


#### (Optionnel) Déporter le client Docker

Dans la suite, nous n'allons taper que quelques commandes dans nos
machines virtuelles, il n'est donc pas primordial d'avoir configuré son
environnement pour utiliser localement les daemons Docker des machines
virtuelles. Néanmoins, cela ne coûte rien de voir les procédures mise en œuvre.

Commençons par voir sur quel port le daemon `dockerd` de notre machine
virtuelle écoute :

<div lang="en-US">
```bash
(virt1) 42sh$ netstat -tpln | grep dockerd
Proto Recv-Q Send-Q Local Address   Foreign Address   PID/Program name
tcp        0      0 :::2376         :::*              980/dockerd
```
</div>

Essayons de renseigner simplement cette configuration à notre client Docker :

<div lang="en-US">
```bash
(main) 42sh$ docker -H tcp://$VM1_IP:2376/ info
Get http://$VM1_IP:2376/v1.32/info: net/http: transport connection broken
* Are you trying to connect to a TLS-enabled daemon without TLS?
```
</div>

En effet, Docker met tout en œuvre pour rendre compliquée l'utilisation
non-sécurisée de la plate-forme. Mais ils font également en sorte que la
sécurité soit la plus transparente et la moins contraignante possible pour
l'utilisateur, sans pour autant être faible.

Afin de contacter un daemon Docker distant, il est nécessaire présenter un
certificat client TLS approuvé par ce daemon. Il est seulement nécessaire de
vérifier le certificat présenté par le daemon, comme dans le cadre d'une
connexion SSH classique.

Tout le nécessaire est déjà configuré au sein de `boot2docker`, pour nos tests,
nous n'avons qu'à recopier la clef et les certificats en place.

<div lang="en-US">
```bash
(main) 42sh$ mkdir remote/virt1
(main) 42sh$ scp "docker@$VM1_IP:.docker/*" remote/virt1
ca.pem
cert.pem
key.pem
```
</div>

Tentons maintenant de nous connecter au daemon distant utilisant ces éléments :

<div lang="en-US">
```bash
DOCKER_CERT_PATH=remote/vir1/ docker -H tcp://$VM1_IP:2376/ --tlsverify info
```
</div>

Nous pouvons effectuer la même opération pour la seconde machine virtuelle.
