---
title: Virtualisation légère -- TP n^o^ 2.3
subtitle: L'orchestration avec Docker
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 18 octobre 2018
abstract: |
  Dans la troisième partie de ce TP, nous allons orchestrer nos
  conteneurs !

  \vspace{1em}

  Tous les éléments de ce TP (exercices et projet) sont à rendre à
  <virli@nemunai.re> au plus tard le mercredi 24 octobre 2018 à 0
  h 42. Consultez la dernière section de chaque partie pour plus
  d'information sur les éléments à rendre.

  En tant que personnes sensibilisées à la sécurité des échanges
  électroniques, vous devrez m'envoyer vos rendus signés avec votre
  clef PGP. Pensez à
  [me](https://pgp.mit.edu/pks/lookup?op=vindex&search=0x842807A84573CC96)
  faire signer votre clef et n'hésitez pas à [faire signer la
  vôtre](https://www.meetup.com/fr/Paris-certification-de-cles-PGP-et-CAcert/).
...
