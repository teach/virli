#!/bin/sh

influxd &
WAIT=$!

/opt/chronograf/chronograf &
WAIT="${WAIT} $!"

wait ${WAIT}
