\newpage

Projet et rendu
===============

Projet
------

À partir du `docker-compose.yml` écrit dans la partie précédente, ajoutez des
contraintes de déploiement pour avoir 1 `telegraf` par nœud, rapportant à la
même base `influx`. N'oubliez pas de définir les `restart_policy`.


Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.


Tarball
-------

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
login_x-TP2/
login_x-TP2/mymonitoring-stack.yml
```
</div>

Utilisez la même tarball pour le rendu que pour les parties précédentes.
