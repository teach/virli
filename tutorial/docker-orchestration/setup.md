Mise en place
-------------

Pour cette partie, nous allons avoir besoin de `docker-machine`, installons-le sur
notre machine hôte, même si ce n'est pas un Linux : le but va être de lancer
plusieurs machines virtuelles dédiées à Docker pour simuler un cluster.

Ce programme permet de simplifier la gestion de multiples environnements
Docker, comme par exemple lorsque l'on souhaite gérer un cluster de machines
pour un projet.

Ainsi, il est possible de provisionner et gérer des machines hôtes sur les
plates-formes de cloud habituelles. C'est également ce projet qui est à la base
de *Docker Dektop*, en permettant de lancer via, respectivement, VirtualBox ou
Hyper-V, un environnement Linux prêt à être utilisé avec Docker.

### Par la distribution binaire

L'équipe en charge de `docker-machine` met à disposition un exécutable compilé
pour bon nombre d'environnements. Nous pouvons l'installer en suivant la
procédure suivante :

<div lang="en-US">
```bash
V=0.16.2
P=docker-machine-`uname -s`-`uname -m`
curl -L https://github.com/docker/machine/releases/download/v${V}/${P} \
    > /usr/bin/docker-machine
chmod +x /usr/bin/docker-machine
```
</div>


### Support de KVM

Le programme support de base de nombreux environnement, dont VirtualBox et
Hyper-V. Bien d'autres environnements peuvent être supportés, au moyen de
plug-ins.

Si vous utilisez KVM comme hyperviseur, vous allez avoir besoin d'installer le
plugin
[`docker-machine-kvm`](https://github.com/machine-drivers/docker-machine-kvm). Vous
n'aurez qu'à suivre les instructions du `README` :\
<https://github.com/machine-drivers/docker-machine-kvm/>

Les autres plugins sont disponibles au sein de l'organisation Machine-Driver
sur GitHub :\
<https://github.com/machine-drivers/>

### Vérification du fonctionnement

Comme avec Docker, nous pouvons vérifier le bon fonctionnement de
`docker-machine` en exécutant la commande :

<div lang="en-US">
```
42sh$ docker-machine version
docker-machine version 0.16.2, build 9371605
```
</div>


### Play With Docker

Si vous avez des difficultés pour réaliser les exercices sur votre machine,
vous pouvez utiliser le projet [Play With
Docker](https://play-with-docker.com/) qui vous donnera accès à un bac à sable
avec lequel vous pourrez réaliser tous les exercices de cette partie.
