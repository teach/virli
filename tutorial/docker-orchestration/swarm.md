\newpage

Déploiement de conteneurs
-------------------------

Regroupés au sein d'un cluster (on parle de *swarm* pour Docker), chaque
*Docker engine* représente un nœud (*node*) dans lequel on va déployer des
*services*.

Certain nœuds ont un rôle de *manager*, parmi ceux-ci, un seul est élu leader et
prendra les décisions d'orchestration. Les autres sont là pour prendre le
relais en cas de dysfonctionnement sur le manager élu.

Les services sont un groupe de tâches (typiquement une image avec sa ligne de
commande à lancer) que le `manager` va conserver à l'état désiré. Le service va
indiquer par exemple le nombre d'instances que l'on désire avoir dans le
cluster. En cas de crash d'un manager ou d'un *worker*, le (nouveau) manager
fera en sorte de redéployer les conteneurs perdus, afin de toujours maintenir
le cluster dans l'état désiré.

En termes de cluster, des tâches sont l'équivalent des conteneurs, hors de
Swarm.


### Création du cluster Swarm

#### Initialisation du cluster

La première chose à faire est d'initialiser un cluster Swarm. Pour ce faire,
ce n'est pas plus compliqué que de faire :

<div lang="en-US">
```bash
docker swarm init
```
</div>

Cela va créer un cluster mono-nœud, sur la machine courante (celle pointée par
la variable `DOCKER_HOST`). Ce nœud est automatiquement passé *manager* du
cluster, afin de pouvoir lancer des tâches ... au moins sur lui-même en
attendant d'autres nœuds.


#### Rejoindre un cluster

Pour rejoindre un cluster, il est nécessaire d'avoir le jeton associé à ce
cluster.

La commande pour rejoindre un cluster et contenant le jeton est indiquée
lorsque vous initialisez le cluster. Si vous avez raté la sortie de la
commande, vous pouvez retrouver le jeton avec :

<div lang="en-US">
```bash
docker swarm join-token worker
```
</div>

Lançons maintenant la commande `join` indiquée, sur une autre machine, en
utilisant `docker-machine`.

::::: {.warning}
Si vous utilisez Docker dans une VM, il faut que celle-ci soit
configurée en mode bridge pour qu'elle soit sur le même sous-réseau. Il n'y
a pas de problème à avoir des nœuds *workers* derrière un NAT, mais il est
primordial que les managers soient joignables. Vous pouvez tenter de faire
des redirections de ports, mais le résultat n'est pas garanti !
:::::

<div lang="en-US">
```bash
eval $(docker-machine env echinoidea)
docker swarm join --token SWMTKN-1-...-... 10.10.10.42:2377
```
</div>

Une fois rejoint, vous devriez voir apparaître un nouveau nœud *worker* dans :

<div lang="en-US">
```
42sh$ eval $(docker-machine env -u)

42sh$ docker node ls
ID                         HOSTNAME    STATUS  AVAILABILITY  MANAGER STATUS
y9skzvuf989hjrkciu8mnsy    echinoidea  Ready   Active
ovgh6r32kgcbswb2we48br1 *  wales       Ready   Active        Leader
```
</div>


#### Rejoindre en tant que manager

Lorsqu'il est nécessaire d'avoir de la haute-disponibilité, on définit
plusieurs managers. Cela permet qu'en cas de dysfonctionnement du manager, un
nouveau manager soit élu.

Nous n'allons pas faire rejoindre de manager supplémentaire à ce stade, mais
vous pouvez facilement expérimenter la chose en lançant 5 machines virtuelles
et en définissant trois des cinq machines comme étant managers.

Pour que l'algorithme de consensus fonctionne de manière optimale, il convient
de toujours avoir un nombre impair de managers : Docker en recommande 3
ou 5. La pire configuration est avec deux managers, car si l'un des deux tombe,
il ne peut pas savoir si c'est lui qui est isolé (auquel cas il attendrait
d'être à nouveau en ligne avant de se proclamer leader) ou si c'est l'autre qui
est tombé. Avec trois managers, en cas de perte d'un manager, les deux restants
peuvent considérer qu'ils ont perdu le troisième et peuvent élire le nouveau
manager entre eux et continuer à faire vivre le cluster.
