### Vers le déploiement

Nous n'allons pas faire le déploiement aujourd'hui, car nous n'avons pas
d'environnement de production sur lequel déployer notre service.

Vous pouvez néanmoins tester les plugins
[`scp`](http://plugins.drone.io/appleboy/drone-scp/) ou
[`ansible`](http://plugins.drone.io/drone-plugins/drone-ansible/) si vous avez
une machine virtuelle avec une connexion SSH. N'hésitez pas à l'ajouter à votre
`.droneci.yml`.


### Profitons !

Sonarqube a repéré quelques erreurs dans le code de `youp0m`, essayez de les
corriger, et publiez une nouvelle version, pour observer toute la chaîne en
action !
