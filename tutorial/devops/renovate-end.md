Dès que le conteneur sera lancé, nous devrions voir apparaître une ou plusieurs
*pull-requests* pour le projet `youp0m`. Si votre CI est configurée
correctement, des tests automatiques seront lancés.

Le conteneur s'arrête dès qu'il a terminé d'analysé tous les dépôts. Vous
devrez le relancer si vous attendez une nouvelle action de la part de
Renovatebot. Il est courant de le lancer entre chaque heure et 2 ou 4 fois par
jour.

De très nombreuses options permettent d'adapter le comportement de Renovatebot
aux besoins de chaque projet. La configuration se fait au travers d'un fichier
`renovate.json` qui se trouve généralement à la racine du dépôt.

Pour avoir un aperçu de toutes les possibilités offertes par renovatebot,
consultez la liste des éléments de configuration :
<https://docs.renovatebot.com/configuration-options/>
\

Ne soyez pas effrayé par la liste interminable d'options. Il est vrai que la
première fois, on peut se sentir submergé de possibilités, mais il faut noter
que le projet arriver avec des options par défaut plutôt correctes, et que l'on
peut facilement avoir une configuration commune pour tous nos dépôts, à travers
les *presets*.

Un certain nombre de *presets* sont distribués par défaut, voici la liste
(humainement lisible cette fois) :
<https://docs.renovatebot.com/presets-default/>

Voici un exemple de configuration que vous pouvez utilisé comme base de tous
vos projets :

<div lang="en-US">
```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "local>renovate/renovate-config"
  ]
}
```
</div>

Ceci ira chercher une configuration dans le fichier `default.json` du dépôt
`renovate/renovate-config`. À condition que votre utilisateur Renovatebot
s'appelle effectivement `renovate`.

Voici un exemple de fichier `default.json` que vous pourriez vouloir utiliser :

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "packageRules": [
    {
      "description": "Opt-out minimum Go version updates",
      "matchManagers": ["gomod"],
      "matchDepTypes": ["golang"],
      "enabled": false
    }
  ],
  "extends": [
    "config:base",
    ":dependencyDashboard",
    ":enableVulnerabilityAlertsWithLabel('security')",
    "group:recommended"
  ]
}
```

Attention, on ne le répétera jamais assez, mais Renovatebot peut vite devenir
infernal, car il va créer de nombreuses *pull-requests*, inlassablement. Il
convient de rapidement activer la fusion automatique des mises à jour pour
lesquelles vous avez confiances et pour lesquelles vous ne feriez qu'appuyer
sur le bouton de fusion, sans même tester vous-même. La fonctionnalité est
décrite en détail dans la documentation[^RENOVATE_AUTOMERGE] et explique les
différentes stratégies. Néanmoins, il est nécessaire d'avoir une bonne suite de
tests avant d'envisager d'utiliser une telle fonctionnalité.

[^RENOVATE_AUTOMERGE]: <https://docs.renovatebot.com/key-concepts/automerge/>
