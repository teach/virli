### Inspection qualité

`youp0m` n'a pas de suite de tests fonctionnels, mais nous allons utiliser
[Sonarqube](https://www.sonarqube.org/) pour faire une revue qualité du code !

Tout d'abord, il faut lancer le conteneur Sonarqube :

<div lang="en-US">
```bash
docker run --rm -d --name sonarqube --network drone -p 9000:9000 sonarqube
```
</div>

Le service met un bon moment avant de démarrer, dès qu'il se sera initialisé,
nous pourrons accéder à l'interface sur <http://localhost:9000>.

::::: {.exercice}

En attendant qu'il démarre, nous pouvons commencer à ajouter le nécessaire à
notre `.drone.yml` : <http://plugins.drone.io/aosapps/drone-sonar-plugin/>.

:::::

Après s'être connecté à Sonarqube (`admin:admin`), nous pouvons aller générer
un token, tel que décrit dans la [documentation du plugin
Drone](http://plugins.drone.io/aosapps/drone-sonar-plugin/).

Une fois la modification *commitée* et poussée, Drone enverra le code à Sonarqube
qui en fera une analyse minutieuse. Rendez-vous sur
<http://127.0.0.1:9000/projects> pour admirer le résultat.
