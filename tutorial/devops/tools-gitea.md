## Gestionnaire de versions

Avant de pouvoir commencer notre aventure, il est nécessaire d'avoir un
gestionnaire de versions. Nous allons ici utiliser Git.


### Problématique du stockage des produits de compilation

Outre les interfaces rudimentaires fournies au-dessus de Git (gitweb[^GITWEB],
gitolite[^GITOLITE], ...), il y a de nombreux projets qui offrent davantage que
le simple hébergement de dépôts. Vous pouvez voir sur GitHub notamment qu'il
est possible d'attacher à un tag un [certain nombre de
fichiers](https://github.com/docker/compose/releases/latest).
Mais cela ne s'arrête pas là puisque [depuis 2020 pour
GitHub](https://github.blog/2020-09-01-introducing-github-container-registry/)
et [2016 pour
GitLab](https://about.gitlab.com/blog/2016/05/23/gitlab-container-registry/),
ces gestionnaires de versions intégrent carrément un registre Docker.

[^GITWEB]: <https://git.wiki.kernel.org/index.php/Gitweb>
[^GITOLITE]: <https://github.com/sitaramc/gitolite>

En effet, la problématique du stockage des produits de compilation est
vaste. Si au début on peut se satisfaire d'un simple serveur web/FTP/SSH pour
les récupérer manuellement, on a vite envie de pouvoir utiliser les outils
standards directement : `docker pull ...`, `npm install ...`, ...

Des programmes et services se sont spécialisés là-dedans, citons notamment
[Artifactory](https://jfrog.com/artifactory/) ou [Nexus
Repository](https://www.sonatype.com/nexus/repository-oss) et bien d'autres.


### Installation et configuration

Aller c'est parti ! première chose à faire : installer et configurer
[Gitea](https://gitea.io/).

Nous allons utiliser l'image :
[`gitea/gitea`](https://hub.docker.com/r/gitea/gitea).
