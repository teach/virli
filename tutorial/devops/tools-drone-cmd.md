Tout comme Gitea, Drone tire un certain nombre de paramètres depuis son
environnement. Nous allons donc commencer par indiquer l'identifiant et le
secret de l'application que l'on a créés précédemment dans Gitea :

<div lang="en-US">
```shell
export DRONE_GITEA_CLIENT_ID=#FIXME
export DRONE_GITEA_CLIENT_SECRET=#FIXME
```
</div>

Puis, tout comme pour Gitea, nous allons générer un secret. Ce secret est
utilisé par les différents *worker*s pour s'authentifier :

<div lang="en-US">
```shell
export DRONE_RPC_SECRET=$(openssl rand -base64 30)
```
</div>

Lançons enfin Drone avec les deux commandes suivantes :

<div lang="en-US">
```shell
docker volume create drone_data

docker container run --name droneci -v drone_data:/data --network my_ci_net \
    -p 80:80 -e DRONE_GITEA_CLIENT_ID -e DRONE_GITEA_CLIENT_SECRET \
    -e DRONE_GITEA_SERVER=http://gitea:3000 -e DRONE_SERVER_PROTO=http \
    -e DRONE_RPC_SECRET -e DRONE_SERVER_HOST=droneci -d \
    drone/drone:2
```
</div>

Gardez la variable d'environnement `DRONE_RPC_SECRET` dans un coin, nous en
aurons encore besoin juste après.
