Commençons par créer un nouveau volume `gitea-data`, celui-ci contiendra
les données de `gitea` (nos dépôts Git, mais également la configuration propre
à `gitea`) :

<div lang="en-US">
```shell
docker volume create gitea-data
```
</div>

Afin de simplifier l'installation de notre conteneur, nous allons utiliser un
maximum de paramètres par défaut. Il faudra toutefois générer deux clefs
secrètes propres à l'installation. Puisque c'est temporaire, on peut se
contenter de ne pas les stocker (elles seront perdues si on ferme notre
terminal) :

<div lang="en-US">
```shell
export GITEA__security__SECRET_KEY=$(openssl rand -base64 30)
export GITEA__security__INTERNAL_TOKEN=$(docker run --rm  gitea/gitea:1 gitea generate secret INTERNAL_TOKEN)
```
</div>

Pour finir, lançons notre conteneur `gitea` :

<div lang="en-US">
```shell
docker container run --name gitea --network my_ci_net -p 2222:22 \
    -p 3000:3000 -v /etc/localtime:/etc/localtime:ro -v gitea-data:/data \
    -v /etc/timezone:/etc/timezone:ro -e RUN_MODE=prod \
    -e DOMAIN=gitea -e SSH_DOMAIN=gitea -e INSTALL_LOCK=true \
    -e GITEA__security__SECRET_KEY -e GITEA__security__INTERNAL_TOKEN \
    -e GITEA__webhook__ALLOWED_HOST_LIST=private,external -d \
    gitea/gitea:1
```
</div>
