### Définir les étapes d'intégration

Nous allons devoir rédiger un fichier `drone.yml`, que l'on placera à la racine
du dépôt. C'est ce fichier qui sera traité par DroneCI pour savoir comment
compiler et tester le projet.

::::: {.warning}
Un fichier `drone.yml` existe déjà à la racine du dépôt. Celui-ci pourra vous
servir d'inspiration, mais il ne fonctionnera pas directement dans votre
installation.

**Vous rencontrerez des problèmes inattendus si vous utilisez le fichier
`.drone.yml` du dépôt.** Vous **DEVEZ** partir d'un fichier vide et suivre la
documentation pour obtenir un `.drone.yml` fonctionnel.
:::::

Toutes les informations nécessaires à l'écriture du fichier `.drone.yml` se
trouvent dans l'excellente documentation du projet :\
<https://docs.drone.io/pipeline/docker/examples/languages/golang/>.
