\newpage

## Intégration continue

Une fois Gitea et Drone installés et configurés, nous allons pouvoir rentrer
dans le vif du sujet : faire de l'intégration continue sur notre premier projet !

### Créez un dépôt pour `linky2influx`

::::: {.exercice}

Après avoir créé (ou migré pour les plus malins !) le dépôt
[`linky2influx`](https://git.nemunai.re/nemunaire/linky2influx) dans gitea,
synchronisez les dépôts dans Drone, puis activez la surveillance de `linky2influx`.

:::::
