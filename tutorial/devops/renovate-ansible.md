Voici à quoi pourrait ressembler le playbook Ansible démarrant notre conteneur
Renovatebot :

<div lang="en-US">
```yaml
- name: Launch renovate container
  docker_container:
    name: renovate
    image: renovate/renovate
    state: started
    restart_policy: no
    memory: 2G
    networks:
      - name: gitea_net
    env:
      RENOVATE_ENDPOINT: "http://gitea:3000/api/v1/"
      RENOVATE_PLATFORM: "gitea"
      RENOVATE_TOKEN: "{{ renovate_token }}"
      RENOVATE_GIT_AUTHOR: "Renovatebot <renovate@gitea.sample>"
      RENOVATE_AUTODISCOVER: "true"
      RENOVATE_LOG_LEVEL: "info"
```
</div>
