### Publier pour plusieurs architectures ?

Le compilateur Go est fourni avec l'ensemble des backends des différentes
architectures matérielles qu'il supporte, nous pouvons donc aisément faire de
la compilation croisée pour d'autres architectures.

Essayons maintenant de compiler `linky2influx` pour plusieurs architectures afin de
vérifier que cela fonctionne bien !

Un exemple est donné tout en haut de cette page :
<https://docs.drone.io/pipeline/environment/syntax/>.

En faisant varier `$GOARCH` en `mips`, `powerpc`, ... nous pouvons générer les
binaires correspondant à chaque architecture et système.

Ajoutez à votre fichier `.drone.yml` les deux architectures pour lesquelles les
Raspberry Pi d'Électropcool sont compatibles. Nommez les fichiers selon
l'architecture que pourrait renvoyer `uname -m`, cela pourrait nous simplifier la tâche ensuite...
\

Une fois taggé et poussé sur notre dépôt, nous aurons une version exécutable
utilisable à la fois sur notre machine de développement, mais aussi sur les
Raspberry Pi déjà déployées dans les bâtiments.

Nous avons atteint l'un des deux objectifs qui nous était demandé. Tâchons
maintenant de trouver une manière de faciliter la distribution du binaire.
