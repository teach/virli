\

Voici la ligne de commande permettant de démarrer notre agent :

<div lang="en-US">
```shell
docker container run --name droneci-runner -d --network my_ci_net \
  -v /var/run/docker.sock:/var/run/docker.sock -e DRONE_RUNNER_CAPACITY=2 \
  -e DRONE_RPC_HOST=droneci -e DRONE_RPC_SECRET -e DRONE_RPC_PROTO=http \
  -e DRONE_RUNNER_NAME=my-runner -e DRONE_RUNNER_NETWORKS=my_ci_net \
  drone/drone-runner-docker:1
```
</div>
