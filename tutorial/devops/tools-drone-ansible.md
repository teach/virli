Voici à quoi pourrait ressembler le playbook Ansible démarrant notre conteneur
Drone :

<div lang="en-US">
```yaml
- name: Launch drone container
  docker_container:
    name: droneci
    image: drone/drone:2
    volumes:
      - /var/lib/drone:/data
    state: started
    restart_policy: unless-stopped
    memory: 1G
    memory_swap: 1G
    networks:
      - name: drone_net
      - name: gitea_net
    published_ports:
      - "80:80"
    env:
      DRONE_GITEA_CLIENT_ID: "{{ client.id }}"
      DRONE_GITEA_CLIENT_SECRET: "{{ client.secret }}"
      DRONE_GITEA_SERVER: "http://gitea:3000"
      DRONE_RPC_SECRET: "{{ shared_secret }}"
      DRONE_SERVER_HOST: "droneci"
      DRONE_SERVER_PROTO: "http"
```
</div>

C'est à vous de définir un `shared_secret`, il s'agit d'une chaîne aléatoire
qui permettra aux *Runner*s (section suivante) de s'authentifier.
