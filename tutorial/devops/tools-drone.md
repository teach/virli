## Logiciel d'intégration continue

De nombreuses solutions sont disponibles sur Internet, la plupart du temps
gratuites pour les projets libres ([Travis CI](https://travis-ci.org/),
[CircleCI](https://circleci.com/), ...).

Mais nous allons déployer notre propre solution, en utilisant [Drone
CI](https://drone.io/). C'est une solution d'intégration continue libre et
moderne, conçue tout autour de Docker. Idéale pour nous !

Deux conteneurs sont à lancer : nous aurons d'un côté l'interface de contrôle
et de l'autre un agent (*runner* dans le vocabulaire de Drone) chargé
d'exécuter les tests. Dans un environnement de production, on aura généralement
plusieurs agents, et ceux-ci seront situés sur des machines distinctes.

### Interface de contrôle et de dispatch des tâches

La documentation du projet est extrêmement bien faite, suivons la marche à
suivre pour [relier Gitea à
Drone](https://docs.drone.io/server/provider/gitea/).

Drone va avoir besoin d'authentifier les utilisateurs afin d'accéder aux dépôts
privés (avec l'autorisation des utilisateurs). Pour cela, comme l'indique la
documentation de Drone, on va utiliser OAuth2 : dans Gitea, il va falloir créer
une *application OAuth2*. Le formulaire de création se trouve dans la
configuration du compte utilisateur, sous l'onglet *Applications*.

Drone aura également besoin d'une URL de redirection. Dans notre cas,
ce sera :\
`http://droneci/login`.
