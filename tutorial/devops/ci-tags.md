### Publier le binaire correspondant aux tags/jalons

Nous savons maintenant que notre projet compile bien dans un environnement
différent de celui du développeur ! Néanmoins, le binaire produit est perdu dès
lors que la compilation est terminée, car nous n'en faisons rien.

::::: {.exercice}

Ajoutons donc une nouvelle règle à notre `.droneci.yml` pour placer le binaire
au sein de la liste des fichiers téléchargeables aux côtés des tags.

Vous aurez sans doute besoin de :

  - <https://docs.drone.io/pipeline/conditions/>
  - <http://plugins.drone.io/drone-plugins/drone-gitea-release/>

::::: {.warning}

#### Attention à ne pas stocker votre clef d'API dans le fichier YAML ! {-}
\

Lorsque l'on est plusieurs à travailler sur le projet ou pour accroître la
sécurité, il convient de créer, un compte *bot* qui sera responsable de la
création des *releases*. Ce sera donc sa clef d'API que l'on indiquera dans
l'interface de Drone.

Pour notre exercice, nous n'avons pas besoin de créer un utilisateur dédié,
mais il est impératif d'utiliser les *secrets* de Drone pour ne pas que la clef
d'API apparaisse dans l'historique du dépôt.

:::::

:::::

![Binaire publié automatiquement sur Gitea](tag-released.png){height=8cm}
