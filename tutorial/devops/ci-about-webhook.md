::::: {.question}

#### Que fait Drone pour « surveiller » un dépôt ? {-}
\

Grâce aux permissions de que Drone a récupéré lors de la connexion OAuth à
Gitea, il peut non seulement lire et récupérer le code des différents dépôts
auxquels vous avez accès, mais il peut aussi changer certains paramètres.

L'activation d'un dépôt dans Drone se traduit par la configuration d'un
*webhook* sur le dépôt en question. On peut le voir dans les paramètres du
dépôt, sous l'onglet *Déclencheurs Web*.

:::::

![L'onglet des *webhooks* dans Gitea](gitea-webhooks.png){height=6cm}

À chaque fois qu'un événement va se produire sur le dépôt, Gitea va prévenir
Drone qui décidera si l'évènement doit conduire à lancer l'intégration continue
ou non, selon les instructions qu'on lui a donné dans la configuration du
dépôt.
