\newpage

But du TP
=========

Nous allons nous mettre aujourd’hui dans la peau d’une équipe DevOps et
réaliser une solution complète d’intégration/déploiement continu (le fameux
CI/CD, pour *Continuous Integration* et *Continuous Delivery*).

Le résultat attendu d’ici la fin de cette partie sera de mettre en place toutes
les briques décrites dans la section précédente.
\

Nous allons pour cela automatiser le projet `youp0m`.

Il est également attendu que vous rendiez un playbook Ansible, permettant de
retrouver un environnement similaire. Car on pourra s’en resservir au prochain
cours.
\

Dans un premier temps, on voudra juste compiler notre projet, pour s’assurer
que chaque commit poussé ne contient pas d’erreur de compilation (dans
l’environnement défini comme étant celui de production).  Ensuite, on ajoutera
quelques tests automatiques. Puis nous publierons automatiquement le binaire
`youp0m` comme fichier associé à un tag au sein de l’interface web du
gestionnaire de versions.

Enfin, `youp0m` produisant une image Docker, en plus de publier le binaire,
nous publierons l'image produite sur un registre Docker. C’est à partir de
cette image Docker que l’on va commencer à déployer automatiquement...
