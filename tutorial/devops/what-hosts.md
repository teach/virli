Étant donné que votre machine ne dispose pas de domaine sur Internet et que
l'on va essayer de simplifier au maximum l'installation, vous devriez ajouter
cette ligne à votre fichier `/etc/hosts` (ou
`\Windows\System32\drivers\etc\hosts`) :

<div lang="en-US">
```conf
127.0.0.1   gitea droneci
```
</div>

Cette ligne va vous permettre de résoudre les noms des conteneurs. Cela
permettra aux requêtes OAuth de se faire de manière transparente pour vous
lorsque vous serez dans votre navigateur.
