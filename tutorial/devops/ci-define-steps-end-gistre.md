Les étapes sont sensiblement les mêmes que dans le `Dockerfile` présent sur le
dépôt.

Committons puis poussons notre travail. Dès qu'il sera reçu par Gitea, nous
devrions voir l'interface de Drone lancer les étapes décrites dans le fichier.

![Drone en action](drone-run-linky.png){height=7.5cm}

::::: {.warning}
**IMPORTANT :** si vous avez l'impression que ça ne marche pas et que vous avez
réutilisé le fichier présent sur le dépôt au lieu de partir de l'exemple donné
dans la documentation, **commencez en partant de l'exemple de la
documentation** ! Le fichier présent sur le dépôt **ne fonctionnera pas** dans
votre situation !
:::::

Lorsqu'apparaît enfin la ligne `git.nemunai.re/linky2influx`, le projet est compilé !
