::::: {.more}

On remarquera que l'on partage notre socket Docker : l'exécution de code
arbitraire n'aura pas lieu directement dans le conteneur, en fait il s'agit
d'un petit orchestrateur qui lancera d'autres conteneurs en fonction des tâches
qu'on lui demandera. Le code arbitraire sera donc toujours exécuté dans un
conteneur moins privilégié.

:::::
