::::: {.warning}

Pour des raisons de sécurité, les *webhooks* ne peuvent, par défaut, être
appelés que vers des IP publiques d'Internet (afin d'éviter de pouvoir explorer
le réseau local par ce biais).

Étant donné que nous faisons tout localement entre deux conteneurs, nous allons
nécessairement utiliser des IP privées, c'est pourquoi il est nécessaire de
changer la valeur du paramètre `ALLOWED_HOST_LIST` pour autoriser les IP
publiques (`external`, la valeur par défaut), mais également `private`.

<https://docs.gitea.io/en-us/config-cheat-sheet/#webhook-webhook>

:::::

Une fois le conteneur lancé, vous pouvez accéder à l'interface de votre
gestionnaire de versions sur le port 3000 de votre machine (à moins que vous
n'ayez opté pour un autre port).
\

Vous pouvez ajouter un nouvel administrateur avec la commande suivante :

<div lang="en-US">
```bash
docker exec -u git gitea gitea admin user create --username "${USER}" --admin \
  --must-change-password=false --random-password --email "${USER}@exmpl.tf"
```
</div>

Notez le mot de passe généré pour ensuite vous y connecter.
