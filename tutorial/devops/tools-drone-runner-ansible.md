Voici à quoi pourrait ressembler le playbook Ansible démarrant notre agent Drone :

<div lang="en-US">
```yaml
- name: Launch drone runer
  docker_container:
    name: droneci-runner
    image: "drone/drone-runner-docker:1"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    state: started
    restart_policy: unless-stopped
    memory: 2G
    memory_swap: 2G
    networks:
      - name: drone_net
    env:
      DRONE_RPC_PROTO: "http"
      DRONE_RPC_HOST: "droneci"
      DRONE_RPC_SECRET: "{{ shared_secret }}"
      DRONE_RUNNER_CAPACITY: "2"
      DRONE_RUNNER_NAME: "my-runner"
      DRONE_RUNNER_NETWORKS: "drone_net,gitea_net"
```
</div>
