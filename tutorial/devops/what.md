\newpage

La journée DevOps
=================

Nous allons maintenant nous mettre dans la peau d'une équipe DevOps et
réaliser une solution complète d'intégration/déploiement continu (le fameux
CI/CD, pour *Continuous Integration* et *Continuous Delivery*).

Le résultat attendu d'ici la fin de cette partie sera de mettre en place toutes
les briques décrites au chapitre précédent. Nous allons pour cela automatiser
le projet `youp0m`, que l'on connaît déjà bien.
\

Dans un premier temps, on voudra juste compiler notre projet, pour s'assurer
que chaque *commmit* poussé ne contient pas d'erreur de compilation (dans
l'environnement défini comme étant celui de production, donc avec une version
précise des outils de compilation). Ensuite, nous ajouterons quelques tests
automatiques, puis nous publierons automatiquement le binaire `youp0m` comme
fichier associé à un tag au sein de l'interface web d'un gestionnaire de
versions.

Enfin, nous mettrons en place un registre Docker qui nous permettra de publier
automatiquement l'image Docker associée. C'est à partir de cette image Docker
que l'on va commencer à déployer automatiquement...
