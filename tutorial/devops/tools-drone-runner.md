### *Runner*

Notre conteneur `droneci` est uniquement une interface graphique qui va
centraliser d'un côté les nouveaux commits à traiter, et de l'autre les
résultats retournés par les agents chargés d'exécuter les tâches.

Il serait impensable d'exécuter arbitrairement du code en parallèle d'une
application privilégiée (ici, notre conteneur `droneci` a accès aux dépôts
potentiellement privés de Gitea). Les agents qui sont amenés à traiter du code
arbitraire s'exécutent à part et peuvent être de différents types.

Nous allons lancer un *runner* Docker : il s'agit d'un type d'agent qui va
exécuter nos étapes de compilation dans des conteneurs Docker (oui, quand on
disait que Drone était conçu autour de Docker, ce n'était pas pour rire !)
