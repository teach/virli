Les étapes sont sensiblement les mêmes que dans le `Dockerfile` que nous avons
écrit précédemment.

::::: {.exercice}

Commencez à partir de l'exemple donné dans la documentation de Drone. Par
rapport au `Dockerfile`, n'ajoutez pas `tags dev`, cela permettra d'embarquer
tout le contenu statique (pages HTML, feuilles de style CSS, Javascript, ...)
directement dans le binaire, ce qui simplifiera la distribution.

*Committons* puis poussons notre travail. Dès qu'il sera reçu par Gitea, nous
devrions voir l'interface de Drone lancer les étapes décrites dans le fichier.

:::::

![Drone en action](drone-run.png){height=6cm}

::::: {.warning}
**IMPORTANT :** si vous avez l'impression que ça ne marche pas et que vous avez
réutilisé le fichier présent sur le dépôt au lieu de partir de l'exemple donné
dans la documentation, **commencez en partant de l'exemple de la
documentation** ! Le fichier présent sur le dépôt **ne fonctionnera pas** dans
votre situation !
:::::

Lorsqu'apparaît enfin la ligne `git.nemunai.re/youp0m`, le projet est compilé !
