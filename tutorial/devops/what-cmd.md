## Préparer le terrain

Tous les déploiements sont à faire sur votre machine ; la plate-forme de CI
utilisera massivement les conteneurs Docker, qui seront regroupés au sein de
réseaux Docker : cela nous permettra de profiter de la résolution de noms entre
les conteneurs.

Dès à présent, nous pouvons commencer par créer un réseau, dans lequel nous
placerons tous nos conteneurs de CI/CD :

<div lang="en-US">
```shell
docker network create my_ci_net
```
</div>
