Exportez dans une variable d'environnement le token d'accès généré qui vous est affiché par Gitea :

<div lang="en-US">
```shell
export RENOVATE_TOKEN=a0b1c2d3e4f5A6B7C8D9
```
</div>


La commande que nous allons utiliser pour lancer Renovatebot est la suivante :

<div lang="en-US">
```shell
docker container run --name renovate --network my_ci_net \
    -e RENOVATE_ENDPOINT="http://gitea:3000/api/v1/" RENOVATE_PLATFORM=gitea \
    -e RENOVATE_TOKEN -e RENOVATE_GIT_AUTHOR="Renovatebot <renovate@sample>" \
    -e RENOVATE_AUTODISCOVER=true -e RENOVATE_LOG_LEVEL=info -d \
    renovate/renovate
```
</div>
