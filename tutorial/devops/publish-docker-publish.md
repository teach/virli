#### Publication de l'image \

Une fois le registre testé, il ne nous reste plus qu'à ajouter une étape de
publication de l'image Docker. Cela se fait au moyen du plugin suivant :\
<http://plugins.drone.io/drone-plugins/drone-docker/>.

::::: {.exercice}

Continuons d'éditer le fichier `.drone.yml` du dépôt pour faire générer à Drone
l'image Docker, et la publier.

Attention dans Drone, le domaine à utiliser pour contacter Gitea (et donc le
registre), n'est pas `localhost`, mais `gitea`. Comme notre registre n'a pas de
certificat TLS pour utiliser `https`, il est nécessaire de définir l'option
`insecure` à `true`. Utilisez les *secrets* de Drone pour stocker le nom
d'utilisateur et le mot de passe d'accès au registre.

:::::
