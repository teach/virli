## Préparer le terrain

Tous les déploiements sont à faire sur votre machine en utilisant des
conteneurs Docker, qui seront regroupés au sein de réseaux
Docker. Cela vous permettra d’utiliser la résolution de noms entre vos
conteneurs.

Dans votre playbook Ansible, vous pourrez procéder ainsi (il s'agit d'un
exemple qu'il faut comprendre et adapter par rapport à la suite) :

<div lang="en-US">
```yaml
- name: Create virli network
  docker_network:
    name: virli3
```
</div>
