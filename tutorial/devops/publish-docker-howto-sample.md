Nous pouvons ensuite envoyer une image pour s'assurer que tout va bien :

<div lang="en-US">
```bash
docker build -t localhost:3000/${USER}/youp0m .
docker push localhost:3000/${USER}/youp0m
```
</div>

Rendez-vous ensuite sur la page <http://gitea:3000/${USER}/-/packages> pour
attribuer l'image au dépôt `youp0m` (voir pour cela dans les paramètres de
l'image).

![Notre image `youp0m` dans Gitea !](gitea-packages.png){height=6cm}
