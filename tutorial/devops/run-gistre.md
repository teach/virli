## Lancement du module sur la machine cible

Mettons de côté le déploiement continu, pour nous concentrer sur la manière
dont nous allons pouvoir exécuter notre module de relevé de compteur Linky.
\

Le binaire de notre module se lance de la manière suivante :

<div lang="en-US">
```shell
42sh$ ./linky2influx --influx-host localhost --influx-username myuser \
    --influx-password mypasswd /dev/ttyEnedis0
# or
42sh$ INFLUXDB_HOST=localhost INFLUXDB_USERNAME=myuser INFLUXDB_PASSWORD=mypasswd \
    ./linky2influx /dev/ttyEnedis0
```
</div>

où `/dev/ttyEnedis0` est le périphérique correspondant à notre liaison série
vers le compteur. On considère qu'une règle `udev` a été écrite pour créer
l'entrée correspondante dans `/dev`.
\

L'objectif est d'établir la méthode qui sera la plus efficace pour
Électropcool, car elle devra sans doute demander à chacune des équipes de
réaliser le packaging des modules qu'elles maintiennent. Il faut pour cela
tester plusieurs solutions. Certaines peuvent sans doute déjà être éliminées car
inadaptées, c'est à vous de déterminer parmi les technologies d'empacketages
proposées, et celles que vous connaissez, lesquelles vous écartez d'emblée,
lesquelles vous souhaitez garder pour explorer davantage, et enfin laquelle
retient votre attention.

Vous êtes tenu de retenir au minimum 2 solutions distinctes (on considère que
les technologies listées sur la même lignes sont similaires).

La liste de technologies proposées est à la suivante :

- conteneur Docker ou podman,
- conteneur LXC (avec base alpine ou nu),
- conteneur OCI via runc,
- service systemd ou systemd-nspawn,
- packages `.ipk` via Yocto/Bitbake,
- package Nix,
- paquet Snap, FlatPack ou AppImage,
- [k3s](https://k3s.io/),
- ...


Les sections suivantes vous donneront une idée des éléments attendus pour chacun.


### Docker, podman, ...

Nous avons déjà un `Dockerfile` dans notre dépôt, vous avez juste à écrire un
script shell permettant de lancer le module, il doit bien évidemment avoir
accès au périphérique correspondant au compteur.

<div lang="en-US">
```
./docker/run.sh
```
</div>


### Conteneur LXC

LXC peut s'utiliser de multiple manière différentes, y compris avec des images
OCI. Choisissez la méthode qui vous semble la plus appropriée, il est attendu
au moins un script pour lancer notre conteneur, s'il est différent d'un

<div lang="en-US">
```shell
42sh# lxc-create --template linky2influx --name l2i && lxc-start -n l2i`.
```
</div>

<div lang="en-US">
```
./lxc/run.sh
```
</div>

En complément, vous pouvez inclure au dépôt de `linky2influx` le modèle
permettant de créer le conteneur LXC, ainsi qu'un exemple de configuration :

<div lang="en-US">
```
./linky2influx/lxc-scripts/template.sh
./linky2influx/lxc-scripts/sample.config
```
</div>


### Conteneur OCI via `runc`

`runc` a besoin d'un fichier `config.json` et éventuellement `runtime.json`. La
ligne de commande pour lancer `runc` n'est pas demandée, l'image OCI résultant
du `Dockerfile` sera utilisée et présente pour l'exécution.


### Service systemd ou systemd-nspawn

Le service `systemd` est redondant avec un système Yocto contenant `systemd`,
vous pourriez écrire le script `systemd` et profiter de l'utiliser dans votre
recette `bitbake`.

Un service basé sur `nspawn` peut par contre être potentiellement intéressant,
il pourra être intégré au dépôt :

<div lang="en-US">
```
./linky2influx/systemd/linky2influx.nspawn
```
</div>


### Yocto

Vous pourriez vouloir écrire une recette Bitbake pour créer et déployer le
module via un paquet `.ipk` ou similaire en fonction de votre configuration.

Écrivez la recette au sein de `meta-electropcool` :

<div lang="en-US">
```
./meta-electropcool/recipes-support/linky2influx/linky2influx_9999.bb
```
</div>


### Nix

L'expression Nix correspondant au paquet pourra être intégré au dépôt

<div lang="en-US">
```
./linky2influx/linky2influx.nix
```
</div>

Précisez ensuite dans un `README-nix.md` à côté, la manière dont vous souhaitez
utiliser le package Nix ensuite.


### Snap, Flatpack, AppImage

Intégrez au dépôt le fichier de description, par exemple :

<div lang="en-US">
```
./linky2influx/snapcraft.yaml
```
</div>

Il faudra également préciser avec un fichier `run.sh` la manière dont lancer
votre conteneur :

<div lang="en-US">
```
./{snap,flatpack}/run.sh
```
</div>



### k3s

Si vous connaissez déjà Kubernetes, vous pourriez vouloir écrire un *Chart*
Helm :

<div lang="en-US">
```
./k3s/linky2influx.yaml
```
</div>


### *Votre solution ici*

N'hésitez pas à apporter une autre solution originale, mais tout de même
adaptée à l'objectif, que celles qui seraient listées ici.
\

Commençons par appréhender la publication d'image Docker, que nous maîtrisons
plutôt bien normalement.


\newpage
## Déploiement via Docker
