Nous pouvons ensuite envoyer une image pour s'assurer que tout va bien :

<div lang="en-US">
```bash
docker build -t localhost:3000/${USER}/linky2influxdb .
docker push localhost:3000/${USER}/linky2influxdb
```
</div>

Rendez-vous ensuite sur la page <http://gitea:3000/${USER}/-/packages> pour
attribuer l'image au dépôt `linky2influxdb` (voir pour cela dans les paramètres de
l'image).

![Notre image `linky2influxdb` dans Gitea !](gitea-packages.png){height=6cm}
