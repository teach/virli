Votre playbook ressemblera à quelque chose comme ça :

<div lang="en-US">
```yaml
- name: Create a volume for storing repositories
  docker_volume:
    name: gitea-data

- name: launch gitea container
  docker_container:
    name: gitea
    image: "gitea/gitea:{{ version }}"
    volumes:
      - gitea-data:/data
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
    state: started
    restart_policy: unless-stopped
    memory: 1G
    memory_swap: 1G
    networks:
      - name: gitea_net
    published_ports:
      - "2222:22"
      - "3000:3000"
    env:
      RUN_MODE: "prod"
      DOMAIN: "gitea"
      SSH_DOMAIN: "gitea"
      INSTALL_LOCK: "true"
      GITEA__security__SECRET_KEY: "{{ secret_key }}"
      GITEA__security__INTERNAL_TOKEN: "{{ internal_token }}"
      GITEA__webhook__ALLOWED_HOST_LIST: "external"
```
</div>

Vous trouverez plus d'infos sur cette page :
<https://docs.gitea.io/en-us/install-with-docker/>.
