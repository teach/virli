Autres outils indispensables
----------------------------

### Maintient à jour des dépendances

Une opération fastidieuse, souvent oubliée sitôt le projet envoyé en
production, c'est la mise à jour des dépendances applicatives. Fastidieux car
il faut d'une part être informé qu'une mise à jour est disponible, c'est-à-dire
qu'il faut suivre les mails, parfois nombreux, informant des nouvelles
*releases*, parfois il s'agir de newslettre, ou encore parfois aucune
notification ne peut être programmée, il faut se rendre régulièrement sur un
site pour savoir si oui ou non une mise à jour est disponible.

Et ce n'est pas tout puisqu'une fois la nouvelle version identifiée, il faut la
récupérer, vérifier que tout compile et que les tests passent... On peut vite
comprendre que personne ne veut avoir cette tâche.

Heureusement pour nous, des outils existent pour faire tout cela ! Dependabot
et Renovatebot sont deux projets qui vont nous aider à maintenir nos
projets. Que ce soit pour corriger une vulnérabilité dans un module NodeJS, un
nouvelle version d'un conteneur Docker ou une évolution majeure d'un framework,
nous serons alerté et pourrons agir en conséquence, en sachant si les tests
passent ou pas, avec l'aide de notre système d'intégration continue.

Pour la suite de notre expérience, nous allons prendre en main Renovatebot qui
semble aujourd'hui la solution la plus aboutie des deux.

![L'activité favorite de renovatebot : créer des *pull-requests*](renovatebot-pr.png){height=10cm}


### Installation de Renovatebot

Une fois de plus, nous allons déployer ... un conteneur Docker ! Mais avant
cela, il va falloir créer un utilisateur dédié à Renovatebot dans notre forge.
Cet utilisateur sera utilisé par le *bot* pour participer à vos projets : il va
régulièrement cloner vos dépôts, rechercher les dépendances pour les outils
qu'il maîtrise, puis préparer des *pull-requests* dès qu'il détectera une
nouvelle version d'une dépendance.

Dès lors que vous aurez créé le nouvel utilisateur dans Gitea, il faudra
générer un jeton d'accès, car aussi doué soit Renovatebot, il préfère utiliser
l'API HTTP plutôt que de cliquer sur les boutons. Dans les paramètres de
l'utilisateur que vous aurez créé, sous l'onglet « Applications », vous
trouverez un encadré « Gérer les jetons d'accès ». Contrairement à DroneCI pour
lequel il fallait créer une application OAuth2, ici il s'agit bien du formulaire en
haut de la page.

::::: {.warning}

Il est bien question de créer un jeton d'accès pour l'utilisateur renovatebot
que vous avez créé, et non pas pour votre compte utilisateur.

Bien que cela fonctionnerait parfaitement, le bot parlerait en votre nom, il
serait alors difficile de suivre les requêtes.

:::::

Juste avant de lancer le conteneur, assurez-vous que votre utilisateur
Renovatebot a bien accès en écriture aux dépôts que vous souhaitez qu'il
surveille. Sans quoi il ne sera pas en mesure de vous aider.
