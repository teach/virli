#### Test de l'image \

Sur notre hôte, nous pouvons tester que l'image a bien été publiée grâce à la
commande suivante :

<div lang="en-US">
```bash
docker pull localhost:3000/${USER}/linky2influxdb
```
</div>

Si une nouvelle image est bien récupérée du dépôt, bravo, vous avez réussi !
