### Pour aller plus loin

Le mouvement DevOps tend à utiliser massivement la CI/CD, mais ce ne leur est
évidemment pas exclusivement réservé. Toute entreprise ayant une méthodologie
de développement saine dispose d'outils automatisés de construction et de
tests.

Pour en savoir plus sur le DevOps, vous devriez lire la première partie du [TP
des SRS](https://virli.nemunai.re/tutorial-5-srs.pdf), qui traite justement de
ce sujet.
