### Publier une image Docker

Toutes les tâches de publication peuvent s'assimiler à des tâches de
déploiement continu. C'est en particulier le cas lorsque le produit de
compilation sera simplement publié et qu'il n'y a pas de service à mettre à
jour ensuite (par exemple, dans le cas de Firefox ou de LibreOffice, une fois
testés, les paquets sont envoyés sur le serveur d'où ils seront distribués ; il
n'y a pas de service/docker à relancer).
