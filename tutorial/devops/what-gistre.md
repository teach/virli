\newpage

Électropcool
============

Bienvenue dans la société Électropcool !

Électropcool est une société française spécialisée dans
l'[immotique](https://fr.wikipedia.org/wiki/Immotique), elle vend des
équipements IoT, principalement à destination des professionnels du bâtiment. Le
produit phare est une plate-forme de
[GTB](https://fr.wikipedia.org/wiki/Gestion_technique_de_b%C3%A2timent)
modulaire de suivi de la vie d'un bâtiment où sont regroupés les différents
compteurs, capteurs et actionneurs que l'on retrouve dans les installations.
Les nouveaux bâtiments conçus autour la plate-forme permettent de faire de
sérieuses économies d'entretien (en ne faisant déplacer les techniciens que
lorsque c'est nécessaire) tout en permettant d'anticiper les problèmes (grâce à
un moteur de *machine-learning* capable d'anticiper les pannes) et les besoins
en combustible (liés à la météo)[^HOMEASSISTANT].

[^HOMEASSISTANT]: Si ça vous met l'eau à la bouche, jetez un œil du côté du
    projet <https://www.home-assistant.io/> qui fait un peu moins de
    *bullshit*, mais est bien réel !

La principale difficulté que rencontre Électropcool est qu'aucun bâtiment ne
dispose des mêmes références de pièces et qu'en dehors des nouveaux bâtiments
pour lesquels la société peut imposer des équipements électroniques
spécifiquement supportés et compatibles avec la plate-forme qu'elle vend, il lui
est bien souvent nécessaire de faire des développements spécifiques pour
s'interfacer avec de l'électronique existant : c'est notamment le cas pour les
chaudières et les
[VMC](https://fr.wikipedia.org/wiki/Ventilation_m%C3%A9canique_contr%C3%B4l%C3%A9e),
qui, compte tenu de leur coût de remplacement prohibitif, nécessitent souvent
de réaliser des interfaces électroniques spécifiques pour s'adapter à
l'existant.

L'entreprise est en train de prendre un tournant historique et
souhaite accélérer ses développements pour faire face à la concurrence
qui arrive sur le marché.
\

L'entreprise utilise principalement de nombreux équipements de la *Raspberry Pi
fundation*, notamment les [Compute Module 3 et
4](https://www.raspberrypi.com/products/compute-module-4/) et les [Raspberry Pi
Zero 2 W](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/), ainsi
que de nombreux PCB fait par l'entreprise, à base de micro-contrôleurs AVR,
lorsqu'il est nécessaire de pour s'interfacer avec des équipements
propriétaires non prévu pour l'immotique.

Une grosse partie des travaux est donc réalisé avec un noyau Linux, sur du
matériel très performant, pour de l'embarqué.
\

Tous les modules logiciels qui interagissent avec les capteurs sont aujourd'hui
intégrés dans un système construit à l'aide de
[`bitbake`](https://www.yoctoproject.org/). L'entreprise grossissant à vue
d'œil, il devient de plus en plus difficile de synchroniser les équipes pour
concilier la stabilité des *releases* avec le besoin de déployer de nouvelles
fonctionnalités chaque semaine.

Vous avez été chargés d'étudier la meilleure façon de déployer les différents
modules, sans qu'il soit nécessaire de reconstruire une image Yocto à chaque
fois, mais tout en assurant la stabilité de la plate-forme.

Le directeur technique vous suggère de regarder du côté des conteneurs
applicatifs, qui sont légers et assurent un cloisonnement suffisant pour ne pas
entraver la stabilité de la plate-forme en cas de déploiement d'un module
défaillant.

Vous êtes également chargés de jeter les bases du système d'intégration continu
des modules. (La partie déploiement continu, sera réalisé plus tard par
l'équipe développant le nouveau système de base, suivant le meilleur outil que
vous retiendrez.)
\

Le projet qui vous servira de base pour vos tests sera
[`linky2influx`](https://git.nemunai.re/nemunaire/linky2influx) : à partir d'un
compteur [Linky](https://fr.wikipedia.org/wiki/Linky), branché sur les bornes
de [téléinformation client du
compteur](https://hallard.me/demystifier-la-teleinfo/) et [lisant le
protocole](https://www.enedis.fr/media/2035/download) pour enregistrer les
données dans une *Time-series database*, en l'occurrence InfluxDB.

![La prod chez Électropcool](../devops/electropcool.jpg)

Dans un premier temps, on voudra juste compiler notre projet, pour s'assurer
que chaque commit poussé ne contient pas d'erreur de compilation, dans
l'environnement défini comme étant celui de production. Ensuite, on ajoutera
quelques tests automatiques. Puis nous publierons automatiquement le binaire
`linky2influx` comme fichier associé à un tag au sein de l'interface web du
gestionnaire de versions.

Nous testerons enfin différentes solution pour déployer notre binaire, afin
d'établir quelle est la solution adéquate.
