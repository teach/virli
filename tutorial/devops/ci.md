Intégration continue
--------------------

Une fois Gitea et Drone installés et configurés, nous allons pouvoir rentrer
dans le vif du sujet : faire de l'intégration continue sur notre premier projet !

L'idée est qu'à chaque nouveau *commit* envoyé sur le dépôt, Drone fasse une
série de tests, le compile et publie les produits de compilation.

### Créez un dépôt pour `youp0m`

::::: {.exercice}

Reprenons les travaux déjà réalisés : nous allons notamment avoir besoin du
`Dockerfile` que nous avons réalisé pour le projet `youp0m`.

Après avoir créé (ou migré pour les plus malins !) le dépôt
`youp0m`[^urlyoup0m] dans gitea, synchronisez les dépôts dans Drone, puis
activez la surveillance de `youp0m`.

[^urlyoup0m]: <https://git.nemunai.re/nemunaire/youp0m>

:::::
