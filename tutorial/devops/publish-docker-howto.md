#### Fonctionnement du registre de Gitea \

Gitea intègre un registre d'images Docker (depuis la version 1.17,
mi-2022). Pour le moment, les images sont uniquement liées à un compte
utilisateur ou à une organisation, pas directement à un dépôt. Une page permet
de rattacher l'image envoyée à un dépôt, ce que l'on fera dans un deuxième
temps.

Afin de pouvoir envoyer une image nous-même, nous devons nous connecter au
registre :

<div lang="en-US">
```
docker login localhost:3000
```
</div>

::::: {.question}

#### N'a-t-on pas besoin d'un certificat TLS pour utiliser un registre Docker ? {-}
\

Ceci est possible exclusivement parce que le registre `localhost` est considéré
non-sûr par défaut. C'est-à-dire qu'il n'a pas besoin de certificat TLS valide
sur sa connexion HTTP pour être utilisé.\

Si on avait dû utiliser un autre nom de domaine, il aurait fallu [l'ajouter à
la liste des
`insecure-registries`](https://docs.docker.com/registry/insecure/).

:::::
