À l'inverse, `youp0m` est à la fois un programme que l'on peut télécharger et
un service qu'il faut déployer pour le mettre à jour. Afin de simplifier son
déploiement en production, nous utiliserons une image Docker.
