Une fois lancé, rendez-vous sur l'interface de DroneCI : <http://droneci/>

Vous serez automatiquement redirigé vers la page d'authentification de Gitea,
puis vers l'autorisation OAuth d'accès de Drone à Gitea. Il faut bien
évidemment valider cette demande, afin que Drone ait accès à nos dépôts.

![OAuth Drone](oauth-drone.png){width=7.6cm}
