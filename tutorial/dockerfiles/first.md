\newpage

Premières étapes
================

Dans un premier temps, nous allons créer une image Docker comme si l'on
réalisait une installation sur une machine classique : en suivant une recette,
sans trop se préoccuper des fonctionnalités que propose Docker.

La machine (notre première image Docker) contiendra tout le nécessaire pour
faire fonctionner notre service de monitoring.




## `RUN` ou script ?

### InfluxDB

Ensuite vient la suite d'instructions pour installer d'InfluxDB. Le paquet
n'est pas disponible dans les dépôts[^debrepos]. La
[procédure décrite du site](https://portal.influxdata.com/downloads) incite à
télécharger le paquet mis à disposition puis à l'installer via `dpkg -i`.

[^debrepos]: Le projet met à disposition des dépôts, si vous préférez cette
    méthode, consultez-la
    [documentation d'installation](https://docs.influxdata.com/influxdb/v1.6/introduction/installation/#ubuntu-debian).

Deux solutions s'offrent à nous :

*  télécharger le paquet hors du conteneur, le copier, puis l'installer.
*  faire un `RUN` avec toutes ces opérations (sans oublier l'installation
   préalable de `wget`/`curl`).

La copie étant définitive (supprimer le fichier ne le supprimera pas des
couches où il a pu exister), on préférera la seconde méthode, bien que `wget`
restera en déchet. La première méthode aura plus sa place dans un dépôt de
projet où les binaires auront été préalablement compilés, il ne restera plus
qu'à les copier dans le conteneur au bon emplacement.

Écrivons une commande `RUN` qui va télécharger la dernière version
d'InfluxDB, qui va l'installer et supprimer le fichier.

\vspace{1em}

À ce stade, nous pouvons déjà terminer le conteneur (`EXPOSE`, `CMD`, ...) et
[tester](http://localhost:8083) qu'InfluxDB est bien utilisable.

Il est possible que vous ayez à écraser le fichier de configuration via un
`COPY` (ou de manière plus maligne en utilisant `--volume` au moment du `docker
run`, cela ne fonctionne pas qu'avec les dossiers !). Ou peut-être ferez-vous
un `ENTRYPOINT` ?


### `telegraf`

`telegraf` est un programme qui permet de collecter des métriques systèmes. Il
travaille de paire avec InfluxDB, qu'il utilise pour stocker les valeurs
relevées.

Vous pouvez monitorer les métriques de n'importe quelle machine, simplement en
installant `telegraf` et en lui indiquant l'emplacement de son serveur
InfluxDB. Nous allons installer `telegraf` sur notre machine à l'aide de la
[documentation](https://docs.influxdata.com/telegraf/v1.8/introduction/installation/).

Ces quelques lignes devraient suffir à lancer la collecte, à condition que
votre InfluxDB écoute sur le port 8086 local :

<div lang="en-US">
```bash
TELEGRAF_VERSION=1.8.0
wget https://dl.influxdata.com/telegraf/releases/telegraf-${TELEGRAF_VERSION}_linux_amd64.tar.gz
tar xf telegraf-${TELEGRAF_VERSION}_linux_amd64.tar.gz
TELEGRAF_CONFIG_PATH=./telegraf/etc/telegraf/telegraf.conf ./telegraf/usr/bin/telegraf
```
</div>

Rendez-vous ensuite dans [l'interface d'InfluxDB](http://localhost:8083/) pour
voir si la collecte se passe bien.

Dans l'interface sélectionnez la base `telegraf` puis explorez les valeurs :

<div lang="en-US">
```sql
SHOW MEASUREMENTS
SHOW FIELD KEYS
SELECT usage_idle FROM cpu WHERE cpu = 'cpu-total' ORDER BY time DESC LIMIT 5
```
</div>

Laissons tourner `telegraf` afin de constituer un petit historique de valeurs.


## Rendu  {-}

Avant de passer à la suite, placez votre `Dockerfile` et les éventuels fichiers
annexes dans un dossier `influxdb`.
