::::: {.exercice}

Faites en sorte que le `Dockerfile` que vous avez créé pour `youp0m` indique un
*frontend* BuildKit à utiliser, tout en restant compatible avec la syntaxe du
`docker build` classique.

:::::
