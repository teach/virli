::::: {.exercice}

Pour mettre en application tout ce que nous venons de voir, réalisons le
`Dockerfile` du service web [`youp0m`](https://you.p0m.fr/) que nous avons
déjà utilisé précédemment.

Pour réaliser ce genre de contribution, on ajoute généralement un `Dockerfile`
à la racine du dépôt.

Vous pouvez cloner le dépôt de sources de `youp0m` à :\
<https://git.nemunai.re/nemunaire/youp0m.git>

Pour compiler le projet, vous pouvez utiliser dans votre `Dockerfile`
des instructions similaires à cela :

<div lang="en-US">
```dockerfile
FROM golang:1.18
COPY . /go/src/git.nemunai.re/youp0m
WORKDIR /go/src/git.nemunai.re/youp0m
RUN go build -tags dev -v
```
</div>

::::: {.question}

#### Que faire des ressources statiques HTML/CSS/JS ? {-}

L'exemple ci-dessus compile uniquement le code Go. À l'exécution, le
binaire s'attendra à trouver un dossier `static/` dans le dossier
courant. Ce dossier peut être changé en ajoutant l'option `-static
NEWPATH` aux paramètres de lancement.

:::::

Remarquez la puissance de Docker : vous n'avez sans doute pas de compilateur Go
installé sur votre machine, et pourtant, en quelques minutes et à partir du
seul code source de l'application et d'un `Dockerfile`, vous avez pu compiler sur
votre poste le binaire attendu. WOW, non ?

À vous de compléter le `Dockerfile` afin de reproduire le comportement
de l'image `registry.nemunai.re/youp0m` que nous avions utilisé :

<div lang="en-US">
```
docker container run -d -P youp0m
```
</div>

:::::
