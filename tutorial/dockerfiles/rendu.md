\newpage

Projet et rendu
===============

Projet
------

Avec l'aide d'un `Dockerfile` *multi-stage*, réalisez l'image la plus petite
possible (partant d'un `FROM scratch`{.dockerfile}), qui permette d'utiliser la
[page de compte à rebours](https://virli.nemunai.re/countdown.html) avec cette
configuration pour nginx :

<div lang="en-US">
```conf
events {}

http {
	default_type text/html;

	index countdown.html;

    server {
        listen 8080;

    	root /srv/http;

    	rewrite "^/[0-9]+:[0-9]{2}$" /countdown.html;
    	rewrite "^/[0-9]+$" /countdown.html;
    }
}
```
</div>

Vous pouvez envisager dans un premier temps d'extraire de l'image `nginx`, le
binaire `nginx` lui-même et observer les différents problèmes. Vous pourrez
ensuite par exemple envisager de compiler `nginx` (vous trouverez les sources
du projet : <http://nginx.org/download>).

Dans tous les cas, votre `Dockerfile` devra être facilement maintenable
(notamment en cas de nouvelle version du serveur web), et vous devrez apporter
une attention particulière au suivi des bonnes pratiques d'écriture des
`Dockerfile`.

### Exemple d'exécution

<div lang="en-US">
```
42sh$ docker image build -t countdown countdown
42sh$ docker container run -d -P countdown
42sh$ firefox http://localhost:32198/42:23
```
</div>


Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Par ailleurs, n'oubliez pas de répondre à
[l'évaluation du cours](https://www.epitaf.fr/moodle/mod/quiz/view.php?id=307).


Tarball
-------

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers
supplémentaires) :

<div lang="en-US">
```
login_x-TP2/
login_x-TP2/youp0m/
login_x-TP2/youp0m/Dockerfile
login_x-TP2/youp0m/entrypoint.sh
login_x-TP2/youp0m/.dockerignore
login_x-TP2/youp0m/...
login_x-TP2/countdown/Dockerfile
(login_x-TP2/countdown/nginx.conf)
(login_x-TP2/countdown/countdown.html)
```
</div>

Les deux fichiers `nginx.conf` et `countdown.html` seront écrasés par
les fichiers fournis lors de la correction, vous n'êtes pas donc
obligés de les embarquer dans votre rendu.
