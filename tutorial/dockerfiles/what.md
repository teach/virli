\newpage

Construire des images
=====================

Jusqu'à maintenant, nous avons profité des images présentes sur les registres
pour utiliser Docker. Sur ces registres, on trouve d'ailleurs non seulement des
images officielles proposées directement par les éditeurs (`nginx`, `mysql`,
...), mais aussi des images conçues par les utilisateurs pour leurs propres
besoins.

Et si nous aussi, nous construisions nos propres images ? Ça vous dit ?
