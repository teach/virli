---
title: Virtualisation légère -- TP n^o^ 2.1
subtitle: Construire des images Docker
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 16 octobre 2019
abstract: |
  Durant ce deuxième TP, nous allons voir comment créer nos propres
  images !

  \vspace{1em}

  Tous les éléments de ce TP (exercices et projet) sont à rendre à
  <virli@nemunai.re> au plus tard le mercredi 23 octobre 2019 à 13
  h 42. Consultez la dernière section de chaque partie pour plus
  d'informations sur les éléments à rendre.

  En tant que personnes sensibilisées à la sécurité des échanges
  électroniques, vous devrez m'envoyer vos rendus signés avec votre
  clef PGP. Pensez à
  [me](https://pgp.mit.edu/pks/lookup?op=vindex&search=0x842807A84573CC96)
  faire signer votre clef et n'hésitez pas à [faire signer la
  vôtre](https://www.meetup.com/fr/Paris-certification-de-cles-PGP-et-CAcert/).
  ...
