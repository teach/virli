D'autres méthodes pour créer des images
---------------------------------------

Les images utilisées par Docker pour lancer les conteneurs répondent avant tout
aux spécifications OCI. Le format étant standard, il est normal que d'autres
outils puissent utiliser, mais aussi créer des images.


### Changer la syntaxe de nos `Dockerfile`

Parfois on peut se sentir un peu frustré par la syntaxe des `Dockerfile` ou par
son manque d'évolutivité. Avec BuildKit, il est possible de préciser un parseur
à utiliser pour l'évaluation de la syntaxe du `Dockerfile`. Les parseurs
(*frontend* dans la documentation anglaise) sont des images Docker : on indique
leur nom dans un commentaire au tout début du fichier :

<div lang="en-US">
```dockerfile
# syntax=docker/dockerfile:1.4
FROM ubuntu
RUN apt-get update && apt-get install gimp
```
</div>

La possibilité d'avoir plusieurs implémentations de `Dockerfile` apporte pas mal
d'avantages :

- La version de l'image *frontend* est systématiquement comparée en ligne au
  début du parsing du `Dockerfile`, ce qui assure la récupération des derniers
  correctifs/versions, sans nécessiter une mise à jour du *daemon* Docker.
- On s'assure que chaque développeur/utilisateur utilise la même
  implémentation, avec la même version.
- L'évolution de la syntaxe peut être plus souple car elle ne dépend plus de la
  version de Docker installée, mais de la version déclarée dans le
  `Dockerfile`.
- On peut même créer de nouvelles syntaxes facilement.

Les images de parseur sont chargées, à partir d'un fichier lisible et
compréhensible par un humain, de créer une représentation intermédiaire
(*LLB*). Cette représentation intermédiaire se compose d'une liste d'opérations
basiques (*ExecOp*, *CacheOp*, *SecretOp*, *SourceOp*, *CopyOp*, ...).

N'hésitez pas à jeter un œil aux autres langages de `Dockerfile` existants,
notamment :

- [Gockerfile](https://github.com/po3rin/gockerfile) : bien que dépassé faute
  de mise à jour, cette implémentation est très simple à comprendre : elle a
  pour but de créer une image contenant un unique binaire Go, à partir du nom
  de son dépôt.
- [hlb](https://openllb.github.io/hlb/) : une syntaxe prometteuse, plus proche
  pour les développeurs.
- [Earthly](https://earthly.dev/) : qui tend à regrouper `Makefile`,
  `Dockerfile`, et autres scripts de CI et de tests.


### Des images sans Docker

Il est aussi possible de se passer complètement de Docker. La plupart des
outils qui sont capables de générer des images de machines virtuelles, sont
aussi capables de générer des images Docker. Citons notamment :

- Buildah : <https://github.com/containers/buildah/> (utilisé par `podman`),
- Buildpacks : <https://buildpacks.io/>,
- Hashicorp Packer : <https://www.packer.io/docs/builders/docker>,
- Nix et Guix : <https://nix.dev/tutorials/building-and-running-docker-images>,
- Kubler : <https://github.com/edannenberg/kubler>,
- et bien d'autres.
