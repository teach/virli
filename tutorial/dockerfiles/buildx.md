`buildx`
--------

Docker `buildx` est un plugin qui apporte
[BuildKit](https://github.com/moby/buildkit). Tout en étant compatible avec la
syntaxe des `Dockerfile` existant, BuildKit apporte une gestion concurrente des
nœuds de construction : très utile lorsque l'on construit une image pour
plusieurs architectures.


### Installation Windows et MacOS {-}

Avec Docker Desktop, le plugin est déjà installé, vous n'avez aucune action
supplémentaire à effectuer, vous pouvez commencer à l'utiliser.

### Installation Linux {-}

En fonction de la méthode d'installation que vous avez suivie, vous avez
peut-être déjà le plugin installé. Si vous n'avez pas d'erreur en exécutant
`docker buildx`, mais que vous voyez l'aide de la commande, c'est bon. Sinon,
vous pouvez l'installer comme ceci :

<div lang="en-US">
```
V="v0.9.1"
mkdir -p ~/.docker/cli-plugins
curl -L -s -S -o ~/.docker/cli-plugins/docker-buildx \
    https://github.com/docker/buildx/releases/download/$V/buildx-$V.linux-amd64
chmod +x ~/.docker/cli-plugins/docker-buildx
```
</div>

### Utilisation

Nous pouvons réutiliser le `Dockerfile` que vous avez écrit pour `youp0m`, en
remplaçant simplement la ligne de `docker build` par celle-ci :

<div lang="en-US">
```
docker buildx build .
```
</div>

::::: {.more}

Nous ne rentrerons pas plus dans les détails de cette nouvelle commande, mais
sachez qu'on la retrouve particulièrement fréquemment dans les *GitHub
Actions* :\
<https://github.com/marketplace/actions/docker-setup-buildx>

:::::

### `docker/dockerfile:1.4`

La version habituelle de la syntaxe des `Dockerfile` est la version 1.1. En
utilisant BuildKit, nous pouvons dès à présent passer à la version 1.4.

Il n'y a pas d'ajouts marquant pour nous à ce stade, c'est pourquoi nous ne
détaillerons pas plus les nouveaux cas d'usage. Néanmoins, vous pouvez voir les
ajouts par rapport à la syntaxe usuelle sur cette page :\
<https://hub.docker.com/r/docker/dockerfile>.
