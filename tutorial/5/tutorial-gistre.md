---
title: Virtualisation légère -- TP n^o^ 6
subtitle: DevOps, intégration et déploiement continu
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Jeudi 1^er^ décembre 2022
abstract: |
  Durant ce nouveau TP, nous allons jouer les DevOps et déployer
  automatiquement des services !

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le dimanche 18
  décembre 2022 à 23 h 42. Consultez les sections matérialisées par un
  bandeau jaunes et un engrenage pour plus d'informations sur les
  éléments à rendre.
...
