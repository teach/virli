\newpage

Rendu
=====

Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Par ailleurs, n'oubliez pas de répondre à
[l'évaluation du cours](https://virli.nemunai.re/quiz/5).


Tarball
-------

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers
supplémentaires) :

<div lang="en-US">
```
login_x-TP3/
login_x-TP3/cicd-playbook/
login_x-TP3/cicd-playbook/cicd-setup.yml
login_x-TP3/cicd-playbook/roles/...
login_x-TP3/youp0m/
login_x-TP3/youp0m/.drone.yml
login_x-TP3/youp0m/.ansible/...        # Pour ceux qui auraient fait le 5.4 optionnel
login_x-TP3/youp0m/Dockerfile
login_x-TP3/youp0m/entrypoint.sh
login_x-TP3/youp0m/.dockerignore
login_x-TP3/youp0m/...
login_x-TP3/registry_play.sh
```
</div>
