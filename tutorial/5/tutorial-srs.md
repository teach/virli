---
title: Virtualisation légère -- TP n^o^ 5
subtitle: DevOps, intégration et déploiement continu
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 16 novembre 2022
abstract: |
  Durant ce nouveau TP, nous allons jouer les DevOps et déployer
  automatiquement des services !

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le mardi 29
  novembre 2022 à 23 h 42. Consultez les sections matérialisées par un
  bandeau jaunes et un engrenage pour plus d'informations sur les
  éléments à rendre.
...
