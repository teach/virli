::::: {.exercice}

### Comparaison de *namespace* -- `cmpns.sh`

Les *namespaces* d'un programme sont exposés sous forme de liens symboliques
dans le répertoire `/proc/<PID>/ns/`.

Deux programmes qui partagent un même *namespace* auront un lien vers le même
*inode*.

Écrivons un script, `cmpns`, permettant de déterminer si deux programmes
s'exécutent dans les mêmes *namespaces*. On ignorera les *namespace*s
`*_for_children`, car ils ne font pas partie du cycle d'exécution que l'on
cherche à comparer.

En shell, vous aurez besoin de `grep(1)` et de `readlink(1)`.


#### Exemples {.unnumbered}

<div lang="en-US">
```
42sh$ docker run -d influxdb
42sh$ ./cmpns $(pgrep influxd) $(pgrep init)
  - cgroup: differ
  - ipc: differ
  - mnt: differ
  - net: differ
  - pid: differ
  - time: same
  - user: same
  - uts: same
```
</div>
\

<div lang="en-US">
```
42sh$ ./cmpns $(pgrep init) self
  - cgroup: same
  - ipc: same
  - mnt: same
  - net: same
  - pid: same
  - time: same
  - user: same
  - uts: same
```
</div>

Ici, `self` fait référence au processus actuellement exécuté (comme il existe
un dossier `/proc/self/`, vous n'avez pas besoin de gérer de cas particulier
pour ça !).

Et pourquoi pas :

<div lang="en-US">
```
42sh# unshare -m ./cmpns $$ self
  - cgroup: same
  - ipc: same
  - mnt: differ
  - net: same
  - pid: same
  - time: same
  - user: same
  - uts: same
```
</div>

:::::
