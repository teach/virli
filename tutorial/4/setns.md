### Rejoindre un *namespace*

Rejoindre un espace de noms se fait en utilisant l'appel système `setns(2)`, ou
la commande `nsenter(1)`. Il est nécessaire de donner en argument
respectivement un *file descriptor* ou le chemin vers le fichier, lien
symbolique, représentant l'espace de nom (dans `/proc/<PID>/ns/...`).

Une particularité de ces fichiers, que l'on ne peut pas afficher (leurs liens
ne pointent pas sur des fichiers que l'on peut atteindre), c'est que l'on peut
les ouvrir avec `open(2)` pour obtenir un *file descriptor* que l'on pourra
passer à `setns(2)`.

Pour les commandes *shell*, il convient de donner en argument le chemin vers le
lien symbolique : la commande se chargera d'`open(2)` le fichier pour obtenir
le *file descriptor* nécessaire.
