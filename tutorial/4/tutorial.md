---
title: Virtualisation légère -- TP n^o^ 4
subtitle: Linux Internals partie 2
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 9 novembre 2022
abstract: |
  Le but de ce second TP sur les mécanismes internes du noyau va nous
  permettre d'utiliser les commandes et les appels système relatifs
  aux *namespaces* ainsi que d'appréhender la complexité des systèmes
  de fichiers.

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le mardi 15
  novembre 2022 à 23 h 42. Consultez les sections matérialisées par un
  bandeau jaunes et un engrenage pour plus d'informations sur les
  éléments à rendre.
...
