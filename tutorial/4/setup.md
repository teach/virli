### Prérequis


#### Noyau Linux

Pour pouvoir suivre les exercices ci-après, vous devez disposer d'un noyau
Linux, idéalement dans sa version 5.6 ou mieux. Il doit de plus être compilé
avec les options suivantes (lorsqu'elles sont disponibles pour votre version) :

<div lang="en-US">
```
General setup  --->
    [*] Control Group support  --->
    -*- Namespaces support
      [*]   UTS namespace
      [*]   TIME namespace
      [*]   IPC namespace
      [*]   User namespace
      [*]   PID Namespaces
      [*]   Network namespace
[*] Networking support  --->
    Networking options  --->
      <M> 802.1d Ethernet Bridging
Device Drivers  --->
    [*] Network device support  --->
      <M>   MAC-VLAN support
      <M>   Virtual ethernet pair device
```
</div>

Les variables de configuration correspondantes sont :

<div lang="en-US">
```
CONFIG_CGROUPS=y

CONFIG_NAMESPACES=y
CONFIG_UTS_NS=y
CONFIG_TIME_NS=y
CONFIG_IPC_NS=y
CONFIG_USER_NS=y
CONFIG_PID_NS=y
CONFIG_NET_NS=y

CONFIG_NET=y
CONFIG_BRIDGE=m

CONFIG_NETDEVICES=y
CONFIG_MACVLAN=m
CONFIG_VETH=m
```
</div>

Référez-vous, si besoin, à la précédente configuration que l'on a faite pour la
marche à suivre.


#### Paquets

Nous allons utiliser des programmes issus des
[`util-linux`](https://www.kernel.org/pub/linux/utils/util-linux/), de
[`procps-ng`](https://gitlab.com/procps-ng/procps) ainsi que ceux de la
[`libcap`](https://sites.google.com/site/fullycapable/).

Sous Debian et ses dérivés, ces paquets sont respectivement :

* `util-linux`
* `procps`
* `libcap2-bin`

Sous ArchLinux et ses dérivés, ces paquets sont respectivement :

* `util-linux`
* `procps-ng`
* `libcap`


#### À propos de la sécurité de l'espace de noms `user`

La sécurité du *namespace* `user` a souvent été remise en cause par le passé,
on lui attribue de nombreuses vulnérabilités. Vous devriez notamment consulter
à ce sujet :

* [Security Implications of User Namespaces](https://blog.araj.me/security-implications-of-user-namespaces/) :\
  <https://blog.araj.me/security-implications-of-user-namespaces/> ;
* [Anatomy of a user namespaces vulnerability](https://lwn.net/Articles/543273/) :\
  <https://lwn.net/Articles/543273/> ;
* <http://marc.info/?l=linux-kernel&m=135543612731939&w=2> ;
* <http://marc.info/?l=linux-kernel&m=135545831607095&w=2> ;
* <https://www.openwall.com/lists/oss-security/2024/04/14/1>.

De nombreux projets ont choisi de ne pas autoriser l'utilisation de cet espace
de noms sans disposer de certaines *capabilities*[^userns-caps].

[^userns-caps]: Sont nécessaires, conjointement : `CAP_SYS_ADMIN`, `CAP_SETUID` et `CAP_SETGID`.

::::: {.question}
De nombreuses distributions ont choisi d'utiliser un paramètre du noyau pour
adapter le comportement.

##### Debian et ses dérivées {.unnumbered}

Si vous utilisez Debian ou l'un de ses dérivés, vous devrez autoriser
explicitement cette utilisation non-privilégiée :

<div lang="en-US">
```bash
42sh# sysctl -w kernel.unprivileged_userns_clone=1
```
</div>


##### Grsecurity {.unnumbered}

D'autres patchs, tels que
[*grsecurity*](https://forums.grsecurity.net/viewtopic.php?f=3&t=3929#p13904)
ont fait le choix de désactiver cette possibilité sans laisser
d'option pour la réactiver éventuellement à l'exécution. Pour avoir un
comportement identique à celui de Debian, vous pouvez appliquer ce
patch[^grsec-patch] sur vos sources incluant le patch de *grsecurity*.

[^grsec-patch]: <https://nemunai.re/post/user-ns-for-grsecurity/>

:::::
