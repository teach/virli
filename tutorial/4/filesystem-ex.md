::::: {.exercice}

## Exercice {-}

Continuons notre script `registry_play` de la partie précédente afin d'être en
mesure d'extraire également les images en plusieurs couches.

<div lang="en-US">
```bash
42sh$ cd $(mktemp -d)

42sh$ ./registry_play library/python:latest

42sh$ tree -L 1
.
./lower0
./lower1
./lower2
./lower3
./lower4
./lower5
./lower6
./lower7
./lower8
./upper
./work
./rootfs

42sh# chroot rootfs /usr/bin/python3
Python 3.23.0 (main) on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> _
```
</div>

:::::
