## Pour aller plus loin

* [Unioning file systems: Architecture, features, and design choices](https://lwn.net/Articles/324291/) :\
  <https://lwn.net/Articles/324291/>

Des expériences sont en cours pour essayer de faire un format
d'archive pour les couches qui permettrait d'utiliser les conteneurs
sans avoir eu besoin préalablement d'avoir téléchargé le contenu des
couches :
<https://github.com/containerd/stargz-snapshotter/blob/main/docs/estargz.md>
