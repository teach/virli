---
title: Virtualisation légère -- Linux Internals partie 2
subtitle: Support de cours
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 7 novembre 2018
abstract: |
  Le but de cette seconde partie sur les mécanismes internes du noyau
  va nous permettre d'utiliser les commandes et les appels système
  relatifs aux espaces de noms du noyau Linux ainsi que d'appréhender
  la complexité des sytèmes de fichiers.
...
