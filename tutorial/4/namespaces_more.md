### Aller plus loin  {-}

Je vous recommande la lecture du *man* `namespaces(7)` introduisant et
énumérant les *namespaces*.

Pour tout connaître en détails, [la série d'articles de Michael Kerrisk sur les
*namespaces*](https://lwn.net/Articles/531114/)[^lwnns1] est excellente ! Auquel il faut
ajouter [l'article sur le plus récent `cgroup`
*namespace*](https://lwn.net/Articles/621006/)[^lwnns2] et [le petit dernier sur le
*namespace* `time`](https://lwn.net/Articles/766089/)[^lwnns3].

[^lwnns1]: <https://lwn.net/Articles/531114/>
[^lwnns2]: <https://lwn.net/Articles/621006/>
[^lwnns3]: <https://lwn.net/Articles/766089/>
