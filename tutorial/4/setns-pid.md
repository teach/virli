\

D'une manière similaire à notre dernier exemple, depuis Linux 5.8, `setns(2)`
peut utiliser comme premier argument, un *file descriptor* pointant un
processus (`pidfd`).

Eh oui, outre l'obtention d'un *file descriptor* sur un lien symbolique étrange
d'une structure du noyau, il est possible d'en obtenir un sur un processus :

- soit en réalisant un `open(2)` d'un dossier `/proc/<PID>` ;
- soit en utilisant l'appel système `pidfd_open(2)`, en précisant l'identifiant
  du processus dont on souhaite obtenir le *file descriptor* ;
- soit en retour d'un `clone(2)` avec l'option `CLONE_PIDFD`.

<div lang="en-US">
```bash
int fd = pidfd_open(42, 0);
setns(fd, CLONE_NEWUSER | CLONE_NEWNET | CLONE_NEWUTS);
```
</div>

À travers cet exemple, on cherche à récupérer un *file descriptor* pour le
processus 42.  On le passe ensuite à `setns(2)` en précisant que l'on ne
souhaite rejoindre que les espaces de noms Utilisateurs, Réseau et UTS (nom de
la machine).
