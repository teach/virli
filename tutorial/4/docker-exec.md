\newpage

::::: {.exercice}

`docker exec`
-------------

Après voir lu la partie concernant les *namespaces*, vous avez dû comprendre
qu'un `docker exec`, n'était donc rien de plus qu'un `nsenter(1)`.

Réécrivons, en quelques lignes, la commande `docker exec` !

Pour savoir si vous avez réussi, comparez les sorties des commandes :

- `ip address` ;
- `hostname` ;
- `mount` ;
- `ps -aux` ;
- ...


Voici quelques exemples pour tester :

<div lang="en-US">
```
42sh$ docker run --name mywebsrv -d -p 80:80 nginx
d63ceae863956f8312aca60b7a57fbcc1fdf679ae4c90c5d9455405005d4980a
42sh$ docker container inspect --format '{{ .State.Pid }}' mywebsrv
234269

42sh# ./mydocker_exec mywebsrv ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
	  inet 127.0.0.1/8 scope host lo
	      valid_lft forever preferred_lft forever
13: eth0@if14: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP
     link/ether 02:42:ac:11:00:02 brd ff:[...]:ff link-netnsid 0
     inet 172.17.0.1/16 scope global eth0
         valid_lft forever preferred_lft forever

42sh# hostname
koala.zoo.paris
42sh# ./mydocker_exec mywebsrv hostname
d63ceae86395

42sh# ./mydocker_exec mywebsrv mount
42sh# ./mydocker_exec mywebsrv ps aux
...
```
</div>

:::::
