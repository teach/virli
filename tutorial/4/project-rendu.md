Modalités de rendu
==================

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire les
vérifications nécessaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>. C'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Pour différencier le rendu du TP, du rendu du projet, ajoutez une balise
`[PROJET]` au sujet de votre courriel, afin qu'il soit traité comme tel.
N'hésitez pas à indiquer dans le corps du courriel votre
ressenti et vos difficultés ou bien alors écrivez votre meilleure histoire
drôle si vous n'avez rien à dire.

Tarball
-------

Le projet à rendre pour ce cours est à placer dans une tarball (pas d'archive
ZIP, RAR, ...).

Voici une arborescence type:

<div lang="en-US">
```
login_x-mymoulette/README
login_x-mymoulette/...
```
</div>
