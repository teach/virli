### Durée de vie d'un *namespace*  {#ns-lifetime}

Le noyau tient à jour un compteur de références pour chaque *namespace*. Dès
qu'une référence tombe à 0, la structure de l'espace de noms est
automatiquement libérée, les points de montage sont démontés, les interfaces
réseaux sont réattribuées à l'espace de noms initial, ...

Ce compteur évolue selon plusieurs critères, et principalement selon le nombre
de processus qui l'utilisent. C'est-à-dire que, la plupart du temps, le
*namespace* est libéré lorsque le dernier processus s'exécutant dedans se
termine.

Lorsque l'on a besoin de référencer un *namespace* (par exemple pour le faire
persister après le dernier processus), on peut utiliser un `mount bind` :

<div lang="en-US">
```bash
42sh# touch /tmp/ns/myrefns
42sh# mount --bind /proc/<PID>/ns/mount /tmp/ns/myrefns
```
</div>

De cette manière, même si le lien initial n'existe plus (si le `<PID>` s'est
terminé), `/tmp/ns/myrefns` pointera toujours au bon endroit.

Il est aussi tout à fait possible d'utiliser directement ce fichier pour
obtenir un descripteur de fichier valide vers le *namespace* (pour passer à
`setns(2)`).


::::: {.question}
#### Faire persister un *namespace* ? {-}
\

Il n'est pas possible de faire persister un espace de noms d'un reboot à
l'autre.\

Même en étant attaché à un fichier du disque, il s'agit d'un pointeur vers une
structure du noyau, qui ne persistera pas au redémarrage.
:::::
