\newpage

Projet et rendu
===============

Projet
------

Réalisez le `docker-compose.yml` permettant de lancer toute notre stack de
monitoring, d'un simple :

<div lang="en-US">
```
42sh$ docker-compose up
```
</div>

Vous intégrerez les trois images (`influxdb`, `chronograf` et `telegraf`),
mettrez en place les *volumes* et *networks* nécessaire au bon fonctionnement
de la stack.

Le résultat final attendu doit permettre d'afficher dans `chronograf` l'hôte
auto-monitoré par la stack, sans plus de configuration. Vous aurez pour cela
éventuellement besoin de placer des fichiers de configuration à côté de votre
`docker-compose.yml`, afin de pouvoir inclure ces configurations dans les
conteneurs, sans avoir besoin de reconstruire ces conteneurs.


Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Par ailleurs, n'oubliez pas de répondre à
[l'évaluation du cours](https://virli.nemunai.re/quiz/3).


Tarball
-------

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
login_x-TP1/
login_x-TP1/ficadmin-run.sh
login_x-TP1/docker-compose.yml
login_x-TP1/...
```
</div>


## Signature du rendu

Deux méthodes sont utilisables pour signer votre rendu :

* signature du courriel ;
* signature de la tarball.

Dans les deux cas, si vous n'en avez pas déjà une, vous devrez créer une clef
PGP à **votre nom et prénom**.

Pour valider la signature, il est nécessaire d'avoir reçu la clef publique
**séparément**. Vous avez le choix de l'uploader sur un serveur de clefs, soit
de me fournir votre clef en main propre, soit l'envoyer dans un courriel
distinct.

### Signature du courriel

[Enigmail](https://enigmail.net) est une extension très bien réputée pour
signer ses mails depuis Thunderbird.

Utilisez le service automatique <signcheck@nemunai.re> pour savoir si votre
courriel est correctement signé et que je suis en mesure de vérifier la
signature.


### Astuces

#### No public key

Si vous recevez un rapport avec l'erreur suivante :

<div lang="en-US">
```
[FAIL] Bad signature. Here is the gnupg output:

gpg: Signature made Tue Jan 01 16:42:23 2014 CET
gpg:                using RSA key 842807A84573CC96
gpg: requesting key E2CCD99DD37BD32E from hkp server keys.openpgp.org
gpg: Can't check signature: No public key
```
</div>

C'est que votre clef publique n'est pas dans mon trousseau et que les
méthodes de récupération automatique n'ont pas permis de la
trouver. Uploadez votre clef sur [un serveur de
clefs](https://keys.openpgp.org/) ou envoyez un courriel au service
avec votre clef publique en pièce-jointe, avant de retenter votre
rendu.


#### Not explicit username

Si vous recevez un rapport avec l'erreur suivante :

<div lang="en-US">
```
[FAIL] The username of your key is not explicit, I can't find you.
```
</div>

Votre clef ne contient sans doute pas vos noms et prénoms ou l'adresse
électronique associée à la clef n'est pas celle que j'ai dans ma base de
données.


#### I've decided to skip your e-mail

Si vous recevez un rapport concluant ainsi :

<div lang="en-US">
```
After analyzing your e-mail, I've decided to SKIP it.
```
</div>

Cela signifie que la lecture de votre courriel qui a été préférée n'est pas
celle d'un rendu. Vérifiez que vous n'envoyez pas votre clef publique avec
votre rendu.
