\newpage

Orchestrer un groupe de conteneurs
==================================

Maintenant que nous savons démarrer individuellement des conteneurs et les lier
entre-eux, nous allons voir une première manière d'automatiser cela.

Plutôt que de lancer les commandes `docker` comme nous l'avons fait jusque-là :
soit directement dans un terminal, soit via un script, nous allons décrire
l'état que nous souhaitons atteindre : quelles images lancer, quels volumes
créer, quels réseaux, etc. Cette description peut s'utiliser pour lancer un
conteneur seul, mais elle prend tout son sens lorsqu'il faut démarrer tout un
groupe de conteneurs qui fonctionnent de concert, parfois avec des dépendances
(un serveur applicatif peut nécessiter d'avoir une base de données prête pour
démarrer).

On parle d'orchestration, car nous allons utiliser Docker comme un chef
d'orchestre : il va ordonner les créations des différents objets (volumes,
réseaux, conteneurs, ...) afin d'arriver au résultat attendu, puis il va faire
en sorte de maintenir ce résultat selon les événements qui pourront survenir.

---

Notre fil rouge dans cette partie sera la réalisation d'un système de
monitoring, tel que nous pourrions le déployer chez un fournisseur de cloud.

Le résultat attendu d'ici la fin de l'exercice, est un groupe de conteneurs
indépendants les uns des autres, réutilisables en fonction de besoins
génériques et pouvant facilement être mis à l'échelle.

Nous collecterons les données d'utilisation de votre machine avec
[Telegraf](https://www.influxdata.com/time-series-platform/telegraf/). Ces
données seront envoyées vers
[InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/), puis
elles seront affichées sous forme de graphique grâce à
[Chronograf](https://www.influxdata.com/time-series-platform/chronograf/).

![Dashboard de l'utilisation CPU et mémoire sur Chronograf](chronograf.png)


L'installation que nous allons réaliser est celle d'une plate-forme TICK. Il
s'agit d'un mécanisme de séries temporelles (*Time Series*) moderne, que l'on
peut utiliser pour stocker toute sorte de données liées à un indice temporel.

La pile logicielle TICK propose de collecter des métriques, en les enregistrant
dans une base de données adaptées et permet ensuite de les ressortir sous
forme de graphiques ou de les utiliser pour faire des alertes intelligentes.
