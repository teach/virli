Lier des conteneurs
-------------------

Avant de voir des méthodes plus automatiques pour déployer toute notre pile
logicielle TICK, nous allons commencer par mettre en place et lier les
conteneurs manuellement, de la même manière que nous avons pu le faire avec
MySQL. Cela nous permettra de voir les subtilités de chaque image, ce qui nous
fera gagner du temps pour ensuite en faire la description.


### Conteneur central : la base de données

Le premier conteneur qui doit être lancé est la base de données orientée séries
temporelles :
[InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/).
En effet, tous les autres conteneurs ont besoin de cette base de données pour
fonctionner correctement : il serait impossible à *Chronograf* d'afficher les
données sans base de données, tout comme *Telegraf* ne pourrait écrire les
métriques dans une base de données à l'arrêt.

Afin d'interagir avec les données, InfluxDB expose une
[API REST](https://fr.wikipedia.org/wiki/Representational_state_transfer)
sur le port 8086. Pour éviter d'avoir plus de configuration à réaliser, nous
allons tâcher d'utiliser ce même port pour tester localement :

<div lang="en-US">
```
docker container run -p 8086:8086 -d --name mytsdb influxdb:1.8
```
</div>

::::: {.warning}

Remarquez que nous n'utilisons pas la version 2 d'InfluxDB. Sa mise en
place est plus contraignantes pour faire de simples tests. Si vous
souhaitez tout de même utiliser la dernière version de la stack TICK,
vous pouvez consulter le `README` du conteneur sur le Docker Hub :\
<https://hub.docker.com/_/influxdb>

:::::

Comme il s'agit d'une API REST, nous pouvons vérifier le bon fonctionnement de
notre base de données en appelant :

<div lang="en-US">
```
42sh$ curl -f http://localhost:8086/ping
42sh$ echo $?
0
```
</div>

Notez que comme nous avons lancé le conteneur en mode détaché (option `-d`),
nous ne voyons pas les logs qui sont écrits par le daemon. Pour les voir, il
faut utiliser la commande `docker container logs` :

<div lang="en-US">
```
docker container logs mytsdb
```
</div>

Si votre influxdb répond, vous pouvez vous y connecter en utilisant directement
le client officiel (le binaire s'appelle `influx`) :

<div lang="en-US">
```
42sh$ docker container run --rm -it --link mytsdb:influxdb influxdb:1.8 \
    influx -host influxdb
Connected to http://influxdb:8086 version 1.8.10
InfluxDB shell version: 1.8.10
> show databases
name: databases
name
---------------
_internal
```
</div>

Si vous aussi vous voyez la table `_internal`, bravo ! vous pouvez passer à la
suite.

#### Mais quelle était cette commande magique ? {-}

Oui, prenons quelques minutes pour l'analyser ...

L'option `--link` permet de lier deux conteneurs. Ici nous souhaitons lancer un
nouveau conteneur pour notre client, en ayant un accès réseau au conteneur
`mytsdb` que nous avons créé juste avant. L'option `--link` prend en argument
le nom d'un conteneur en cours d'exécution (ici `mytsdb`, nom que l'on a
attribué à notre conteneur exécutant la base de données influxdb via l'option
`--name`), après les `:`, on précise le nom d'hôte que l'on souhaite attribuer
à cette liaison, au sein de notre nouveau conteneur.

`influx -host influxdb` que nous avons placé après le nom de l'image,
correspond à la première commande que l'on va exécuter dans notre conteneur, à
la place du daemon `influxdb`. Ici nous voulons exécuter le client, il
s'appelle `influx`, auquel nous ajoutons le nom de l'hôte dans lequel nous
souhaitons nous connecter : grâce à l'option `--link`, le nom d'hôte à utiliser
est `influxdb`.

L'option `--link` peut être particulièrement intéressante lorsque l'on souhaite
sauvegarder une base, car en plus de définir un nom d'hôte, cette option fait
hériter le nouveau conteneur des variables d'environnement du conteneur
auquel elle est liée : on a donc accès aux `$MYSQL_USER`,
`$MYSQL_PASSWORD`, ...

On aurait aussi pu créer un réseau entre nos deux conteneurs (via `docker
network`), ou bien encore, on aurait pu exécuter notre client `influx`
directement dans le conteneur de sa base de données :

<div lang="en-US">
```
42sh$ docker container exec mytsdb influx -execute "show databases"
name: databases
name
---------------
_internal
```
</div>

Dans ce dernier exemple, nous n'avons pas besoin de préciser l'hôte, car
`influx` va tenter `localhost` par défaut, et puisque nous lançons la commande
dans notre conteneur `mytsdb`, il s'agit bien du conteneur où s'exécute
localement `influxdb`.

::::: {.exercice}

Ajoutez à la ligne de commande de lancement du conteneur les bon(s) volume(s)
qui permettront de ne pas perdre les données d'influxDB si nous devions
redémarrer le conteneur. Aidez-vous pour cela de la [documentation du
conteneur](https://hub.docker.com/_/influxdb).

:::::

### Collecter les données locales

Tentons maintenant de remplir notre base de données avec les métriques du
système. Pour cela, on commence par télécharger *Telegraf* :

<div lang="en-US">
```bash
V=1.23.4
P=telegraf-${V}_linux_$(uname -m)
curl https://dl.influxdata.com/telegraf/releases/${P}.tar.gz | \
tar xzv -C /tmp
```
</div>

Puis, lançons *Telegraf* :

<div lang="en-US">
```bash
cd /tmp/telegraf
./usr/bin/telegraf --config etc/telegraf/telegraf.conf
```
</div>

::::: {.more}

La configuration par défaut va collecter les données de la machine locale et
les envoyer sur le serveur situé à <http://localhost:8086>. Ici, cela
fonctionne parce que l'on a fait en sorte de rediriger le port de notre
conteneur sur notre machine locale (option `-p`).

:::::

Et observons ensuite :

<div lang="en-US">
```bash
42sh$ docker container run --rm -it --link mytsdb:zelda influxdb:1.8 \
    influx -host zelda
InfluxDB shell version: 1.8.10
> show databases
name: databases
name
---------------
_internal
telegraf

> use telegraf
Using database telegraf

> show measurements
name: measurements
name
------------------
cpu
disk
diskio
kernel
mem
processes
swap
system
```
</div>

La nouvelle base a donc bien été créée et tant que nous laissons *Telegraf*
lancé, celui-ci va régulièrement envoyer des métriques de cette machine.

::::: {.exercice}

### Afficher les données collectées

À vous de jouer pour lancer le conteneur
[*Chronograf*](https://store.docker.com/images/chronograf).

::::: {.question}

#### InfluxDB v2 {-}

Chronograf n'existe plus en tant que projet indépendant dans la version 2, si
vous êtes parti sur cette version, vous retrouverez les tableaux de bord
directement dans l'interface d'InfluxDB, sur le port 8086.

:::::

L'interface de *Chronograf* est disponible sur le port 8888.

Consultez la [documentation du conteneur](https://hub.docker.com/_/chronograf)
si besoin.

::::: {.warning}

La page d'accueil est vide au démarrage, pour savoir si vous avez réussi,
rendez-vous sous l'onglet *Hosts*, le nom de votre machine devrait y
apparaître. En cliquant dessus, vous obtiendrez des graphiques similaires à
ceux ci-après :

:::::

:::::

![Résultat obtenu](chronograf_latest.png)
