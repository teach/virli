#!/bin/sh

note_init() {
    NOTE=0
    echo -n $@
}

note() {
    NOTE=$(($NOTE + $1))
    echo -n ,$@
}

for LOGIN in $@
do
    note_init $LOGIN
    pushd $LOGIN > /dev/null

    # Questions

    if grep -i "go" questions.txt 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi

    if grep -E -i "(linkage|static|statique|liaison)" questions.txt 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi


    # Exercice InfluxDB

    DOCKERFILE_influxdb="influxdb/Dockerfile"
    if ! [ -f "${DOCKERFILE_influxdb}" ] && [ -f "influxdb/dockerfile" ]; then
	DOCKERFILE_influxdb="influxdb/dockerfile"
    fi

    NBRUN=$(grep -E -i ^RUN "${DOCKERFILE_influxdb}" 2> /dev/null | wc -l)
    if [ $NBRUN -le 2 ] && [ $NBRUN -gt 0 ]; then
	note 1
    else
	note 0
    fi

    if grep -E -i '^EXPOSE.*8083' "${DOCKERFILE_influxdb}" 2> /dev/null > /dev/null && grep -E -i '^EXPOSE.*8086' "${DOCKERFILE_influxdb}" 2> /dev/null > /dev/null && \
	    grep -i ^EXPOSE "${DOCKERFILE_influxdb}" 2> /dev/null | sed "s/ 8083//g;s/ 8086//g" | grep -E '^EXPOSE[[:space:]]*$' > /dev/null; then
	note 1
    else
	note 0
    fi

    if grep -E -i '^MAINTAINER[[:space:]]' "${DOCKERFILE_influxdb}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi

    if grep -E -i '^(ADD|COPY)[[:space:]]' "${DOCKERFILE_influxdb}" 2> /dev/null > /dev/null; then
	CONFIGFILE=$(grep -E -i '^(ADD|COPY)[[:space:]]' "${DOCKERFILE_influxdb}" | sed -r 's/^(COPY|ADD)[[:space:]]+([^[:space:]]*).*$/\2/')
	if [ -f "influxdb/${CONFIGFILE}" ]; then
	    note 1
	else
	    note 0
	fi
    else
	note 0
    fi


    # Exercice mymonitoring

    DOCKERFILE_mymonitoring="mymonitoring/Dockerfile"
    if grep -E -i '^FROM[[:space:]]' "${DOCKERFILE_mymonitoring}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi

    if grep -E -i '^ENV[[:space:]]' "${DOCKERFILE_mymonitoring}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi

    CONFIGFILE=$(grep -E -i '^(ADD|COPY)[[:space:]]' "${DOCKERFILE_mymonitoring}" 2> /dev/null | grep -vi "influx" | grep -vi "chrono" | sed -r 's/^(COPY|ADD)[[:space:]]+([^[:space:]]*).*$/\2/')
    if ! [ -f "mymonitoring/${CONFIGFILE}" ]; then
	CONFIGFILE="mymonitoring/supervisor.conf"
    fi
    if [ -f "mymonitoring/${CONFIGFILE}" ]; then
	ERRS=0

	if grep -E -i "command=.*service.*start" "mymonitoring/${CONFIGFILE}" > /dev/null; then
	    ERRS=$(($ERRS + 1))
	fi

	note $((2 - $ERRS))
    else
	note 0
    fi


    # Exercice docker-compose

    DOCKERCOMPOSE="docker-compose.yml"

    NBBUILD=$(grep -E -i "build[[:space:]]*:" "${DOCKERCOMPOSE}" 2> /dev/null | wc -l)
    if [ $NBBUILD -ge 2 ]; then
	note 2
    elif [ $NBBUILD -ge 1 ]; then
	note 1
    else
	note 0
    fi

    NBVOLS=$(grep -E -i "volumes[[:space:]]*:" "${DOCKERCOMPOSE}" 2> /dev/null | wc -l)
    if [ $NBVOLS -ge 2 ]; then
	note 2
    elif [ $NBVOLS -ge 1 ]; then
	note 1
    else
	note 0
    fi

    NBNET=$(grep -E -i "networks[[:space:]]*:" "${DOCKERCOMPOSE}" 2> /dev/null | wc -l)
    NBLINK=$(grep -E -i "networks[[:space:]]*:" "${DOCKERCOMPOSE}" 2> /dev/null | wc -l)
    if [ $NBNET -ge 2 ]; then
	note 2
    elif [ $NBNET -ge 1 ]; then
	note 1
    elif [ $NBLINK -ge 1 ]; then
	note 2
    else
	note 0
    fi


    echo #" = $NOTE"
    popd > /dev/null
done
