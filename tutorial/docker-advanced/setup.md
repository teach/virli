Mise en place
-------------

Jusqu'ici, nous avons utilisé l'environnement Docker principal, qui inclut le
client, le daemon et toute sa machinerie. Mais le projet Docker propose de
nombreuses extensions, souvent directement trouvées dans les usages de la
communauté, et parfois même appropriées par Docker.


### `docker-compose`

Dans cette partie, nous allons avoir besoin du plugin `docker-compose`.

L'équipe en charge du projet met à disposition un exécutable que nous pouvons
téléchargeant depuis <https://github.com/docker/compose/releases>.

Ajoutez l'exécutable dans le dossier des plugins : `$HOME/.docker/cli-plugins`
(sans oublier de `chmod +x` !).

::::: {.more}

Autrefois, `docker-compose` était un script tiers que l'on utilisait
indépendamment de Docker. Le projet, historiquement écrit en Python, a été
entièrement réécrit récemment afin qu'il s'intégre mieux dans l'écosystème.

Vous trouverez encore de nombreux articles vous incitant à utiliser
`docker-compose`. Dans la plupart des cas, vous pouvez simplement remplacer par
des appels à `docker compose`.

Il y a même un outil qui a spécialement été conçu pour migrer les lignes de
commandes :\
<https://github.com/docker/compose-switch>

:::::

#### Vérification du fonctionnement

Comme avec Docker, nous pouvons vérifier le bon fonctionnement de
`docker-compose` en exécutant la commande :

<div lang="en-US">
```
42sh$ docker compose version
Docker Compose version v2.10.2
```
</div>

Si vous obtenez une réponse similaire, c'est que vous êtes prêt à continuer !
Alors n'attendons pas, partons à l'aventure !


### Play With Docker

Tout comme pour la partie précédente, si vous avez des difficultés pour
réaliser les exercices sur votre machine, vous pouvez utiliser le projet [Play
With Docker](https://play-with-docker.com/) qui vous donnera accès à un bac à
sable avec lequel vous pourrez réaliser tous les exercices.
