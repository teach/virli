Composition de conteneurs
-------------------------

### Automatiser le lancement de conteneurs

Au lieu de faire un script pour construire et lancer tous vos conteneurs, nous
allons définir à la racine de notre projet un fichier `docker-compose.yml` qui
contiendra les paramètres d'exécution.

<div lang="en-US">
```yaml
version: "3.9"
services:
  influxdb:
    ...
  chronograf:
    build: grafana/
    image: nginx
    ports:
      - "3000:3000"
    volumes:
      - ./:/tmp/toto
    links:
      - influxdb
```
</div>

Ce fichier est un condensé des options que nous passons habituellement au
`docker container run`.

#### `version`

Notons toutefois la présence d'une ligne `version` ; il ne s'agit pas de la
version de vos conteneurs, mais de la version du format de fichier
`docker-compose` qui sera utilisé. Sans indication de version, la version
originale sera utilisée, ne vous permettant pas d'utiliser les dernières
fonctionnalités de Docker.


#### `services`

Cette section énumère la liste des services (ou conteneurs) qui seront gérés
par `docker compose`.

Ils peuvent dépendre d'une image à construire localement, dans ce cas ils
auront un fils `build`. Ou ils peuvent utiliser une image déjà existante, dans
ce cas ils auront un fils `image`.

Les autres fils sont les paramètres classiques que l'on va passer à `docker
run`.


#### `volumes`

Cette section est le pendant de la commande `docker volume`.

On déclare les volumes simplement en leur donnant un nom et un driver comme
suit :

<div lang="en-US">
```yaml
volumes:
  mysql-data:
    driver: local
```
</div>

Pour les utiliser avec un conteneur, on référence le nom ainsi que
l'emplacement à partager :

<div lang="en-US">
```yaml
[...]
  mysql:
    [...]
    volumes:
	  - mysql-data:/var/lib/mysql
```
</div>


#### `network`

Cette section est le pendant de la commande `docker network`.

Par défaut, Docker relie tous les conteneurs sur un bridge et fait du NAT pour
que les conteneurs puissent accéder à l'Internet. Mais ce n'est pas le seul
mode possible !

De la même manière que pour les `volumes`, cette section déclare les réseaux
qui pourront être utilisés par les `services`. On pourrait donc avoir :

<div lang="en-US">
```yaml
networks:
  knotdns-slave-net:
    driver: bridge
```
</div>


##### Driver `host`

Le driver `host` réutilise la pile réseau de la machine hôte. Le conteneur
pourra donc directement accéder au réseau, sans NAT et sans redirection de
port. Les ports alloués par le conteneur ne devront pas entrer en conflit avec
les ports ouverts par la machine hôte.


##### Driver `null`

Avec le driver `null`, la pile réseau est recréée et aucune interface (autre
que l'interface de loopback) n'est présente. Le conteneur ne peut donc pas
accéder à Internet, ni aux autres conteneurs, ...

Lorsque l'on exécute un conteneur qui n'a pas besoin d'accéder au réseau, c'est
le driver à utiliser. Par exemple pour un conteneur dont le but est de vérifier
un backup de base de données.


##### Driver `bridge`

Le driver `bridge` crée un nouveau bridge qui sera partagé entre tous les
conteneurs qui la référencent.

Avec cette configuration, les conteneurs ont accès à une résolution DNS des
noms de conteneurs qui partagent leur bridge. Ainsi, sans avoir à utiliser la
fonctionnalité de `link` au moment du `run`, il est possible de se retrouver
lié, même après que l'on ait démarré. La résolution se fera dynamiquement.


#### Utiliser le `docker-compose.yml`

Consultez la documentation[^COMPOSEDOC] pour une liste exhaustive des options
que nous pouvons utiliser.

[^COMPOSEDOC]: La documentation des `docker-compose.yml` :
<https://docs.docker.com/compose/compose-file/>

Une fois que notre `docker-compose.yml` est prêt, nous pouvons lancer
la commande suivante et admirer le résultat :

<div lang="en-US">
```bash
docker compose up
```
</div>

Encore une fois, testez la bonne connexion entre `chronograf` (accessible sur
<http://localhost:8888>) et `influxdb`.
