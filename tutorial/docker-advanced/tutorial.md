---
title: Virtualisation légère -- TP n^o^ 1.2
subtitle: Les bases de Docker -- compose
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 2 octobre 2019
abstract: |
  Dans cette deuxième partie du TP, nous allons apprendre à déployer
  un groupe de conteneurs !

  \vspace{1em}

  Le TP se termine par un petit projet à rendre à <virli@nemunai.re>
  au plus tard le mercredi 16 octobre 2019 à 13 h 42, des questions de
  cours sont également à compléter avant cette date sur
  Epitaf. Consultez la dernière partie de ce TP pour les modalités.

  En tant que personnes sensibilisées à la sécurité des échanges
  électroniques, vous devrez m'envoyer vos rendus signés avec votre
  clef PGP. Pensez à
  [me](https://keys.openpgp.org/search?q=nemunaire%40nemunai.re)
  faire signer votre clef et n'hésitez pas à [faire signer la
  vôtre](https://www.meetup.com/fr/Paris-certification-de-cles-PGP-et-CAcert/).
...
