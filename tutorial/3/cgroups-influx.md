::::: {.exercice}

### Exercice (obligatoire pour les SRS -- optionnel pour les GISTRE) {-}

Poursuivons [notre script de monitoring](#script-monitoring) afin d'envoyer nos
résultats vers InfluxDB : nous l'appellerons `./telegraf.sh`.

### Rappel d'InfluxDB

Commençons par lancer le conteneur Docker d'InfluxDB (pour éviter de
l'installer sur notre machine) :

<div lang="en-US">
```bash
docker container run --name mytsdb -d -p 8086:8086 influxdb:1.8
```
</div>

::::: {.warning}

Nous utilisons la version 1.8 d'influxDB qui est plus simple à administrer pour
faire des tests. Vous pouvez partir sur la version 2, une API compatible avec
la version 1 est disponible, elle est plus simple à utiliser à partir d'un
shell.

:::::

Il nous faut ensuite créer une base de données pour y stocker nos
métriques. Voici comment on s'était débrouillé précédemment pour interagir avec
InfluxDB :

<div lang="en-US">
```bash
docker container exec -i mytsdb influx <<EOF
CREATE DATABASE metrics;
SHOW DATABASES;
EOF
```
</div>

Vérifiez que la base de données `metrics` a bien été créée.


### Monitoring vers InfluxDB

Maintenant, envoyons nos données vers la base
<https://docs.influxdata.com/influxdb/v1.8/guides/write_data/> :

<div lang="en-US">
```bash
curl -i -XPOST 'http://localhost:8086/write?db=metrics' --data-binary \
    "$my_cgroup_name memory.usage_in_bytes=$(cat .../my_cgroup_name/memory.usage_in_bytes)"
```
</div>

Pour vérifier que les données ont bien été ajoutées, nous pouvons effectuer la
requête suivante dans notre client `influx` :

<div lang="en-US">
```sql
SELECT * from "$my_cgroup_name";
```
</div>


### Monitorer davantage de données

Liste non exhaustive de données à monitorer :

* Nombre d'IOs effectué ;
* nombre d'octets lus/écrits sur les disques ;
* temps de calcul utilisé (`userspace`, `system`, tout confondu) ;
* ...

Tous les cgroups existants dans le dernier noyau publié ont leur documentation
accessible ici :

- v1 : <https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/index.html>
- v2 : <https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html>


### Permettre à l'utilisateur de monitorer des processus

Maintenant, séparons notre script en deux parties afin qu'un utilisateur normal
(non-root) puisse utiliser la partie monitoring de notre script. La procédure
sera différente suivant la version des *cgroup*s que vous utilisez.

#### *cgroup*s v1\

Un premier script doit s'occuper de créer le(s) *cgroup*s et lui attribuer les
bons droits (`chown $EUID`), tandis que le deuxième va effectuer le monitoring,
sans privilèges particuliers.

##### Exemple {-}
\
<div lang="en-US">
```
42sh$ sudo ./telegraf_init.sh my_cgroup_name
42sh$ ./telegraf.sh my_cgroup_name firefox
```
</div>


#### *cgroup*s v2\

On part du principe que `systemd` est le système d'initialisation de votre
machine et qu'il gère lui-même l'arborescence.

Dans cette version des *cgroup*s, on peut toujours déléguer une partie de
l'arborescence à un utilisateur, au moyen d'un `chown`, cependant une
contrainte supplémentaire entre en jeu. Lorsque l'on veut déplacer un
processus, il faut avoir les droits d'écriture à la fois sur la destination,
mais également sur la source, et sur tous les nœuds de l'arborescence que l'on
peut croiser lors du parcourt de l'arbre.

Cela signifie, qu'après avoir `mkdir -p virli/child1 virli/child2 && chown -R
$EUID:$EUID virli`, il est possible de déplacer un processus de :

```
/virli/child1
```

vers

```
/virli/child2
```

car on dispose bien des droits d'écriture sur l'ancêtre commun le plus proche
(à savoir `/virli`).

Les problèmes surviennent lorsque l'on souhaite déplacer un processus que
`systemd` a placé dans :

```
/user.slice/user@$UID.service/app.slice/app-alacritty-0a1b2c3d4e5f6e7d9c
```

Il ne sera alors pas possible de le déplacer vers `/virli/child1`, car
l'ancêtre commun le plus proche est la racine des *cgroup*s, sur laquelle
l'utilisateur n'a pas les droits d'écriture.\

Pour faire cet exercice, vous allez donc devoir rechercher l'ancêtre commun le
plus proche sur lequel votre utilisateur a les droits d'écritures.

En utilisant `systemd` :

```
42sh$ systemctl --user show | grep ControlGroup
ControlGroup=/user.slice/user-1234.slice/user@1234.service
```

Ou bien en recherchant dans l'arborescence des *cgroup*s votre processus :

```
42sh$ find /sys/fs/cgroup/ -name cgroup.procs -exec grep -l $$ {} \;
```

... puis en remontant tant que l'on dispose des droits d'écritures.\


::::: {.question}

#### Mon processus se trouve dans un dossier `session-X.scope` propriété de `root` ! {-}
\
Vraisemblablement votre environnement graphique ne communique pas avec `systemd` à la connexion pour initialiser l'environnement.

Vous pouvez créer un environnement temporaire sain avec `systemd-run` :

<div lang="en-US">
```
42sh$ systemd-run --user --scope $0
```
</div>

Dans le nouveau processus obtenu (et **uniquement dans celui-ci et ses fils**), vous vous retrouverez dans un groupe dont votre utilisateur est bien propriétaire.

:::::


Le script `telegraf_init.sh` devra retourner le chemin vers le dossier
(éventuellement) créé à partir du nom passé en paramètre, depuis la racine des
*cgroup*s.

Ce retour servira de premier argument au script `telegraf.sh`.

##### Exemple {-}
\
<div lang="en-US">
```
42sh$ ./telegraf_init.sh my_cgroup_name
/user.slice/user@1234.service/my_cgroup_name
42sh$ ./telegraf.sh "/user.slice/user@1234.service/my_cgroup_name" firefox
```
</div>

:::::
