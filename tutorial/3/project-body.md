## Palier 1 : Restreindre l'environnement (2 points)  {-}

Après avoir mis en place les bases de votre programme, commencez par créer les
différentes hiérarchies (si vous avez un noyau récent, vous pouvez utiliser les
cgroups-v2) dont vous allez avoir besoin pour limiter l'utilisation de
ressources.

Puis, mettez en place ces limites :

* pas plus d'1 GB de mémoire utilisée ;
* 1 seul CPU au maximum ;
* 100 PIDs ;
* ...

En bonus, vous pouvez gérer les cas où le noyau sur lequel s'exécute votre
moulinette ne possède pas tous ces *CGroup*s, au lieu de planter, ne rien faire
n'est pas forcément une mauvaise solution.


## Palier 2 : Réduire les `capabilities` (2 points)  {-}

Réduisez au maximum les *capabilities*, de telle sorte qu'il ne soit pas
possible de faire un ping dans l'environnement restreint :

<div lang="en-US">
```
42sh# ping 9.9.9.9
PING 9.9.9.9 (9.9.9.9) 56(84) bytes of data.
64 bytes from 9.9.9.9: icmp_seq=1 ttl=56 time=3.93 ms
64 bytes from 9.9.9.9: icmp_seq=2 ttl=56 time=3.97 ms
^C
--- 9.9.9.9 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 3.931/3.954/3.978/0.067 ms

42sh# ./mymoulette /bin/bash
    bash# curl http://www.linuxcontainers.org/ | md5sum
    c7d68d1cb4737125a84cd69f55add202

    bash# ping 9.9.9.9
    ping: icmp open socket: Permission denied
```
</div>

**Astuces\ :** `prctl(2)`, `capabilities(7)`, `capget(2)`, `capset(2)`, ...

Aidez-vous du visualisateur de *capabilities* de la partie 1.3 du TP, pour voir
si vous êtes sur la bonne voie.

Si votre distribution vous permet d'utiliser `ping` sans privilège, utilisez à
la place n'importe quel binaire ayant besoin de *capabilities* pour vos tests,
par exemple `arping` (qui nécessite `CAP_NET_RAW`) ou `halt` (qui nécessite
`CAP_SYS_BOOT`) :

<div lang="en-US">
```
42sh# ./mymoulette /bin/bash
    bash# arping 192.168.0.1
    arping: socket: Permission denied

    bash# halt -f
    halt: (null): Operation not permitted
```
</div>


## Palier bonus : Utilisable par un utilisateur (2 points)  {-}

Jouez avec les attributs étendus pour qu'un utilisateur non-privilégié puisse
exécuter votre moulinette. Ajoutez la/les commande(s) à votre `Makefile` ou
script d'installation.


## Création d'un environnement d'exécution minimal  {-}

Plutôt que d'utiliser votre système hôte au complet, avec tous ses programmes
et toutes ses bibliothèques, il faudrait utiliser un système contenant le
strict minimum. Recréez un environnement minimaliste, comme on a pu en voir
dans la partie sur les `chroot`.

**Ne mettez pas cet environnement dans votre tarball de rendu, il vous
  sera seulement utile pour faire des tests.**


## Palier 3 : Isolation du pauvre (1 point)  {-}

Nous n'avons pas encore vu de meilleure méthode pour mieux isoler
l'environnement que de faire un `chroot`, ajoutez à votre programme cette
isolation rudimentaire. Et rendez-vous au prochain cours pour avoir une
meilleure isolation !

<div lang="en-US">
```
42sh$ which firefox
/usr/bin/firefox
42sh# ./mymoulette ./newrootfs/ /bin/bash
    bash# which firefox
    which: no firefox in (/usr/bin:/usr/local/bin:/bin:/opt/bin)
```
</div>


## Palier 4 : seccomp (2 points)  {-}

Filtrez les appels système de telle sorte qu'aucun programme exécuté dans
votre bac à sable ne puisse plus lancer les appels système suivants :

* `nfsservctl(2)` ;
* `personality(2)` ;
* `pivot_root(2)` ;
* ...

N'hésitez pas à en utiliser d'autres pour vos tests ;)

**Astuces\ :** `seccomp(2)`, `seccomp_init(3)`, `seccomp_load(3)`, ...
