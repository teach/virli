---
title: Virtualisation légère -- TP n^o^ 3
subtitle: Linux Internals partie 1
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: Mercredi 19 octobre 2022
abstract: |
  Ce premier TP consacré aux Linux Internals va nous permettre
  d'appréhender les notions de pseudos systèmes de fichiers, de
  *cgroups* ainsi que de *capabilities*.

  \vspace{1em}

  Les exercices de ce cours sont à rendre au plus tard le mardi 8
  novembre 2022 à 23 h 42. Consultez la dernière partie pour plus
  d'informations sur les éléments à rendre.
...
