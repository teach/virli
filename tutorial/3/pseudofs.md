Pseudos systèmes de fichiers
----------------------------

Les systèmes Unix définissent le système de fichiers comme étant un arbre
unique partant d'une racine[^FHS] et où l'on peut placer au sein de son arborescence
des points de montage. Ainsi, l'utilisateur définit généralement deux points de
montage :

[^FHS]: Consultez
    <https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard> pour
    plus de détails sur l'arboresence.

<div lang="en-US">
```
/dev/sda1 on / type ext4 (rw,relatime,data=ordered)
/dev/sda3 on /home type ext4 (rw,relatime,data=ordered)
```
</div>

Dans ce schéma, la racine correspond à la première partition du premier disque,
et les fichiers des utilisateurs sont sur la troisième partition du premier
disque.


### Présentation des pseudos systèmes de fichiers

D'autres points de montage sont utilisés par le système : `/dev`, `/proc`,
`/tmp`, ... Ces points de montage vont, la plupart du temps, être montés par le
programme d'initialisation en utilisant des systèmes de fichiers virtuels, mis
à disposition par le noyau.

Ces systèmes sont virtuels, car ils ne correspondent à aucune partition d'aucun
disque : l'arborescence est créée de toute pièce par le noyau pour trier les
informations mises à disposition, mais il n'est pas toujours possible d'y
apporter des modifications.

Linux emploie de nombreux systèmes de fichiers virtuels :

- `/proc` : contient, principalement, la liste des processus (`top` et ses
  dérivés se contentent de lire les fichiers de ce point de montage) ;
- `/proc/sys` : contient la configuration du noyau ;
- `/sys` : contient des informations à propos du matériel (utilisées notamment
  par `udev` pour peupler `/dev`) et des périphériques (taille des tampons,
  clignotement des DELs, ...) ;
- `/sys/firmware/efi/efivars` : pour accéder et modifier les variables de
  l'UEFI ;
- ...

Tous ces systèmes de fichiers sont généralement exclusivement stockés en
RAM. Pour rendre une modification persistante, il est nécessaire de modifier un
fichier de configuration qui sera chargé par le programme d'initialisation. Par
exemple, pour modifier les paramètres du noyau, on passe par le fichier
`/etc/sysctl.conf` et le programme `sysctl`.


### Consultation et modification

La consultation d'un élément se fait généralement à l'aide d'un simple `cat` :

<div lang="en-US">
```
42sh$ cat /sys/power/state
freeze mem
```
</div>

La modification d'un élément se fait avec `echo`, comme ceci :

<div lang="en-US">
```bash
42sh# echo mem > /sys/power/state
```
</div>

Vous devriez constater l'effet de cette commande sans plus attendre !


::::: {.exercice}

#### `procinfo`

Explorons le pseudo système de fichiers `/proc` pour écrire un script qui va
afficher des informations sur un processus donné :

<div lang="en-US">
```
42sh$ ./procinfo $$
PID: 4242
Path: /bin/bash
Command line: bash
Working directory: /home/nemunaire/virli/
Root: /
State: S (sleeping)
Threads: 1

CGroups
=======
12:pids:/
11:net_prio:/
10:perf_event:/
9:net_cls:/
8:freezer:/
7:devices:/
6:memory:/
5:blkio:/
4:cpuacct:/
3:cpu:/
2:cpuset:/
1:name=openrc:/

Namespaces
==========
cgroup:[4026531835]
ipc:[4026531839]
mnt:[4026531840]
net:[4026531969]
pid:[4026531836]
user:[4026531837]
uts:[4026531838]
```
</div>


#### `batinfo.sh`, `cpuinfo.sh`

Explorons le pseudo système de fichiers `/sys` pour écrire un script
qui va, en fonction de ce que vous avez de disponible :

* afficher des statistiques sur votre batterie ;
* afficher des statistiques sur la fréquence du CPU.

##### `batinfo.sh` {-}

Voici un exemple d'utilisation :

<div lang="en-US">
```
42sh$ ./batinfo.sh
BAT0 Discharging
====
Capacity: 83% (Normal)
Voltage: 11.972000 V (minimal: 11.400000 V)
Energy: 18.290000/21.830000 Wh
Power: 7.937000 W
Remaining time: 2.304 h

BAT1 Unknown
====
Capacity: 83% (Normal)
Voltage: 11.972000 V (minimal: 11.400000 V)
Energy: 18.290000/21.830000 Wh
Power: 0.0 W
Remaining time: N/A
```
</div>

Pour les détails sur l'organisation de ce dossier, regardez :\
<https://www.kernel.org/doc/Documentation/power/power_supply_class.txt>.

---

##### `cpuinfo.sh` {-}

Voici un exemple d'utilisation :

<div lang="en-US">
```
42sh$ ./cpuinfo.sh
cpu0
====
Current frequency: 2100384 Hz
Current governor: powersave
Allowed frequencies: between 500000 - 2100000 Hz
Thermal throttle count: 0

cpu1
====
Current frequency: 2099871 Hz
Current governor: powersave
Allowed frequencies: between 500000 - 2100000 Hz
Thermal throttle count: 0
```
</div>

N'hésitez pas à rajouter toute sorte d'informations intéressantes !


#### `rev_kdb_leds.sh`, `suspend_schedule.sh`

Maintenant que vous savez lire des informations dans `/sys`, tentons d'aller
modifier le comportement de notre système. Au choix, réalisez l'un des scripts
suivants, en fonction du matériel dont vous disposez :

* inverser l'état des diodes de votre clavier ;
* mettre en veille votre machine, en ayant programmé une heure de réveil.

##### `rev_kdb_leds.sh` {-}

Si vous avez :

* numlock On,
* capslock Off,
* scrolllock Off ;

Après avoir exécuté le script, vous devriez avoir :

* numlock Off,
* capslock On,
* scrolllock On.

Voici un exemple d'utilisation :

<div lang="en-US">
```
42sh# ./rev_kdb_leds.sh input20
```
</div>

`input20` correspond à l'identifiant de votre clavier, sous
`/sys/class/input/`.

---

##### `suspend_schedule.sh` {-}

Votre script prendra en argument l'heure à laquelle votre machine doit être
réveillée, avant de la mettre effectivement en veille.

Bien sûr, vous ne devez utiliser que des écritures (et des lectures) dans le
système de fichiers `/sys`. Il n'est pas question de faire appel à un autre
programme (vous pourriez cependant avoir besoin de `date(1)` pour faire les
calculs horaires).

Voici un exemple d'utilisation :

<div lang="en-US">
```
42sh# ./suspend_schedule.sh 15:42

```
</div>

Vous aurez besoin de définir une alarme au niveau de votre RTC, via le
fichier :\
`/sys/class/rtc/rtcX/wakealarm`\

::::: {.warning}
Attention au fuseau horaire utilisé par votre RTC, si votre système principal
est Windows, elle utilisera sans doute le fuseau horaire courant. Sinon, ce
sera UTC.
:::::

Un article très complet sur le sujet est disponible ici :\
<https://www.linux.com/tutorials/wake-linux-rtc-alarm-clock/>

:::::
