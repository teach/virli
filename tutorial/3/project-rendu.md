\newpage

Rendu
=====

Est attendu d'ici le TP suivant :

- le rendu des exercice de ce TP ;
- vos réponses à [l'évaluation du cours](https://virli.nemunai.re/quiz/14).

Pour les GISTRE (et en bonus pour les SRS), [un
projet](https://virli.nemunai.re/project-2.pdf) est à rendre pour le 13
novembre. Consultez les modalités de rendu sur le sujet directement.


## Modalité de rendu

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Afin d'orienter correctement votre rendu, ajoutez une balise `[TP3]` au sujet
de votre courriel. N'hésitez pas à indiquer dans le corps du message votre
ressenti et vos difficultés ou bien alors écrivez votre meilleure histoire
drôle si vous n'avez rien à dire.


## Tarball

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (adaptez les extensions et les éventuels
fichiers supplémentaires associés au langage que vous aurez choisi
pour chaque exercice) :

<div lang="en-US">
```
login_x-TP3/
login_x-TP3/escape.c              # SRS
login_x-TP3/procinfo.sh
login_x-TP3/suspend_schedule.sh
login_x-TP3/view_caps.c
login_x-TP3/monitor.sh
login_x-TP3/telegraf.sh           # SRS
login_x-TP3/telegraf_init.sh      # SRS
login_x-TP3/syscall_filter.c
```
</div>
