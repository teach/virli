Prérequis
---------

Pour pouvoir suivre les exemples et faire les exercices qui suivent, vous aurez
besoin d'un noyau Linux récent (une version 5.x sera très bien). Il doit de
plus être compilé avec les options suivantes (lorsqu'elles sont disponibles
pour votre version) :

<div lang="en-US">
```
General setup  --->
    [*] Control Group support  --->
      [*]   Freezer cgroup subsystem
      [*]   PIDs cgroup subsystem
      [*]   Device controller for cgroups
      [*]   Cpuset support
      [*]   Simple CPU accounting cgroup subsystem
      [*]   Memory Resource Controller for Control Groups
      [*]   Group CPU scheduler  --->
        [*]   Group scheduling for SCHED_OTHER
        [*]   Group scheduling for SCHED_RR/FIFO
      <*>   Block IO controller
[*] Networking support  --->
    Networking options  --->
      [*] Network priority cgroup
      [*] Network classid cgroup
```
</div>

Si vous utilisez un noyau standard fourni par votre distribution, les options
requises seront a priori déjà sélectionnées et vous n'aurez donc pas à compiler
votre propre noyau. Néanmoins, nous allons nous interfacer avec le noyau, il
est donc nécessaire d'avoir les en-têtes de votre noyau.

Sous Debian, vous pouvez les installer via le paquet au nom semblable à
`linux-headers`. Le paquet porte le même nom sous Arch Linux et ses dérivés.


### Vérification via `menuconfig`

L'arbre ci-dessus correspond aux options qui seront *built-in* (signalées par
une `*`) ou installées en tant que module (signalées par un `M`). En effet,
chaque noyau Linux peut être entièrement personnalisé en fonction des options
et des pilotes que l'on voudra utiliser.

Pour parcourir l'arbre des options du noyau, il est nécessaire d'avoir les
sources de celui-ci. Les dernières versions stables et encore maintenues sont
disponibles sur la page d'accueil de <https://kernel.org>.

Dans les sources, on affiche la liste des options avec la commande :

<div lang="en-US">
```bash
make menuconfig
```
</div>


### Vérification via `/boot/config-xxx`

Les distributions basées sur Debian ont pour habitude de placer le fichier de
configuration ayant servi à compiler le noyau et ses modules dans le dossier
`/boot`, aux côtés de l'image du noyau `vmlinuz-xxx`, de l'éventuel système de
fichiers initial (`initramfs-xxx`) et des symboles de débogage
`System.map-xxx`.

Ce fichier répertorie toutes les options qui ont été activées. Par rapport à
l'arbre présenté ci-dessus, vous devriez trouver :

<div lang="en-US">
```
CONFIG_CGROUPS=y
CONFIG_CGROUP_FREEZER=y
CONFIG_CGROUP_PIDS=y
CONFIG_CGROUP_DEVICE=y
CONFIG_CPUSETS=y
CONFIG_CGROUP_CPUACCT=y
CONFIG_CGROUP_MEMCG=y
CONFIG_CGROUP_SCHED=y
CONFIG_BLK_CGROUP=y

CONFIG_NET=y
CONFIG_CGROUP_NET_PRIO=y
CONFIG_CGROUP_NET_CLASSID=y
```
</div>


### Vérification via `/proc/config.gz`

Dans la plupart des autres distributions, la configuration est accessible à
travers le fichier `/proc/config.gz`. Comme vous ne pouvez pas écrire dans
`/proc` pour décompresser le fichier, utilisez les outils `zcat`, `zgrep`, ...

Vous devez retrouver les mêmes options que celles de la section précédente.
