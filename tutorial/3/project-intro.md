\newpage

Projet et rendu
===============

## Sujet

Vous allez commencer aujourd'hui un projet qui s'étendra au prochain TP et qui
consistera à réaliser la partie d'isolation de la moulinette des ACUs !

Cette semaine, il faudra faire en sorte de restreindre un groupe de processus
pour qu'il ne puisse pas faire de déni de service sur notre machine.

Il n'y a pas de restriction sur le langage utilisé, vous pouvez tout aussi bien
utiliser du C, du C++, du Python, etc.

L'usage de bibliothèques **non relatives** au projet est autorisé : le but de
ce sujet est d'évaluer votre compréhension et votre utilisation de la
tuyauterie bas-niveau du noyau liée à la virtualisation légère. À partir du
moment où vous n'utilisez pas une bibliothèque qui abstrait complètement cette
plomberie, n'hésitez pas à l'utiliser !

Gardez en tête que ce projet sera à continuer au prochain TP, où il sera
principalement question de faire des appels système.
