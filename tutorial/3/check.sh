#!/bin/sh

note_init() {
    NOTE=0
    echo -n $@
}

note() {
    NOTE=$(($NOTE + $1))
    echo -n ,$@
}

for LOGIN in $@
do
    note_init $LOGIN

    if [ -f "$LOGIN" ]; then
	QUESTIONSF="$LOGIN"
    else
	QUESTIONSF="$LOGIN/questions.txt"
    fi

    # Questions

    if grep -E -i "(grsec)" "${QUESTIONSF}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi


    if grep -E -i "(/proc/.*/oom_|score)" "${QUESTIONSF}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi

    if grep -E -i "root" "${QUESTIONSF}" 2> /dev/null > /dev/null; then
	note 1
    else
	note 0
    fi


    echo #" = $NOTE"
done
