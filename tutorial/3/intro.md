\newpage

Statistiques et contrôle des processus
======================================

Maintenant que nous avons pu voir en détail comment utiliser Docker, nous
allons tâcher d'appréhender les techniques qu'il met en œuvre. De nombreuses
fonctionnalités du noyau Linux sont en effet sollicitées : certaines sont là
depuis longtemps, quelques unes sont standardisées, d'autres sont plus récentes.

Dans un premier temps, nous allons aborder la manière dont le noyau Linux
permet de contrôler les processus : que ce soit en collectant des informations
sur eux ou en imposant certaines restrictions.
