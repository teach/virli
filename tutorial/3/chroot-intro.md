\newpage

En route vers la contenerisation
================================

Nous avons vu un certain nombre de fonctionnalités offertes par le noyau Linux
pour limiter, autoriser ou contraindre différents usages des ressources de
notre machine.

Il est temps maintenant de commencer à parler d'isolation, mais ... cela fait
déjà beaucoup à digérer pour aujourd'hui, alors on va se contenter de parler
d'un mécanisme que vous connaissez sans doute déjà.
