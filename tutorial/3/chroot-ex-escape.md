::::: {.exercice}

## Exercice (SRS seulement)  {-}

Écrivons maintenant un programme dont le seul but est de s'échapper du `chroot` :

<div lang="en-US">
```bash
make escape
echo bar > ../foo
chroot .
```
</div>

Dans le nouvel environnement, vous ne devriez pas pouvoir faire :

<div lang="en-US">
```bash
cat ../foo
```
</div>

Mais une fois votre programme `escape` exécuté, vous devriez pouvoir !

<div lang="en-US">
```
(chroot) 42sh# ./escape
  bash# cat /path/to/foo
```
</div>

:::::
