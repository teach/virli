\newpage

Rendu
=====

Est attendu d'ici le cours suivant :

- vos réponses à l'évaluation du cours,
- [SRS] tous les exercices de ce TP,
- [GISTRE] les premiers paliers du [projet final](https://virli.nemunai.re/project-gistre.pdf).


Arborescence attendue (SRS)
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans un dépôt
Git privé, que vous partagerez avec [votre
professeur](https://gitlab.cri.epita.fr/nemunaire/).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires) :

<div lang="en-US">
```
./pseudofs/procinfo
./pseudofs/cpuinfo.sh // batinfo.sh
./pseudofs/suspend_schedule.sh // rev_kdb_leds.sh
./cgroups/monitor
./cgroups/telegraf
./cgroups/telegraf_init
./caps/view_caps.c
./seccomp/syscall_filter.c
./docker/registry_play
./chroot/escape.c
```
</div>

Votre rendu sera pris en compte en faisant un [tag **signé par votre clef
PGP**](https://lessons.nemunai.re/keys). Consultez les détails du rendu (nom du
tag, ...) sur la page dédiée au projet sur la plateforme de rendu.

::::: {.question}

Si vous utilisez un seul dépôt pour tous vos rendus, vous **DEVRIEZ**
créer une branche distincte pour chaque rendu :

<div lang="en-US">
```
42sh$ git checkout --orphan renduX
42sh$ git reset
42sh$ rm -r *
42sh$   # Créer l'arborescence de rendu ici
```
</div>

Pour retrouver ensuite vos rendus des travaux précédents :

<div lang="en-US">
```
42sh$ git checkout renduY
-- ou --
42sh$ git checkout master
...
```
</div>

Chaque branche est complètement indépendante l'une de l'autre. Vous
pouvez avoir les exercices du TP1 sur `master`, les exercices du TP3
sur `rendu3`, ... ce qui vous permet d'avoir une arborescence
correspondant à ce qui est demandé, sans pour autant perdre votre
travail (ou le rendre plus difficile d'accès).

::::
