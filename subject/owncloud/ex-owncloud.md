Projet
======

Pour mettre en pratiques toutes les notions que l'on a vu jusque là, écrivez un
playbook Ansible pour automatiser le lancement de votre instance personnelle de
[`nextcloud`](https://hub.docker.com/_/nextcloud/) ou
d'[`owncloud`](https://hub.docker.com/r/owncloud/server/). Une attention
particulière devra être apportée à la manière dont vous faites persister les
données entre deux lancements, tout en prenant en compte les futures mises à
jour.

Le service doit être accessible sur votre réseau local, sur un port
configurable.

Sur la partie Docker, votre playbook devra se limiter aux modules
[`docker-container`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_container_module.html),
[`docker-network`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_network_module.html),
[`docker-volume`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_volume_module.html)
(ie. sans utiliser `docker-compose` ou `docker stack`).

Cette instance devra utiliser une base de données MySQL/MariaDB ou Postgres
(lancée par vos soins dans un autre conteneur) et contenir ses données dans un
ou plusieurs volumes (afin qu'elles persistent notamment à une mise à jour des
conteneurs).

L'exécution doit être la plus sécurisée possible (pas de port MySQL exposé sur
l'hôte par exemple, etc.) et la plus respectueuse des bonnes pratiques que l'on
a pu voir durant le cours.


## Exemple d'exécution

<div lang="en-US">
```bash
42sh$ ansible-playbook -i hosts my_cloud.yml
```
</div>
