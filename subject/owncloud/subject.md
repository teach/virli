---
title: Virtualisation légère -- Projet n^o^ 1
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: EPITA -- Promo 2022
abstract: |
  Le rendu de ce projet est attendu par courrier électronique signé à
  <virli@nemunai.re> au plus tard le **mercredi 13 octobre 2021 à 23 h
  42**.

  Ce projet est **obligatoire pour tous les SRS**, et optionnel pour les GISTRE.
...
