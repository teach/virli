Rendu
=====

Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Afin d'orienter correctement votre rendu, ajoutez une balise `[PROJET1]` au sujet
de votre courriel. N'hésitez pas à indiquer dans le corps du message votre
ressenti et vos difficultés ou bien alors écrivez votre meilleure histoire
drôle si vous n'avez rien à dire.


Tarball
-------

Tous les fichiers identifiés comme étant à rendre pour ce TP sont à
placer dans une tarball (pas d'archive ZIP, RAR, ...).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
login_x-project1/
login_x-project1/my_cloud.yml
login_x-project1/roles/...
```
</div>


## Signature du rendu

Deux méthodes sont utilisables pour signer votre rendu :

* signature du courriel ;
* signature de la tarball.

Dans les deux cas, si vous n'en avez pas déjà une, vous devrez créer une clef
PGP à **votre nom et prénom**.

Pour valider la signature, il est nécessaire d'avoir reçu la clef publique
**séparément**. Vous avez le choix de l'uploader sur un serveur de clefs, soit
de me fournir votre clef en main propre, soit l'envoyer dans un courriel
distinct.
