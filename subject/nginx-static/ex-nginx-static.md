Projet
======

Avec l'aide d'un `Dockerfile` *multi-stage*, réalisez l'image la plus petite
possible (partant d'un `FROM scratch`{.dockerfile}), qui permette d'utiliser la
[page de compte à rebours](https://virli.nemunai.re/countdown.html) avec cette
configuration pour nginx :

<div lang="en-US">
```conf
events {}

http {
	default_type text/html;

	index countdown.html;

    server {
        listen 8080;

    	root /srv/http;

    	rewrite "^/[0-9]+:[0-9]{2}$" /countdown.html;
    	rewrite "^/[0-9]+$" /countdown.html;
    }
}
```
</div>

Vous pouvez envisager dans un premier temps d'extraire de l'image `nginx`, le
binaire `nginx` lui-même et observer les différents problèmes. Vous pourrez
ensuite par exemple envisager de compiler `nginx` (vous trouverez les sources
du projet : <http://nginx.org/download>).

Dans tous les cas, votre `Dockerfile` devra être facilement maintenable
(notamment en cas de nouvelle version du serveur web), et vous devrez apporter
une attention particulière au suivi des bonnes pratiques d'écriture des
`Dockerfile`.

## Exemple d'exécution

<div lang="en-US">
```
42sh$ docker image build -t countdown countdown
42sh$ docker container run -d -P countdown
42sh$ firefox http://localhost:32198/42:23
```
</div>
