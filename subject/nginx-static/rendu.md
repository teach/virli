Rendu
=====

Modalités de rendu
------------------

En tant que personnes sensibilisées à la sécurité des échanges électroniques,
vous devrez m'envoyer vos rendus signés avec votre clef PGP.

Un service automatique s'occupe de réceptionner vos rendus, de faire des
vérifications élémentaires et de vous envoyer un accusé de réception (ou de
rejet).

Ce service écoute sur l'adresse <virli@nemunai.re>, c'est donc à cette adresse
et exclusivement à celle-ci que vous devez envoyer vos rendus. Tout rendu
envoyé à une autre adresse et/ou non signé et/ou reçu après la correction ne
sera pas pris en compte.

Par ailleurs, n'oubliez pas de répondre à
[l'évaluation du cours](https://www.epitaf.fr/moodle/mod/quiz/view.php?id=307).


Tarball
-------

Tous les fichiers identifiés comme étant à rendre sont à placer dans une
tarball (pas d'archive ZIP, RAR,\ ...).

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
 login_x-project_2/
 login_x-project_2/Dockerfile
(login_x-project_2/nginx.conf)
(login_x-project_2/countdown.html)
```
</div>

Les deux fichiers `nginx.conf` et `countdown.html` seront écrasés par
les fichiers fournis lors de la correction, vous n'êtes pas donc
obligés de les embarquer dans votre rendu.
