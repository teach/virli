---
title: Virtualisation légère -- Projet n^o^ 2
author: Pierre-Olivier *nemunaire* Mercier
institute: EPITA
date: EPITA -- SRS 2021
abstract: |
    Ce project complémentaire a pour but de vous montrer la manière dont
    Docker récupère les images et lance ces conteneurs.
...
