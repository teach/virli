## Palier 6 à 10 : *To be continued* {-}

Nous verrons ensemble de meilleures techniques d'isolation au prochain cours.

Le sujet sera mis à jour en conséquence.


\newpage

Rendu
=====

Le rendu du projet final avec tous les paliers est attendu pour le samedi 3
décembre 2022 à 23:42.

Il n'est pas attendu de rendu intermédiaire pour cette première partie seule.


Tarball
-------

Voici une arborescence type (vous pourriez avoir des fichiers supplémentaires,
cela dépendra de votre avancée dans le projet) :

<div lang="en-US">
```
./
./README
./Makefile
./src/...
```
</div>

\newpage
