Exemples
========

## Arguments de la ligne de commande

<div lang="en-US">
```
42sh$ ./mymoulette -h
MyMoulette, the students' nightmare, now highly secured
Usage: ./mymoulette [-v student_workdir] <-I docker-img|rootfs-path> moulette_prog [moulette_arg [...]]
    rootfs-path is the path to the directory containing the new rootfs (exclusive with -I option)
    docker-img is an image available on hub.docker.com (exclusive with rootfs-path)
	moulette_prog will be the first program to be launched, must already be in the environment
	student_workdir is the directory containing the code to grade
```
</div>


## Réseau accessible

<div lang="en-US">
```
42sh$ ip address show veth0l
2: veth0l@if82: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP mode DEFAULT group default
    link/ether 42:a2:a0:89:54:ef brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.41/24 brd 10.10.10.255 scope global veth3e06cad
       valid_lft forever preferred_lft forever

42sh$ ./mymoulette ./newrootfs/ /usr/bin/ip address show veth0r
2: veth0r@if82: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP mode DEFAULT group default
    link/ether 42:a2:a0:89:55:ef brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.42/24 brd 10.10.10.255 scope global veth3e06cad
       valid_lft forever preferred_lft forever

42sh$ ping -c 1 10.10.10.42
PING 10.10.10.42 (10.10.10.42) 56(84) bytes of data.
64 bytes from 10.10.10.42: icmp_seq=1 ttl=56 time=3.90 ms

--- 10.10.10.42 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 3.789/3.847/3.906/0.085 ms
```
</div>

<div lang="en-US">
```
42sh$ ./mymoulette ./newrootfs/ /usr/bin/curl http://www.linuxcontainers.org/ | md5sum
79c12c02c8a08b4ec3fe219f97df8e78
```
</div>


## Capabilities limitées

<div lang="en-US">
```
42sh$ ./mymoulette ./newrootfs/ /bin/ping 9.9.9.9
ping: icmp open socket: Permission denied
```
</div>

<div lang="en-US">
```
42sh$ ./mymoulette ./newrootfs/ /usr/local/bin/view_caps
cap_user_header_t
-----------------
Version: 20080522
PID: 0

cap_user_data_t
---------------
effective: 0x0
permitted: 0x0
inheritable: 0x0

ambient:

bounding:
```
</div>
