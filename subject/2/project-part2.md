## Palier 6 : Une vraie isolation (2 points)  {-}

En plus du `chroot`, assignez de nouveaux *namespaces* au processus que vous
allez lancer : `cgroups`, `IPC`, `mount`, `net`, `PID`, `UTS`.

Il est requis que le nouveau processus ne puisse pas s'échapper de ses
*namespaces* !

**Astuce :** `unshare(2)`.


## Palier bonus : Révolution des utilisateurs (2 points)  {-}

Utilisez le *namespace* `user` pour vous permettre de ne plus avoir besoin de
lancer votre moulinette en tant que `root` ou avec plus de *capabilities* que
nécessaire dans vos attributs étendus.

Vous aurez besoin de déplacer le code écrit pour les premiers paliers (cgroups,
caps, ...), après vous être dissocié de cet espace de noms.


## Palier 7 : Empêcher les fuites d'information (2 points)  {-}

Démontez tous les systèmes de fichiers qui ne sont pas nécessaires au
fonctionnement de votre conteneur et remontez les partitions

N'oubliez pas de remonter les systèmes de fichiers pour lesquels cette
opération est nécessaire afin de terminer l'étape d'isolation.

**Astuce :** `mount(2)`.


## Palier 7 bis : Volume attaché au code à moulinéter (1 point)  {-}

Ajoutez une option à votre moulinette afin de lier le dossier (qui se trouve
sur la machine hôte) contenant le code d'un étudiant, à l'intérieur du
conteneur. Cette option est similaire à l'option `--volume`/`-v` de Docker :

L'option devra attendre le répertoire contenant le code de l'étudiant, et le
monter dans `/home/student` :

<div lang="en-US">
```
42sh$ ./mymoulette ./newrootfs/ /bin/bash
    bash# ls /home/student/
    bash# ls /var/lib/git/login_x/42sh/
    ls: cannot access '/var/lib/git/login_x/42sh/': No such file or directory

42sh$ ls /var/lib/git/login_x/42sh/
src/ tests/ AUTHORS configure Makefile.am README TODO

42sh$ ./mymoulette -v /var/lib/git/login_x/42sh/ ./newrootfs/ /bin/bash
    bash# ls /home/student/
    src/ tests/ AUTHORS configure Makefile.am README TODO
    bash#
```
</div>

**Astuce :** `mount(2)`, `mkdir(2)`.


## Palier 8 : Identification du conteneur (1 point)  {-}

Maintenant que vous avez votre conteneur, personalisez-le un peu en lui donnant
un nom unique.

**Astuce :** `sethostname(2)`

<div lang="en-US">
```
42sh$ ./mymoulette ./newrootfs/ hostname
ush9ukohh3Ch

42sh$ ./mymoulette ./newrootfs/ hostname
shie6aif2aiH
```
</div>


## Palier 9 : `pivot_root` (2 points)  {-}

Effectuez un `pivot_root(2)` de telle sorte qu'il ne reste plus de trace du
système de fichiers hôte.

**Astuce :** `pivot_root(2)`, `umount(2)`.

**Attention :** vos filtres `seccomp` filtrant l'appel système
`pivot_root(2)`, vous devrez donc ne les appliquer qu'après cette étape.


## Palier 10 : Bac à sable connecté (3 points)  {-}

Partant d'une liste d'interfaces sur la machine hôte similaire à :

<div lang="en-US">
```
42sh$ ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
    link/ether 90:2b:34:5e:fa:a7 brd ff:ff:ff:ff:ff:ff
```
</div>

Vous devrez pouvoir `ping`er votre conteneur depuis votre hôte :

<div lang="en-US">
```
42sh$ ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
[...]
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
[...]
3: veth3e06cad@if82: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP   mode DEFAULT group default
    link/ether 42:a2:a0:89:54:ef brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.41/24 brd 10.10.10.255 scope global veth3e06cad
       valid_lft forever preferred_lft forever
42sh$ ping 10.10.10.42
PING 10.10.10.42 (10.10.10.42) 56(84) bytes of data.
64 bytes from 10.10.10.42: icmp_seq=1 ttl=56 time=3.90 ms
64 bytes from 10.10.10.42: icmp_seq=2 ttl=56 time=3.78 ms
^C
--- 10.10.10.42 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 3.789/3.847/3.906/0.085 ms
```
</div>

Dans l'exemple ci-dessus, l'interface dans le conteneur a l'IP `10.10.10.42`,
tandis que la machine hôte a l'IP `10.10.10.41`.

**Astuces :** vous pouvez utiliser la `libnetlink(3)` ou même faire des appels
aux programmes `ip(8)`, `brctl(8)`, ...
