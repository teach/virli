---
title: Virtualisation légère -- Projet n^o^ 3
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: EPITA -- Promo 2023
abstract: |
  Le laboratoire des assistants a besoin de votre expertise afin de
  renforcer la sécurité et la réactivité de son système de correction
  automatisé.

  Votre mission consistera à réaliser la partie d'isolation de la
  moulinette des ACUs : dans un premier temps vous ferez en sorte de
  restreindre un groupe de processus pour qu'il ne puisse pas faire de
  déni de service sur notre machine ; puis dans une seconde partie,
  vous vous assurerez de l'isolation de chaque étudiant.

  \vspace{1em}

  Il n'y a pas de restriction sur le langage utilisé, vous pouvez tout
  aussi bien utiliser du C, du C++, du Python, etc.

  L'usage de bibliothèques **non relatives** au projet est autorisé :
  le but de ce sujet est d'évaluer votre compréhension et votre
  utilisation de la tuyauterie bas-niveau du noyau liée à la
  virtualisation légère. À partir du moment où vous n'utilisez pas une
  bibliothèque qui abstrait complètement cette plomberie, n'hésitez
  pas à l'utiliser pour gagner du temps !

  \vspace{1em}

  Ce projet (qui sera complété au prochain cours) sera à rendre au
  plus tard le samedi 3 décembre 2022 à 23 h 42.

  Ce projet est **obligatoire pour les GISTRE**, et optionnel pour les SRS.
...

\newpage

Paliers
=======
