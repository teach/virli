## Palier 5 : Automatisation de la création de l'environnement (1 point)  {-}

Intégrer le script `registry_play` à votre programme afin de pouvoir créer un
environnement neuf à chaque nouvelle exécution.

Vous pouvez l'utiliser tel quel, ou le réécrire pour qu'il s'intègre plus
facilement au sein du langage que vous avez choisi.

L'appel du programme se fera alors ainsi, avec l'otion `-I <docker image>` pour
préciser que l'on s'attend à trouver le nom d'une image Docker :

<div lang="en-US">
```
42sh# ./mymoulette -I library/alpine:latest /bin/ash
    ash# which apk
    /sbin/apk
```
</div>

Vous pourriez utiliser `mkdtemp(3)` pour déterminer l'emplacement de
décompression des tarballs d'image.
