---
title: Virtualisation légère -- Projet n^o^ 1
author: Pierre-Olivier *nemunaire* Mercier
institute: EPITA
date: EPITA -- SRS 2018
abstract: |
  M. Dessi, votre DSI, vient vous voir, paniqué : une grande
  conférence se tient dans deux semaines, l'entreprise avait été
  mandatée de longue date pour réaliser une interface d'animation pour
  un des événements majeurs et cela fait un mois maintenant qu'il est
  sans nouvelle du sous-traitant à qui cette tâche avait été
  confiée. On ne peut pas attendre davantage, vous êtes dépêché pour
  terminer le projet qui a été laissé en plan par le sous-traitant.

  \vspace{1em}

  Ce projet est à rendre à <virli@nemunai.re> au plus tard le jeudi 9
  novembre 2017 à 8 h 42.
...

/newpage

# Paliers
