Heureusement pour vous, il apparaît que tout le code de l'interface a été
envoyé sur un dépôt git de l'entreprise. Il semblerait qu'il ne reste plus qu'à
déployer la solution.

Voici les notes retrouvées dans les derniers échanges avec le sous-traitant :

<div lang="en-US">
> Pour avoir quelque chose de fonctionnel :
>
>     # Un environnement de travail Go >= 1.9 est nécessaire.
>
>     # Récupération du projet, des dépendances et build
>     git clone git://git.nemunai.re/fic/server.git $GOPATH/src/srs.epita.fr/fic-server
>
>     go get -d srs.epita.fr/fic-server/admin
>     go build -o $GOPATH/src/srs.epita.fr/fic-server/fic-admin srs.epita.fr/fic-server/admin
>
>     go get -d srs.epita.fr/fic-server/backend
>     go build -o $GOPATH/src/srs.epita.fr/fic-server/fic-backend srs.epita.fr/fic-server/backend
>
>     go get -d srs.epita.fr/fic-server/frontend
>     go build -o $GOPATH/src/srs.epita.fr/fic-server/fic-frontend srs.epita.fr/fic-server/frontend
>
>     # Configuration de la base de données : par défaut tous les composants vont se connecter à
>     # fic:fic@localhost/fic, voir l'option -dsn pour d'autres paramètres. Les variables
>     # d'environnement MYSQL_HOST, MYSQL_PASSWORD, MYSQL_USER et MYSQL_DATABASE permettent
>     # également de construire le DSN.
>     mysql -u root -p <<EOF
>     CREATE DATABASE fic;
>     CREATE USER fic@localhost IDENTIFIED BY 'fic';
>     GRANT ALL ON fic.* TO fic@localhost;
>     EOF
>
>     cd $GOPATH/src/srs.epita.fr/fic-server/
>     mkdir SETTINGS
>     # Ce dossier va contenir les paramètres du challenge
>     ./fic-admin &
>     # À ce stade http://localhost:8081/ permet d'accéder à l'interface d'admin
>
>     mkdir TEAMS
>     # Ce dossier va contenir les fichiers statiques générés (themes.json, teams.json, my.json, ...)
>     # pour chaque équipe
>     ./fic-backend &
>     # Le backend génére les fichiers pour toutes les équipes puis attend des validations dans
>     # le dossier submissions.
>     # Il n'écoute que les modifications du système de fichiers, il n'a pas d'interface HTTP.
>
>     ./fic-frontend &
>     # Il s'agit d'une API qui va donner l'heure (/time.json) et qui va recevoir les validations
>     # pour les écrire dans des fichiers. Avec le moins d'intelligence possible pour éviter des
>     # vulnérabilités.
>     # Un nginx est nécessaire au dessus pour gérer l'authentification.
>
> Un exemple de conf nginx est disponible dans la branche `f/ansible` :
>
>   - la version utilisant un simple `htpasswd` :
>     <https://git.nemunai.re/?p=fic/server.git;a=blob;hb=refs/heads/f/ansible;f=playbooks/roles/fic-frontend/files/nginx-frontend-htpasswd.conf>
>
>   - la version utilisant PAM (c'est ce qu'on utilise dans
>     l'environnement de préproduction, on se base sur le LDAP de la boîte)
>     <https://git.nemunai.re/?p=fic/server.git;a=blob;f=playbooks/roles/fic-frontend/files/nginx-frontend-pam.conf;hb=refs/heads/f/ansible>
>
>   - la version utilisant les certificats clients : `configs/nginx-prod.conf`
>
> Il faut ajouter un binding certificat/remote_user -> team, dans le
> fichier `/etc/nginx/auth.conf`.
>
>   - pour les certificats : <http://localhost:8081/api/teams-nginx> ;
>   - pour l'auth HTTP : <http://localhost:8081/api/teams-nginx-members>.
>
> En production, le frontend est déporté sur une autre machine et un `rsync`
> s'occupe de synchroniser les dossiers `submissions` et `TEAMS` des deux
> machines. Ainsi, la machine validant les solutions et contenant toutes
> les données n'est jamais accessible du réseau local, c'est elle qui
> initie les `rsync` de manière régulière (`configs/synchro.sh`)
>
> Un mécanisme de synchronisation permet d'importer le challenge à partir d'un
> WebDAV : il faut pour cela utiliser les options `-clouddav`, `-clouduser` et
> `-cloudpass`. Par exemple :
>
>     [ -z "${FICCLOUD_PASS}" ] && read -p 'FICCLOUD_PASS: ' -s FICCLOUD_PASS; \
>     ./fic-admin -clouddav "https://owncloud.srs.epita.fr/remote.php/webdav/FIC 2018" \
>       -clouduser login_x -cloudpass "${FICCLOUD_PASS}"
>
> Il est également possible, après avoir monté le webDAV avec FUSE ou en ayant
> téléchargé son contenu dans un répertoire, d'utiliser un répertoire local,
> via l'option `-localimport` :
>
>     ./fic-admin -localimport /mnt/fic/ -localimportsymlink
</div>

![Vue d'ensemble des échanges de données entre les services](FIC-overview.png)


### Palier 0 : Récupérer les images  {-}

Le sous-traitant a laissé des images Docker sur le Docker Store, vous pourrez
vous baser dessus pour commencer.

* `nemunaire/fic-admin`
* `nemunaire/fic-backend`
* `nemunaire/fic-frontend`

Faites-vous la main sur ces trois images : lancez les ; l'ordre suggéré par les
notes du prestataire devraient vous aider.

Pensez à inspecter les conteneurs pour voir les objets Docker qu'ils créent
et/ou exposent.

Vous devriez pouvoir lancer `docker container run nemunaire/fic-IMAGE --help`
sur les images.

Durant la durée du projet, les images seront peut-être amenées à être mises à
jour, si vous vous trouvez bloqué, commencez par vérifier que vous avez bien la
dernière version disponible de l'image :

<div lang="en-US">
```bash
docker pull nemunaire/fic-admin nemunaire/fic-backend nemunaire/fic-frontend
```
</div>


### Palier 1 : `docker-compose.yml`  {-}

Maintenant que vous arrivez à lancer les images, rendez cela reproductible en
inscrivant tout ça dans un fichier YAML, compréhensible par `docker-compose` !

Vous devriez avoir ces services :

* `mariadb` (ou `mysql`),
* `admin`,
* `backend`,
* `frontend`,
* `nginx` (ou `apache`, ...).


### Palier 2 : retrouver les `Dockerfile`  {-}

Maintenant que vous êtes en mesure de lancer le service, il serait temps de ne
plus dépendre d'une image que l'on ne peut plus modifier facilement.

Pour ce palier, vous allez devoir réécrire les trois fichiers `Dockerfile`,
pour chacun des service :

* `admin`,
* `backend`,
* `frontend`.

Arriverez-vous à générer des images plus propres que celles du prestataire
disparu ?!


#### Astuces  {-}

Les différents projets sont organisés au sein d'un
[dépôt monolithique](https://danluu.com/monorepo/). Les projets ont des
dépendances entre les dossiers qui se trouvent à la racine (qui sont
l'équivalent de bibliothèques). Vous allez sans doute vouloir placer les trois
`Dockerfile` à la racine pour simplifier les étapes de construction des images :

<div lang="en-US">
```shell
docker image build --file Dockerfile-admin .
```
</div>

Les
[*multi-stage builds*](https://docs.docker.com/engine/userguide/eng-image/multistage-build/)
vous seront d'une grande aide.

Si vous n'avez pas la possibilité d'utiliser une version récente de Docker qui
supporte cette syntaxe, vous pouvez ajouter des scripts et autant de
`Dockerfile` que nécessaire à votre rendu (mais vous devriez considérer
l'option de mettre à jour votre Docker !).


### Palier 3 : `fic-server.yml` prêt pour le déploiement  {-}

À présent, faites les éventuelles adaptations nécessaires pour que votre
fichier `docker-compose.yml` puisse s'exécuter au sein d'un cluster de
plusieurs machines. Via `docker stack deploy`.


### Palier 4 : `fic-server.yml` prêt pour la production  {-}

Comme indiqué dans les notes du prestataire, en production, le frontend se
trouve sur une machine distincte du backend.

À vous de trouvez une solution élégante pour gérer la synchronisation des
données ! Il est fort probable que vous ayez à ajouter des services à votre
fichier YAML.


### Palier 5 (bonus) : `fic-server.yml` sécurisé  {-}

Vous avez indiqués des mots de passes bidons dans votre YAML ? Rangez les
informations sensibles au sein de
[`docker secret`](https://docs.docker.com/engine/swarm/secrets/) et les
éléments de configuration qui pourraient être changés au sein de
[`docker config`](https://docs.docker.com/engine/swarm/configs/).
