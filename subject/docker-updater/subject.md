---
title: Virtualisation légère -- Mini-projet n^o^ 2
author: Pierre-Olivier *nemunaire* [Mercier]{.smallcaps}
institute: EPITA
date: EPITA -- Promo 2023
abstract: |
  Le rendu de ce mini-projet individuel est attendu par un tag signé
  sur un dépôt privé de la forge de l'école au plus tard le **mardi
  18 octobre 2022 à 23 h 42**.
...
