::::: {.exercice}

Écrivez un programme tirant parti de l'API du Docker Engine (le *daemon*
Docker) qui pour un conteneur donné :

1. détecte si le conteneur exécute une image disposant d'une mise à jour ;
1. cherche à récupérer la dernière image disponible ;
1. met à jour le conteneur.

Enfin, pour simplifier son utilisation régulière, vous conceverez une image de
conteneur minimaliste.

## Étape 1 : Lister les conteneurs

Lancé sans argument, votre programme doit retourner la liste des conteneurs
actifs. Un nom par ligne.

### Exemple d'exécution {-}

<div lang="en-US">
```bash
42sh$ docker run -d nginx
42sh$ docker run -d mysql
42sh$ docker run -d redis
42sh$ ctr-updater
Choose a container to analyze:
dazzling_driscoll
amazing_proskuriakova
optimistic_meninsky
```
</div>


## Étape 1 bis : Prêt pour la prod

Écrivez un `Dockerfile` pour conteneuriser ce programme : gérer tant la
construction (s'il y a des étapes de construction) que l'exécution. En
utilisant les bonnes pratiques que nous avons pu voir.

### Exemple d'exécution {-}

<div lang="en-US">
```bash
42sh$ docker run -d --name myp0m nemunaire/youp0m
42sh$ docker build -t login/ctr-updater .
42sh$ docker run --rm -v /var/run/docker.sock:/var/run/docker.sock login/ctr-updater
Choose a container to analyze:
myp0m

# Après l'étape 2 :
42sh$ docker run --rm -v /var/run/docker.sock:/var/run/docker.sock login/ctr-updater myp0m
42sh$ echo $?
0                 # Le conteneur est à jour
```
</div>

Notez que l'on n'emploie pas `ctr-updater` pour appeler le binaire dans
l'image. Le premier argument passé au conteneur est donc le nom du conteneur à
vérifier.

::::: {.question}

#### Afficher ou ne pas afficher le conteneur courant dans la liste ? {-}

On remarque que dans l'exemple, on lance un conteneur `youp0m` puis un
conteneur `login/ctr-updater`, mais pourtant `ctr-updater` n'affiche qu'un seul
conteneur.

Étant donné que l'on ne pourra pas mettre à jour le conteneur courant
automatiquement, puisque c'est lui qui stope puis relance les conteneurs, on ne
le propose pas dans la liste.

Lors de l'affichage de la liste des conteneurs, sautez le conteneur dont le
début de l'identifiant commence par le nom d'hôte du conteneur courant
(`hostname(1)`).

Le comportement est indéfini si on passe le nom de notre propre conteneur en
argument, pour les étapes suivantes.

:::::


## Étape 2 : Détecter si le conteneur exécute la dernière image disponible

Lancé avec un argument, votre programme doit, à partir des informations
accessibles localement (conteneurs et images), retourner le code de 0 ou 1,
suivant si (0) aucune mise à jour de l'image n'est disponible, respectivement
(1) une mise à jour est disponible.

### Exemple d'exécution {-}

<div lang="en-US">
```bash
42sh$ docker build -t my_webserver .
42sh$ docker run -d --name mws my_webserver
42sh$ ctr-updater mws
42sh$ echo $?
0                 # Le conteneur est à jour

42sh$ echo "nouvelle page" > index.html
42sh$ docker build -t my_webserver .
42sh$ ctr-updater mws
42sh$ echo $?
1                 # Une mise à jour est dispo.
```
</div>

N'hésitez pas à utiliser la sortie d'erreur et la sortie standard pour afficher
des informations de débogage pour vous.

::::: {.warning}

#### Qu'est-ce qu'une image à jour ? {-}

Par *image à jour*, on entend : « aligné par rapport à l'image du
registre ». On ne s'intéresse pas à la date de construction ou de récupération
de l'image, mais uniquement à son identifiant. En bref : si l'image sur
laquelle se base un conteneur en cours d'exécution n'a pas le même identifiant
que l'image du même tag dans le cache d'images, alors on considère que le
conteneur doit être redémarré (même si la construction ou la récupération est
antérieure).\

Attention, on souhaite rester sur **le même tag** que celui avec lequel on a
démarré le conteneur. Il ne s'agit pas de trouver un tag plus récent.

Par exemple, si on a lancé un conteneur à partir de l'image `mariadb:10`, on va
chercher s'il y a une image différente pour le tag `mariadb:10` sur le
registre. Il n'est pas question d'aller parcourir la liste des tags pour voir
si oui ou non le tag est bien le dernier.

Si on a lancé un conteneur à partir de l'image `python:3.9`, il n'est pas
question de passer sur `python:3` ou `python:3.11`. Dans le cas de `latest`, on
cherche également à se maintenir à jour par rapport à `latest`, nécessairement
plus souvent que les autres tags.

:::::


## Étape 3 : Chercher une mise à jour l'image

En ajoutant l'option `--pull`, votre programme va lancer un *pull* de l'image
avant de faire la vérification.

<div lang="en-US">
```bash
42sh$ docker run -d --name myp0m nemunaire/youp0m
42sh$ ctr-updater myp0m
42sh$ echo $?
0                 # Le conteneur est à jour
42sh$ ctr-updater --pull myp0m
42sh$ echo $?
1                 # Une mise à jour est dispo.
42sh$ ctr-updater myp0m
42sh$ echo $?
1                 # La nouvelle image a été pull précédemment
                  # et est disponible dans le cache local
```
</div>

::::: {.question}

L'image `youp0m` est mise à jour régulièrement. Il y a de forte chance pour
qu'elle ne soit plus à jour si celle dont vous disposez date de plus d'une
semaine. Si vous possédez déjà la dernière version de vos conteneurs,
recherchez une image sur le Docker Hub régulièrement mise à jour pour faire vos
tests.

:::::

::::: {.warning}

Une fois l'image *pull* par `ctr-updater`, un nouvel appel à `ctr-updater` sans
`--pull` indiquera qu'une mise à jour est disponible, car le `pull` précédent
aura téléchargé localement l'image.

:::::

::::: {.question}

#### Comment tester lorsqu'on a `pull` toutes ses images ? {-}

Toutes les images persistent tant que vous ne les supprimez pas (via `docker
image rm` ou `docker image prune`). Vous pouvez alors réattribuer manuellement
son ancien tag à l'image précédente.

<div lang="en-US">
```bash
42sh$ docker image ls -f reference=nemunaire/youp0m
REPOSITORY         TAG       IMAGE ID       CREATED       SIZE
nemunaire/youp0m   latest    aafb58fb6a2d   5 days ago    25.1MB
nemunaire/youp0m   <none>    2c06880e48aa   3 weeks ago   25MB
42sh$ docker image tag 2c06880e48aa nemunaire/youp0m:latest

[...] # Considérant les commandes de l'exemple précédent
42sh$ ctr-updater myp0m
42sh$ echo $?
0                 # Le conteneur est à nouveau à jour
                  # On peut retenter de pull
```
</div>

:::::


## Étape 4 : Mettre à jour le conteneur

En ajoutant l'option `--autoupdate`, si une mise à jour de l'image utilisée par
le conteneur est disponible (après un éventuel `pull` si l'option est passée),
le conteneur est arrêté, supprimé, puis relancé avec les mêmes options, mais en
utilisant la nouvelle image.

Il est attendu dans ce cas de toujours retourner le statut 0 si la mise à jour
se passe bien.

### Exemples d'exécution {-}

<div lang="en-US">
```bash
42sh$ docker build -t my_webserver .
42sh$ docker run -d --name mws my_webserver
42sh$ echo "nouvelle page" > index.html
42sh$ docker build -t my_webserver .
42sh$ ctr-updater mws
42sh$ echo $?
1                 # Une mise à jour est dispo.

42sh$ ctr-updater --autoupdate mws
```
</div>

<div lang="en-US">
```bash
42sh$ docker run -d --name myp0m nemunaire/youp0m
42sh$ ctr-updater --pull --autoupdate myp0m
```
</div>

:::::
