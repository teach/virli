Utiliser l'API de Docker
========================

Le Docker Engine expose une API REST sur le protocole HTTP. Le client `docker`
utilise cette API pour communiquer avec le daemon. C'est aussi cette même API
que `docker-compose` emploie. En fait n'importe quel programme peut en faire
usage, c'est d'ailleurs très simple d'utilisation.

Si vous n'avez jamais communiqué avec une API REST, voici un premier exemple
pour récupérer des informations générales sur le daemon Docker avec un simple
`curl` :

<div lang="en-US">
```bash
curl -s --unix-socket /var/run/docker.sock http://localhost/v1.38/info | jq .
```
</div>

On utilise l'option `--unix-socket` de `curl` car on ne se connecte pas
directement à un port, comme c'est le cas habituellement avec le protocole
HTTP. Bien évidemment lorsque le daemon Docker se trouve sur une machine
distante (dans le cas de Docker Desktop sur Windows et Mac), on utilise l'IP et
le port de la machine distance.

Le premier élément dans le chemin de l'URL (`/v1.38/`) correspond à la version
de l'API que l'on souhaite utiliser. Celle-ci change dès que des
fonctionnalités sont ajoutées, à l'occasion d'une nouvelle version. Il n'est
généralement pas nécessaire de mettre à jour cette version dans les programmes
que vous développez car l'API est rétro-compatible : les anciennes versions de
l'API restent accessibles.

La commande que l'on a lancé nous retourne un objet JSON contenant des
informations similaires à ce que l'on obtient avec un `docker info`. L'avantage
est ici de pouvoir effectuer un traitement programmatique de ces informations.


Pour réaliser cet exercice, vous pouvez utiliser le langage de votre choix, en
utilisant des outils ou des bibliothèques cohérents avec l'objectif recherché :
dialoguer avec l'API.

Si vous êtes à l'aise en Python ou en Go, vous devriez utiliser les SDK
officiels :

- Python : <https://docker-py.readthedocs.io/en/stable/client.html>,
- Go : <https://pkg.go.dev/github.com/docker/docker/client>.

Pour le C, C#, C++, Clojure, Dart, Erlang, Gradle, Groovy, Haskell, Java,
NodeJS, Perl, PHP, Ruby, Rust, Scala, Swift, des SDK plus ou moins complets
sont disponibles :\
<https://docs.docker.com/engine/api/sdk/#unofficial-libraries>

Mais pas de panique si cela ne vous convient pas, l'API est assez succincte et
très bien documentée. N'importe quel langage dans lequel il vous est aisé de
faire des requêtes HTTP et parser du JSON fait très bien l'affaire, y compris
en shell.

Retrouvez la documentation de l'API ici :\
<https://docs.docker.com/engine/api/latest/>
